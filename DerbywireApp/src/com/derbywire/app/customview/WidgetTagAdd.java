package com.derbywire.app.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.derbywire.app.R;

public class WidgetTagAdd extends LinearLayout {

	TextView tv;

	public WidgetTagAdd(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.tagwidget, this, true);

		tv = (TextView) findViewById(R.id.tv_tag);
	}

	public WidgetTagAdd(Context context) {
		super(context);
	}

	public void setText(String tagValue) {
		if (tv != null && tagValue != null && tagValue.trim() != null)
			tv.setText(tagValue);
	}
}
