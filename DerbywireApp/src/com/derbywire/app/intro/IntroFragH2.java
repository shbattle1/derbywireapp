package com.derbywire.app.intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derbywire.app.R;

public class IntroFragH2 extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_intro_screen_h_2, container,
				false);

		return v;
	}

	public static IntroFragH2 newInstance(String text) {

		IntroFragH2 f = new IntroFragH2();
		Bundle b = new Bundle();
		b.putString("msg", text);

		f.setArguments(b);

		return f;
	}
}
