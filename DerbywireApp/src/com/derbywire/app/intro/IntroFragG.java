package com.derbywire.app.intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derbywire.app.R;

public class IntroFragG extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_intro_screen_g, container,
				false);

		return v;
	}

	public static IntroFragG newInstance(String text) {

		IntroFragG f = new IntroFragG();
		Bundle b = new Bundle();
		b.putString("msg", text);

		f.setArguments(b);

		return f;
	}
}
