package com.derbywire.app.intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derbywire.app.R;

public class IntroFragE extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_intro_screen_e, container,
				false);

		return v;
	}

	public static IntroFragE newInstance(String text) {

		IntroFragE f = new IntroFragE();
		Bundle b = new Bundle();
		b.putString("msg", text);

		f.setArguments(b);

		return f;
	}
}
