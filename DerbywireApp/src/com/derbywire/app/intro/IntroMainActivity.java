package com.derbywire.app.intro;

import java.lang.reflect.Field;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;

public class IntroMainActivity extends FragmentActivity {

	ViewPager pager;
	int position = 0;
	Context mContext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;
		/*
		 * Set the Activity to occupy full screen
		 */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.intro_main);

		pager = (ViewPager) findViewById(R.id.viewPager);
		pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

		try {
			Field mScroller;
			mScroller = ViewPager.class.getDeclaredField("mScroller");
			mScroller.setAccessible(true);
			FixedSpeedScroller scroller = new FixedSpeedScroller(
					pager.getContext());
			// scroller.setFixedDuration(5000);
			mScroller.set(pager, scroller);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}

	}

	public class FixedSpeedScroller extends Scroller {

		private int mDuration = 2000;

		public FixedSpeedScroller(Context context) {
			super(context);
		}

		public FixedSpeedScroller(Context context, Interpolator interpolator) {
			super(context, interpolator);
		}

		public FixedSpeedScroller(Context context, Interpolator interpolator,
				boolean flywheel) {
			super(context, interpolator, flywheel);
		}

		@Override
		public void startScroll(int startX, int startY, int dx, int dy,
				int duration) {
			// Ignore received duration, use fixed one instead
			super.startScroll(startX, startY, dx, dy, mDuration);
		}

		@Override
		public void startScroll(int startX, int startY, int dx, int dy) {
			// Ignore received duration, use fixed one instead
			super.startScroll(startX, startY, dx, dy, mDuration);
		}
	}

	private Handler handler = new Handler();

	private Runnable runnable = new Runnable() {
		@Override
		public void run() {

			if (position >= 7) {

				DerbyWireUtils.startActivity(mContext, ActivityType.SING_IN);
				return;
			} else {
				position = position + 1;
			}
			pager.setCurrentItem(position, true);
			handler.postDelayed(runnable, 3000);

		}
	};

	@Override
	public void onPause() {
		super.onPause();
		if (handler != null) {
			handler.removeCallbacks(runnable);
		}
	}

	@Override
	public void onResume() {
		super.onResume(); // Always call the superclass method first
		handler.postDelayed(runnable, 3000);

	}

	private class MyPagerAdapter extends FragmentPagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {

			case 0:
				return IntroFragB.newInstance("FirstFragment, Instance 1");
			case 1:
				return IntroFragC.newInstance("SecondFragment, Instance 1");
			case 2:
				return IntroFragE.newInstance("ThirdFragment, Instance 1");
			case 3:
				return IntroFragF.newInstance("ThirdFragment, Instance 2");
			case 4:
				return IntroFragG.newInstance("ThirdFragment, Instance 3");
			case 5:
				return IntroFragH1.newInstance("ThirdFragment, Instance 2");
			case 6:
				return IntroFragH2.newInstance("ThirdFragment, Instance 3");
			default:
				return IntroFragB.newInstance("ThirdFragment, Default");
			}
		}

		@Override
		public int getCount() {
			return 5;
		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
