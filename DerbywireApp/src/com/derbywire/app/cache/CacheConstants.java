package com.derbywire.app.cache;

import android.os.Environment; 

public class CacheConstants {
	
	public static final int TIME_OUT_READ = 15 * 1000;

	public static final int VERSION_HONEYCOMB = 13;
	
	public static final String RESPONSE_FILE_NAME = "response.txt";

	private static final String TEMP_DIR_NAME = "systemcache";
	
	public static final String CACHE_DIR = Environment.getExternalStorageDirectory()+"/"+TEMP_DIR_NAME+"/";
	
	public static final String KEY_ENTITY_OBJECT = "entity";	
	
	public static final String KEY_INTERNAL_CACHE_DIR = "inter_cache";
	
	public static final String THUMB_DIR = "thumbs/";
	
	public static int PAGE_SIZE_OFFERS = 10;
}