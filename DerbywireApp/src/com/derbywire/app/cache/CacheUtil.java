package com.derbywire.app.cache;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import com.derbywire.app.common.AppConstants;


public class CacheUtil {
	public static String readDiskFile(File file) throws Exception {
		FileInputStream fis = new FileInputStream(file);
		String content = readStream(fis);
		return content;
	}
	
	public static String readStream(InputStream stream) throws Exception {
		if(stream != null){
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(stream));
		String responeLine;
		StringBuilder responseBuilder = new StringBuilder();
		while ((responeLine = bufferedReader.readLine()) != null) {
			responseBuilder.append(responeLine);
			responseBuilder.append("\n");
		}
		
		stream.close();
		stream = null;
		return responseBuilder.toString();
		}
		
		return null;
	}
	
	public static Object readObjectStream(InputStream stream) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(stream));
		String responeLine;
		StringBuilder responseBuilder = new StringBuilder();
		while ((responeLine = bufferedReader.readLine()) != null) {
			responseBuilder.append(responeLine);
		}
		
		stream.close();
		stream = null;
		
		return responseBuilder.toString();
	}
	
	public static String readAssetContent(Context context, String assetFileName)
			throws Exception {

		AssetManager am = context.getAssets();
		InputStream stream = am.open(assetFileName);

		String data = readStream(stream);
		return data;
	}
	
	public static InputStream writeInputStreamToFile(String fileName,
			InputStream responseStream) throws Exception {
		
		int PACKET_SIZE = 1024 * 8;
		
		if(!isSdCardPresent() || !doesSdcardHasEnufSpace(responseStream.available()) || TextUtils.isEmpty(fileName)){
			return responseStream;
		}
		
		InputStream fis = null;

		File cacheDir = new File(CacheConstants.CACHE_DIR);
		if(!cacheDir.exists()){
			cacheDir.mkdir();
		}
		
		//Check if the target dir exists
		String[] tokens = fileName.split("/");
		
		for(int index=0; index < tokens.length-1; index++){
			String token = tokens[index];
			File interDir = new File(CacheConstants.CACHE_DIR+"/"+token);
			if(!interDir.exists()){
				interDir.mkdir();
			}
		}
		 
		File file = new File(CacheConstants.CACHE_DIR + "/" + fileName);

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);

			int read = 0;
			byte buffer[] = new byte[PACKET_SIZE];

			while ((read = responseStream.read(buffer)) != -1) {
				fos.write(buffer, 0, read);
			}

			if (fos != null) {
				fos.flush();
				fos.close();
			}
			
			{
				fis = new FileInputStream(file);
			}
			

		} catch (Exception e) {
			//This will ensure if the file is partially written then it gets deleted
			if (file != null) {
				file.delete();
			}
			
			e.printStackTrace();
			return responseStream;
		}

		return fis;
	}
	
	public static String writeInputStreamToFileNoReturn(String fileName,String folderName,
			InputStream responseStream) throws Exception {
		
		String pathWrittenTo = "null";
		
		int PACKET_SIZE = 1024 * 8;
		
		if(!isSdCardPresent() || !doesSdcardHasEnufSpace(responseStream.available())){
			return null;
		}
		
		File cacheDir = new File(CacheConstants.CACHE_DIR+"/");
		if(!cacheDir.exists()){
			cacheDir.mkdir();
		}
		
		cacheDir = new File(CacheConstants.CACHE_DIR+"/"+folderName);
		if(!cacheDir.exists()){
			cacheDir.mkdir();
		}
		
		File outFile = new File(cacheDir.getAbsoluteFile() + "/" + fileName);
		
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(outFile);
			pathWrittenTo = outFile.getAbsolutePath();
			
			int read = 0;
			byte buffer[] = new byte[PACKET_SIZE];

			while ((read = responseStream.read(buffer)) != -1) {
				fos.write(buffer, 0, read);
			}

			if (fos != null) {
				fos.close();
			}

		} catch (Exception e) {
			//This will ensure if the file is partially written then it gets deleted
			if (outFile != null) {
				outFile.delete();
			}
			e.printStackTrace();
			return null;
		}

		return pathWrittenTo;
	}
	
	public static void writeInputStreamToFileOnInternalMemory(String fileFullPath, InputStream responseStream, long lastModified) throws Exception {
		
		int PACKET_SIZE = 1024 * 8;
		
		File file = new File(fileFullPath);

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			
			int read = 0;
			byte buffer[] = new byte[PACKET_SIZE];

			while ((read = responseStream.read(buffer)) != -1) {
				fos.write(buffer, 0, read);
			}

			if (fos != null) {
				fos.close();
			}

			file.setLastModified(lastModified);
			
		} catch (Exception e) {
			//This will ensure if the file is partially written then it gets deleted
			if (file != null) {
				file.delete();
			}
			e.printStackTrace();
		}
	}
	
	public static boolean isSdCardPresent() {
		boolean result = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)){
			result = true;
		}

		return result;
	}
	
	public static boolean doesSdcardHasEnufSpace(double requiredSize){
		boolean spaceAvailable = false;
		
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		@SuppressWarnings("deprecation")
		double availSize = (double)stat.getAvailableBlocks() *(double)stat.getBlockSize();
		//One binary gigabyte equals 1,073,741,824 bytes.
		//double gigaAvailable = availSize / 1073741824;
		
		if(requiredSize < availSize){
			spaceAvailable = true;
		}
		
		return spaceAvailable;
	}
	
	public static void clearCache(Context context) {
		File cacheDir = new File(CacheConstants.CACHE_DIR);

		clearCacheDir(cacheDir);

		if (context != null) {
			SharedPreferences preferences = context.getSharedPreferences(
					AppConstants.PREF_TAG, Context.MODE_PRIVATE);

			Editor editor = preferences.edit();
			editor.clear();
			editor.commit();
		}
		context = null;
	}

	public static void clearCacheDir(File cacheDir) {
		File[] fileList = cacheDir.listFiles();

			for (int index = 0; index < fileList.length; index++) {
				if(fileList[index].isDirectory()){
					clearCacheDir(fileList[index]);
					fileList[index].delete();
				}else{
					fileList[index].delete();
				}
			}
			
			if(cacheDir.isDirectory()){
				cacheDir.delete();
			}
	}

	public static boolean checkIfFileExists(String localFilepath) {
		File file = new File(localFilepath);

		if (file.exists()) {
			return true;
		}

		return false;
	}
	
	public static void writeObjectToFile(Object cachedPageResponse,
			String folderName, String fileName) throws IOException {
		
		File cacheDir = new File(CacheConstants.CACHE_DIR+"/");
		if(!cacheDir.exists()){
			cacheDir.mkdir();
		}
		
		cacheDir = new File(CacheConstants.CACHE_DIR+"/"+folderName);
		if(!cacheDir.exists()){
			cacheDir.mkdir();
		}
		
		File f = new File(cacheDir.getAbsoluteFile() + "/" + fileName);
		
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		if( null != oos ){
			oos.writeObject(cachedPageResponse);
			oos.close();
		}		
	}
	
	public static String getMd5Hash(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String md5 = number.toString(16);
			while (md5.length() < 32)
				md5 = "0" + md5;
			return md5;
		} catch (NoSuchAlgorithmException e) {

			return null;
		}
	}

	public static void replaceContentInFile(String filePath, String target, String replaceWith) throws IOException{
		 String originalContent = getFileContent(filePath);
		 long lastModified = getFileLastModified(filePath);
		 
		 originalContent = originalContent.replace(target, replaceWith);
		
		 writeFile(filePath, originalContent, lastModified);
	}
	
	private static void writeFile(String filePath, String content, long lastModified) throws IOException {
	    File file = new File(filePath);
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
	    bw.write(content);
	    bw.close();
	    
	    file.setLastModified(lastModified);
	}

	private static long getFileLastModified(String filePath) throws IOException {
		File file = new File(filePath);
		
		long lastModified = -1;
		
		if(file != null){
			lastModified = file.lastModified();
		}
		
		return lastModified;
	}
	private static String getFileContent(String filePath) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));

	    String line;
	    StringBuffer sa = new StringBuffer();
	    while ((line = br.readLine()) != null) {
	       sa.append(line);
	       sa.append("\n");
	    }   
	    br.close();
	    return sa.toString();
	}
	
}