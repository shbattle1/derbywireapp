package com.derbywire.app.messaging;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.derbywire.app.R;

public class MessagingActivity extends Activity {
	private ListView mListView = null;
	private String[] listarry = { "Anandi", "Tarun" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_contacts);
		mListView = (ListView) findViewById(R.id.listview);
		mListView.setAdapter(new ContactsAdapter(listarry));

		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView v = (TextView) view.findViewById(R.id.contact);
				Intent intent = new Intent(MessagingActivity.this,
						ChatActivity.class);
				intent.putExtra("name", v.getText());
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		// noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private class ContactsAdapter extends BaseAdapter {
		private String[] listitem;

		public ContactsAdapter(String[] list) {
			listitem = list;
		}

		@Override
		public int getCount() {
			return listitem.length;
		}

		@Override
		public Object getItem(int arg0) {
			return listitem[arg0];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView name;

			LayoutInflater inflater = getLayoutInflater();
			View row;
			row = inflater.inflate(R.layout.chat_contacsrow, parent, false);
			name = (TextView) row.findViewById(R.id.contact);
			name.setText(listitem[position]);

			return (row);
		}

	}
}