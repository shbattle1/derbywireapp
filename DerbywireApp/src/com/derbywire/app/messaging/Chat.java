package com.derbywire.app.messaging;

/**
 * @author greg
 * @since 6/21/13
 */
public class Chat {

	private String message;
	private String author;
	private String to;

	// Required default constructor for Firebase object mapping
	@SuppressWarnings("unused")
	private Chat() {
	}

	Chat(String message, String author, String to) {
		this.message = message;
		this.author = author;
		this.to = to;
	}

	public String getMessage() {
		return message;
	}

	public String getAuthor() {
		return author;
	}

	public String getTo() {
		return to;
	}

}
