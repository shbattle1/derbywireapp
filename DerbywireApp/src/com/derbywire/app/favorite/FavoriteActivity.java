package com.derbywire.app.favorite;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants;

public class FavoriteActivity extends FragmentActivity {
	final String TAG = FavoriteActivity.class.getSimpleName();
	private View cell;
	private LinearLayout mContainerview;
	private ViewPager mViewpager;
	private FragmentPagerAdapter mPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favorites);
		intResources();
		addgalleryViews();
		mPagerAdapter = new FavoritesPagerAdapter(getSupportFragmentManager());
		mViewpager.setAdapter(mPagerAdapter);
	}

	private void intResources() {
		mContainerview = (LinearLayout) findViewById(R.id.contentview);
		mViewpager = (ViewPager) findViewById(R.id.vPager_fav);
	}

	private void addgalleryViews() {
		for (int i = 0; i < AppConstants.tracksImageId.length; i++) {
			cell = getLayoutInflater().inflate(R.layout.cell, null);
			final ImageView imageView = (ImageView) cell
					.findViewById(R.id._image);
			imageView.setId(i);
			imageView.setImageResource(AppConstants.tracksImageId[i]);

			mContainerview.addView(cell);
		}
	}

	private class FavoritesPagerAdapter extends FragmentPagerAdapter {

		public FavoritesPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {

			case 0:
				return FavoriteList.newInstance();
			default:
				return FavoriteList.newInstance();
			}
		}

		@Override
		public int getCount() {
			return 1;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "Page " + position;
		}
	}
}
