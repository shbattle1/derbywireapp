package com.derbywire.app.favorite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.detailsPageNo;

public class FavoritesListAdapter extends BaseAdapter {

	private Context context = null;
	private LayoutInflater mInflater = null;
	private String[] mCreatorlist = { "Daniel walker", "Drive", "The warmth",
			"Seconds", "truth", "Man up", "Love", "Drive", "Drive" };

	public FavoritesListAdapter(Context context, detailsPageNo pageNo) {
		this.context = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mCreatorlist.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.fav_list, null, false);
			holder.imageview1 = (DoubleView) convertView
					.findViewById(R.id.image1);
			holder.imageview2 = (DoubleView) convertView
					.findViewById(R.id.image2);
			holder.textview2 = (TextView) convertView.findViewById(R.id.t2);
			holder.textview3 = (TextView) convertView.findViewById(R.id.t2);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imageview1.setTextRuntime(mCreatorlist[position]);
		if (position % 2 == 0) {
			setBackground(
					holder,
					context.getResources().getColor(
							R.color.menu_left_background));
		} else {
			setBackground(
					holder,
					context.getResources().getColor(
							R.color.menu_right_background));
		}
		return convertView;
	}

	private void setBackground(ViewHolder holder, int color) {
		holder.imageview1.setBackgroundColor(color);
		holder.textview2.setBackgroundColor(color);
		holder.textview3.setBackgroundColor(color);
		holder.imageview2.setBackgroundColor(color);
	}

	private static class ViewHolder {
		DoubleView imageview1;
		DoubleView imageview2;
		TextView textview2;
		TextView textview3;
	}
}