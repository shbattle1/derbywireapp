package com.derbywire.app.favorite;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.detailsPageNo;

public class FavoriteList extends Fragment {
	private ListView favoriteList;

	public static FavoriteList newInstance() {
		FavoriteList fragmentFirst = new FavoriteList();
		return fragmentFirst;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View myFragmentView = inflater.inflate(R.layout.favorite_frag, null,
				false);
		favoriteList = (ListView) myFragmentView.findViewById(R.id.favList);
		favoriteList.setAdapter(new FavoritesListAdapter(getActivity(),
				detailsPageNo.PAGE_ONE));
		return myFragmentView;
	}
}
