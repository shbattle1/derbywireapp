package com.derbywire.app.signup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.common.LogFile;
import com.derbywire.app.common.SettingsEditor;
import com.derbywire.app.managers.ApiManager;
import com.derbywire.app.managers.OptimalPayments;
import com.derbywire.app.models.CardItem;
import com.derbywire.app.models.UserCard;
import com.derbywire.app.models.UserOpInfo;
import com.derbywire.app.models.UserProfile;
import com.derbywire.app.signin.UserInfo;
import com.google.gson.Gson;

public class SignUpActivity extends Activity {

	Button signUpBtn = null;
	// RadioButton privacyPolicy = null;
	EditText mEmailText = null;
	EditText mPasswordText = null;
	EditText mDOBText = null;
	EditText mGenderText = null;
	EditText mArtistText = null;
	EditText mCodewireText = null;
	ApiManager apiManager;
	SettingsEditor mSettingsEditor;
	UserProfile mprofile;
	OptimalPayments optimalPayments;
	ApiManager apimanager;

	Context mContext = null;

	UserInfo mSignUpData = null;

	private int year;
	private int month;
	private int day;

	static final int DATE_PICKER_ID = 1111;
	private static String TAG = SignUpActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		mContext = this;
		mSignUpData = new UserInfo();

		intResources();
		setClickListerners();

		// Get current date by calender

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
	}

	private void intResources() {
		signUpBtn = (Button) findViewById(R.id.signupBtn);
		mEmailText = (EditText) findViewById(R.id.emailText);
		mPasswordText = (EditText) findViewById(R.id.passwordText);
		mDOBText = (EditText) findViewById(R.id.DOBText);
		mGenderText = (EditText) findViewById(R.id.genderText);
		mArtistText = (EditText) findViewById(R.id.artistText);
		mCodewireText = (EditText) findViewById(R.id.codewireText);

		/*
		 * privacyPolicy = (RadioButton) findViewById(R.id.radioPolicy);
		 * 
		 * String privacyText =
		 * "I agree to the <b> terms </b> & <b> privacy policy </b>";
		 * privacyPolicy.setText(Html.fromHtml(privacyText));
		 */
	}

	private void setClickListerners() {
		signUpBtn.setOnClickListener(BtnListener);
		// mDOBText.setOnFocusChangeListener(onFocusChangeListener);
		mDOBText.setOnTouchListener(onTouchListener);
	}

	OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View view, boolean hasFocus) {
			switch (view.getId()) {

			case R.id.DOBText:
				// On touch click show date picker dialog
				showDialog(DATE_PICKER_ID);
				break;

			default:
				break;
			}
		}
	};

	OnTouchListener onTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View view, MotionEvent event) {
			switch (view.getId()) {

			case R.id.DOBText:
				// On touch click show date picker dialog
				showDialog(DATE_PICKER_ID);
				break;

			default:
				break;
			}
			return false;
		}
	};
	OnClickListener BtnListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {

			case R.id.signupBtn:
				doSignUp();
				createprofile();
				break;

			default:
				break;
			}
		}
	};

	private void doSignUp() {
		
		try{
			
		
		mSignUpData.setEmail(mEmailText.getText().toString());
		mSignUpData.setPassword(mPasswordText.getText().toString());
		mSignUpData.setConfirmpassword(mPasswordText.getText().toString());
		mSignUpData.setMonth(month);
		mSignUpData.setDate(day);
		mSignUpData.setYear(year);
		mSignUpData.setGender("Male");
		// mSignUpData.setIMEINo(DerbyWireUtils.getIMEI(this));
		mSignUpData.setUserType("Fan");
		mSignUpData.setCodewire("codewire");
		mSignUpData.setRequestFrom("mobile");
		
		apimanager = new ApiManager(getApplicationContext(),
				new ApiManager.TimeOutListener() {
					@Override
					public void onTimeout() {

//						apimanager.doLogin(mSignInData,
//								signinResponseListener);

					}
				});
		apimanager.registerAccount(mSignUpData, signupResponseListener);
		
		}catch(Exception e){
			e.printStackTrace();
		}

		//signUpHandler.doSignUp(mSignUpData, signUpListener);
	}

	

	

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			return new DatePickerDialog(this, pickerListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// Show selected date
			mDOBText.setText(new StringBuilder().append(month + 1).append("-")
					.append(day).append("-").append(year).append(" "));

		}
	};

	private void createprofile() {
		apiManager = new ApiManager(getApplicationContext(),
				new ApiManager.TimeOutListener() {
					@Override
					public void onTimeout() {
					}
				});

		optimalPayments = new OptimalPayments(getApplicationContext(),
				new ApiManager.TimeOutListener() {
					@Override
					public void onTimeout() {
						// if (mProgressDialog != null) {
						// mProgressDialog.dismiss();
						Toast.makeText(
								getApplicationContext(),
								"Error, Invalid fields\n Either Invalid card number or Invalid Exp Date",
								Toast.LENGTH_LONG).show();
					}
				});

		mSettingsEditor = new SettingsEditor(getApplicationContext());
		UserProfile profile = new UserProfile();
		profile.setFirstName("unknown");
		profile.setLastName("unknown");
		profile.setEmail(mSignUpData.getEmail());
		profile.setPassword(mSignUpData.getPassword());
		profile.setCountry("US");
		profile.setZipCode("53434");
		AppSession.getInstance().updateUserProfile(profile);
		mprofile = AppSession.getInstance().getUserProfile();
		Bitmap bm = BitmapFactory.decodeResource(getResources(),
				R.drawable.image);
		UserCard card = new UserCard("4530910000012345", "111", 12, 2016);
		AppSession.getInstance().setCard(card);

		// apiManager.registerAccount("Tarun", "Vijay",
		// "tarun@test.com",
		// "Iloveindia@123", bm, apiResponseListener);
		optimalPayments.createProfile(
				String.valueOf(mprofile.getEmail()
						+ System.currentTimeMillis()), Locale.US.toString(),
				mprofile.getFirstName(), mprofile.getLastName(),
				mprofile.getEmail(), apiResponseListener);
	}

	private Response.Listener<JSONObject> apiResponseListener = new Response.Listener<JSONObject>() {
		@Override
		public void onResponse(JSONObject response) {
			try {
				Log.d("Senthil", "onResponse");
				if (response.toString().contains(ApiManager.RESPONSE.ERROR)) {

					boolean isError = response
							.getBoolean(ApiManager.RESPONSE.ERROR);
					if (isError) {
						AppSession
								.getInstance()
								.DisplayToast(
										getApplicationContext(),
										response.getString(ApiManager.RESPONSE.MESSAGE));

					} else if (!isError) {
						if (response.toString().contains(UserProfile.API_KEY)) {
							mSettingsEditor.setValue(UserProfile.API_KEY,
									response.getString(UserProfile.API_KEY));
							// apiManager.setApiKey(response
							// .getString(UserProfile.API_KEY));

							String emailId = mprofile.getEmail().substring(0,
									mprofile.getEmail().indexOf("@"));
							optimalPayments.createProfile(
									String.valueOf(emailId
											+ System.currentTimeMillis()),
									Locale.US.toString(),
									mprofile.getFirstName(),
									mprofile.getLastName(),
									mprofile.getEmail(), apiResponseListener);
						} else {
							Log.d("Senthil", "onResponse");
						}
					}
				} else {
					if (response.toString().contains(
							OptimalPayments.FIELDS.MERCHANT_CUSTOMER_ID)) {
						mSettingsEditor
								.setValue(
										OptimalPayments.OP_PROFILE_ID,
										response.getString(OptimalPayments.FIELDS.UNIQUE_ID));
						optimalPayments
								.addAddress(
										response.getString(OptimalPayments.FIELDS.UNIQUE_ID),
										mprofile.getFirstName() + " "
												+ mprofile.getLastName(),
										mprofile.getCountry(), mprofile
												.getZip(), apiResponseListener);
					} else if (response.toString().contains(
							OptimalPayments.FIELDS.COUNTRY)
							&& response.toString().contains(
									OptimalPayments.FIELDS.ZIP_CODE)) {
						mSettingsEditor
								.setValue(
										OptimalPayments.FIELDS.BILLING_ADDRESS_ID,
										response.getString(OptimalPayments.FIELDS.UNIQUE_ID));
						Log.d("Senthil", "Response: " + response.toString());
						UserCard card = AppSession.getInstance().getCard();
						optimalPayments
								.createCard(
										response.getString(OptimalPayments.FIELDS.UNIQUE_ID),
										mSettingsEditor
												.getStringValue(OptimalPayments.OP_PROFILE_ID),
										card.getCardNumber(), card.getCvv(),
										card.getExpMonth(), card.getExpYear(),
										apiResponseListener);
					} else if (response.toString().contains(
							OptimalPayments.FIELDS.LAST_DIGITS)) {

						List<CardItem> cards = new ArrayList<CardItem>();
						cards.add(new CardItem(
								response.getString(OptimalPayments.FIELDS.UNIQUE_ID),
								response.getString(OptimalPayments.FIELDS.CARD_TYPE),
								response.getString(OptimalPayments.FIELDS.LAST_DIGITS),
								true));
						Log.d("Senthil", "Response: " + response.toString());

						UserOpInfo opInfo = new UserOpInfo(
								mSettingsEditor
										.getStringValue(OptimalPayments.OP_PROFILE_ID),
								cards);

						mSettingsEditor.setValue(UserOpInfo.OP_INFO_JSON,
								new Gson().toJson(opInfo));

						// apiManager.insertOpInfo(false, opInfo,
						// apiResponseListener);

					} else {
						String msg = response
								.getString(ApiManager.RESPONSE.MESSAGE);
						AppSession.getInstance().DisplayToast(
								getApplicationContext(),
								msg.substring(0, msg.indexOf("-")));

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				Log.d("Senthil", "Exception:" + e.toString());
			}
		}
	};

	private Response.Listener<JSONObject> signupResponseListener = new Response.Listener<JSONObject>() {
		@Override
		public void onResponse(JSONObject response) {
			try {
				AppSession.getInstance().DEBUG(getApplicationContext(),
						"SIGN up RESPONSE = " + response.toString());

				JSONObject jsonObject = new JSONObject();

				jsonObject = new JSONObject(response.toString());
				if (jsonObject.getString(ApiManager.RESPONSE.ERROR) != "true") {
					finish();
					DerbyWireUtils.startActivity(mContext, ActivityType.INTRO);

				} else {
					AppSession.getInstance().DisplayToast(
							getApplicationContext(), response.toString());

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

}
