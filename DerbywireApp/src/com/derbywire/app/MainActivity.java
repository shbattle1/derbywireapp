package com.derbywire.app;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.common.RestAPIConstants;
import com.derbywire.app.managers.ApiManager;
import com.derbywire.app.signin.SignInActivity;
import com.derbywire.app.signin.UserInfo;

public class MainActivity extends Activity {

	private ProgressBar mRefreshProgressBar;
	LinearLayout mBottomView;
	RelativeLayout mFirstView;
	Button mSignIn;
	ApiManager apimanager;
	Button mSignUp;
	Timer timer = null;
	MyTimerTask myTimerTask;
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.flashscreen);
		try {

			mContext = this;

			intResources();
			setListeners();

			trysign();

			showRefreshProgress();
			initRestConstants();

			// set dummy timer for displaying launching screen
			timer = new Timer();
			myTimerTask = new MyTimerTask();
			timer.schedule(myTimerTask, 3000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void trysign() {

		SharedPreferences sp1 = getSharedPreferences("Login", 0);

		String unm = sp1.getString(ApiManager.FIELDS.EMAIL, null);
		String pass = sp1.getString(ApiManager.FIELDS.PASSWORD, null);

		AppSession.getInstance().DisplayToast(getApplicationContext(),
				unm + " " + pass + " " + "Login");
		if (unm != null && pass != null) {

			apimanager = new ApiManager(getApplicationContext(),
					new ApiManager.TimeOutListener() {
						@Override
						public void onTimeout() {

							// apimanager.doLogin(mSignInData,
							// signinResponseListener);

						}
					});
			apimanager.doLogin(unm, pass, signinResponseListener);
		}
		// if(unm != null && pass != null){
		// finish();
		// DerbyWireUtils.startActivity(mContext, ActivityType.BROWSE);
		//
		// }

	}

	private void setListeners() {
		mSignIn.setOnClickListener(BtnListener);
		mSignUp.setOnClickListener(BtnListener);
	}

	private void intResources() {
		mRefreshProgressBar = (ProgressBar) findViewById(R.id.refresh_progressbar);
		mBottomView = (LinearLayout) findViewById(R.id.bootomView);
		mFirstView = (RelativeLayout) findViewById(R.id.firstView);
		mSignIn = (Button) findViewById(R.id.signInBtn);
		mSignUp = (Button) findViewById(R.id.signUpBtn);
	}

	OnClickListener BtnListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.signInBtn:
				finish();
				DerbyWireUtils.startActivity(mContext, ActivityType.SING_IN);
				break;

			case R.id.signUpBtn:
				finish();
				DerbyWireUtils.startActivity(mContext, ActivityType.SING_UP);
				break;
			default:
				break;
			}
		}
	};

	private void initRestConstants() {
		RestAPIConstants.getInstance().setDerbywireBaseUrl(0);
	}

	private void showRefreshProgress() {
		if (mRefreshProgressBar != null) {
			mRefreshProgressBar.setVisibility(View.VISIBLE);

		}
	}

	private void hideRefreshProgress() {
		if (mRefreshProgressBar != null) {
			mRefreshProgressBar.setVisibility(View.INVISIBLE);
		}
	}

	class MyTimerTask extends TimerTask {

		@Override
		public void run() {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (timer != null) {
						timer.cancel();
					}
					hideRefreshProgress();
					showButtonAnimation();
					// launchSignInActivity();
				}
			});
		}

	}

	private Animation inFromRightAnimation() {

		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(5000);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		return inFromRight;
	}

	private Animation inFromLeftAnimation() {
		Animation inFromLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromLeft.setDuration(5000);
		inFromLeft.setInterpolator(new AccelerateInterpolator());
		return inFromLeft;
	}

	private void showButtonAnimation() {
		mFirstView.setVisibility(View.GONE);
		mBottomView.setVisibility(View.VISIBLE);
		mSignIn.startAnimation(inFromRightAnimation());
		mSignUp.startAnimation(inFromLeftAnimation());
	}

	private Response.Listener<JSONObject> signinResponseListener = new Response.Listener<JSONObject>() {
		@Override
		public void onResponse(JSONObject response) {
			try {
				AppSession.getInstance().DEBUG(getApplicationContext(),
						"SIGN IN RESPONSE = " + response.toString());

				JSONObject jsonObject = new JSONObject();

				jsonObject = new JSONObject(response.toString());
				if (jsonObject.getString(ApiManager.RESPONSE.ERROR) != "true") {

					finish();
					DerbyWireUtils.startActivity(mContext, ActivityType.BROWSE);

				} else {
					AppSession.getInstance().DisplayToast(
							getApplicationContext(), response.toString());

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
}
