package com.derbywire.app.mywire;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.derbywire.app.R;

public class MyWireActivity extends Activity {
	final String TAG = MyWireActivity.class.getSimpleName();
	Context mContext = null;
	TextView title = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.temp_layout);
		mContext = this;

		intResources();
		setClickListerners();

		title.setText(TAG);
	}

	private void setClickListerners() {
	}

	private void intResources() {
		title = (TextView) findViewById(R.id.title);
	}
}
