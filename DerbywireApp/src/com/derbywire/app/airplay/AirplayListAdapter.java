package com.derbywire.app.airplay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.favorite.DoubleView;

public class AirplayListAdapter extends BaseAdapter {

	private Context context = null;
	private LayoutInflater mInflater = null;
	private String[] mCreatorlist = { "Daniel walker", "Drive", "The warmth",
			"Seconds", "truth", "Man up", "Love", "Drive", "Drive" };

	public AirplayListAdapter(Context context) {
		this.context = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mCreatorlist.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.livestream_list, null,
					false);
			holder.textview1 = (TextView) convertView.findViewById(R.id.t1);
			holder.textview2 = (TextView) convertView.findViewById(R.id.t2);
			holder.textview3 = (TextView) convertView.findViewById(R.id.t3);
			holder.imageview4 = (DoubleView) convertView
					.findViewById(R.id.image4);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// holder.textview1.setTextRuntime(mCreatorlist[position]);
		if (position % 2 == 0) {
			setBackground(
					holder,
					context.getResources().getColor(
							R.color.menu_left_background));
		} else {
			setBackground(
					holder,
					context.getResources().getColor(
							R.color.menu_right_background));
		}
		return convertView;
	}

	private void setBackground(ViewHolder holder, int color) {
		holder.textview1.setBackgroundColor(color);
		holder.textview2.setBackgroundColor(color);
		holder.textview3.setBackgroundColor(color);
		holder.imageview4.setBackgroundColor(color);
	}

	private static class ViewHolder {
		TextView textview1;
		TextView textview2;
		TextView textview3;
		DoubleView imageview4;
	}
}