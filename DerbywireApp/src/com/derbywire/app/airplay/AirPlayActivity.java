package com.derbywire.app.airplay;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;

import com.derbywire.app.R;
import com.derbywire.app.airplay.DemoPlayer.RendererBuilder;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer.extractor.ts.AdtsExtractor;
import com.google.android.exoplayer.extractor.ts.TsExtractor;
import com.google.android.exoplayer.extractor.webm.WebmExtractor;
import com.google.android.exoplayer.util.Util;

public class AirPlayActivity extends Activity implements DemoPlayer.Listener {
	final String TAG = AirPlayActivity.class.getSimpleName();
	Context mContext = null;
	private ListView mListview = null;
	private AirplayListAdapter mListadapter = null;
	private SurfaceView mSurfaceview = null;
	private MediaController mediaController;
	private DemoPlayer player;
	private int contentType;
	private Uri contentUri;
	private boolean playerNeedsPrepare;
	private ProgressBar mProgressbar = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.live_streaming);
		mContext = this;
		contentType = DemoUtil.TYPE_SS;
		contentUri = Uri
				.parse("http://iis7test.entriq.net/Clr/Big_Buck_Bunny.ism/Manifest");

		intResources();
		setClickListerners();

		mediaController = new MediaController(this);
		mediaController.setAnchorView(mSurfaceview);
		mListadapter = new AirplayListAdapter(mContext);
		mListview.setAdapter(mListadapter);
	}

	private void setClickListerners() {
		mSurfaceview.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					if (mediaController != null && player != null) {
						mediaController.setEnabled(true);
						mediaController.show();
					}
				} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
					view.performClick();
				}
				return true;
			}
		});
		mSurfaceview.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
					return mediaController.dispatchKeyEvent(event);
				}
				return false;
			}
		});

		mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				releasePlayer();
				preparePlayer();
			}
		});
	}

	private void intResources() {
		mListview = (ListView) findViewById(R.id.live_list);
		mSurfaceview = (SurfaceView) findViewById(R.id.surfaceview);
		mProgressbar = (ProgressBar) findViewById(R.id.live_progress);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void preparePlayer() {
		if (player == null) {
			player = new DemoPlayer(getRendererBuilder());
			// player.seekTo(playerPosition);
			playerNeedsPrepare = true;
			player.addListener(this);
			mediaController.setMediaPlayer(player.getPlayerControl());
			mediaController.setEnabled(true);
		}
		if (playerNeedsPrepare) {
			player.prepare();
			playerNeedsPrepare = false;
		}
		player.setSurface(mSurfaceview.getHolder().getSurface());
		player.setPlayWhenReady(true);
	}

	private void releasePlayer() {
		if (player != null) {
			player.release();
			player = null;
		}
	}

	private RendererBuilder getRendererBuilder() {
		String userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
		switch (contentType) {
		case DemoUtil.TYPE_SS:
			return new SmoothStreamingRendererBuilder(this, userAgent,
					contentUri.toString(),
					new SmoothStreamingTestMediaDrmCallback(), null);
		case DemoUtil.TYPE_DASH:
			return new DashRendererBuilder(this, userAgent,
					contentUri.toString(), new WidevineTestMediaDrmCallback(
							null), null, null);
		case DemoUtil.TYPE_HLS:
			return new HlsRendererBuilder(this, userAgent,
					contentUri.toString(), null, null);
		case DemoUtil.TYPE_M4A: // There are no file format differences between
								// M4A and MP4.
		case DemoUtil.TYPE_MP4:
			return new ExtractorRendererBuilder(this, userAgent, contentUri,
					null, new Mp4Extractor());
		case DemoUtil.TYPE_MP3:
			return new ExtractorRendererBuilder(this, userAgent, contentUri,
					null, new Mp3Extractor());
		case DemoUtil.TYPE_TS:
			return new ExtractorRendererBuilder(this, userAgent, contentUri,
					null, new TsExtractor(0, null));
		case DemoUtil.TYPE_AAC:
			return new ExtractorRendererBuilder(this, userAgent, contentUri,
					null, new AdtsExtractor());
		case DemoUtil.TYPE_WEBM:
			return new ExtractorRendererBuilder(this, userAgent, contentUri,
					null, new WebmExtractor());
		default:
			throw new IllegalStateException("Unsupported type: " + contentType);
		}
	}

	@Override
	public void onStateChanged(boolean playWhenReady, int playbackState) {
		switch (playbackState) {
		case ExoPlayer.STATE_BUFFERING:
			mProgressbar.setVisibility(View.VISIBLE);
			break;
		case ExoPlayer.STATE_ENDED:
			break;
		case ExoPlayer.STATE_IDLE:
			break;
		case ExoPlayer.STATE_PREPARING:
			mProgressbar.setVisibility(View.VISIBLE);
			break;
		case ExoPlayer.STATE_READY:
			mProgressbar.setVisibility(View.GONE);
			break;
		default:
			break;
		}

	}

	@Override
	public void onError(Exception e) {
	}

	@Override
	public void onVideoSizeChanged(int width, int height,
			float pixelWidthHeightRatio) {
	}
}
