package com.derbywire.app.browse;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.GridView;
import android.widget.ImageButton;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;

public class BrowseActivity extends Activity implements OnClickListener {
	final String TAG = BrowseActivity.class.getSimpleName();
	Context mContext = null;
	ImageButton menuBtn = null;
	ImageButton collapseView = null;

	Boolean isSubMenu = false;

	BrowseAdapter listAdapter;
	ExpandableListView expListView;
	int previousGroup = 2; // snehal need to check

	final String categoryIds[] = { "top trending", "new releases", "genres",
			"top reviews" };
	final String subCategoryIds[] = { "music", "videos", "photos", "movies" };

	Integer[] categoryIcons1 = { R.drawable.browse_alternative,
			R.drawable.browse_audiolyrics, R.drawable.browse_beats,
			R.drawable.browse_christian, R.drawable.browse_classical,
			R.drawable.browse_country, R.drawable.browse_dj_mix,
			R.drawable.browse_reggea };

	Integer[] categoryIcons2 = { R.drawable.browse_edm,
			R.drawable.browse_electronic, R.drawable.browse_fashion_photo,
			R.drawable.browse_rb };

	Integer[] categoryIcons3 = { R.drawable.browse_hiphop,
			R.drawable.browse_india, R.drawable.browse_latin,
			R.drawable.browse_quotes };

	Integer[] categoryIcons4 = { R.drawable.browse_marketing_hhoto,
			R.drawable.browse_metal, R.drawable.browse_musicvideo,
			R.drawable.browse_pop };

	Integer[] subCategoryIcons1 = { R.drawable.browse_spokenword,
			R.drawable.browse_shortmovie, R.drawable.browse_selfie_video,
			R.drawable.browse_selfie_photo };

	Integer[] subCategoryIcons2 = { R.drawable.browse_rock,
			R.drawable.browse_reggea };

	Integer[] subCategoryIcons3 = { R.drawable.browse_rb,
			R.drawable.browse_quotes };

	Integer[] subCategoryIcons4 = { R.drawable.browse_pop,
			R.drawable.browse_musicvideo };

	List<BrowseMenuData> listBrowseMenuData;
	List<BrowseMenuData> listBrowseSubMenuData;

	GridView mGridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.browse_layout);
		mContext = this;

		intResources();
		setClickListerners();

		// preparing list data
		prepareListData();
		prepareSubListData();
		/*
		 * mGridView = (GridView) findViewById(R.id.gridview);
		 * mGridView.setNumColumns(2); mGridView.setAdapter(new
		 * ImageAdapter(mContext, categoryIcons1));
		 */
		listAdapter = new BrowseAdapter(mContext, listBrowseMenuData,
				onClickListener);

		// setting list adapter
		expListView.setAdapter(listAdapter);
		expListView.setGroupIndicator(null);
		expListView.expandGroup(previousGroup);
	}

	private void setClickListerners() {
		menuBtn.setOnClickListener(this);
		expListView.setOnGroupExpandListener(groupExpandListener);
		expListView.setOnChildClickListener(childClickListener);
		collapseView.setOnClickListener(this);
	}

	private void intResources() {
		menuBtn = (ImageButton) findViewById(R.id.menuBtn);
		expListView = (ExpandableListView) findViewById(R.id.menuOptions);
		collapseView = (ImageButton) findViewById(R.id.collapseView);
	}

	OnChildClickListener childClickListener = new OnChildClickListener() {

		@Override
		public boolean onChildClick(ExpandableListView parent, View view,
				int groupPosition, int childPosition, long id) {
			setSelectedMenu(view);
			return false;
		}
	};

	// Listview Group expanded listener
	OnGroupExpandListener groupExpandListener = new OnGroupExpandListener() {

		@Override
		public void onGroupExpand(int groupPosition) {
			if (groupPosition != previousGroup)
				expListView.collapseGroup(previousGroup);
			previousGroup = groupPosition;
			// BrowseMenuData selctedMenu =
			// listBrowseMenuData.get(groupPosition);
			/*
			 * if( selctedMenu.getActivityType() != ActivityType.NONE){
			 * Utils.startActivity(mContext, selctedMenu.getActivityType()); }
			 */
		}
	};

	// ListView Group clicked listener
	OnGroupClickListener groupClickListener = new OnGroupClickListener() {

		@Override
		public boolean onGroupClick(ExpandableListView parent, View view,
				int groupPosition, long id) {
			expListView.expandGroup(groupPosition);
			setSelectedMenu(view);
			return false;
		}
	};

	private void setSelectedMenu(View view) {
		if (isSubMenu == false) {
			isSubMenu = true;
			listAdapter.updateMenu(listBrowseSubMenuData);
			listAdapter.notifyDataSetChanged();
		} else {
			DerbyWireUtils.startActivity(mContext, ActivityType.MUSIC_PLAYLIST);
		}
		/*
		 * if (view != null && view.getTag() != null) { menuData selectedMenu =
		 * (menuData) view.getTag(); ActivityType selectedActivityType =
		 * selectedMenu.getActivityType(); if (selectedActivityType !=
		 * ActivityType.NONE) { LogFile.ShowLog(TAG,
		 * "Selected Menu: ActivityType: " + selectedActivityType);
		 * Utils.startActivity(mContext, selectedActivityType); } }
		 */
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isSubMenu == false) {
				isSubMenu = true;
				listAdapter.updateMenu(listBrowseSubMenuData);
				listAdapter.notifyDataSetChanged();
			} else {
				DerbyWireUtils.startActivity(mContext,
						ActivityType.MUSIC_PLAYLIST);
			}
		}
	};

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.menuBtn:
			DerbyWireUtils.startActivity(mContext, ActivityType.MENU);
			break;
		case R.id.collapseView:
			expListView.collapseGroup(previousGroup);
			break;
		default:
			break;
		}
	}

	/*
	 * Preparing the list data
	 */
	private void prepareListData() {
		listBrowseMenuData = new ArrayList<BrowseMenuData>();

		// Adding parent data
		for (int i = 0; i < categoryIds.length; i++) {
			BrowseMenuData menuItem = new BrowseMenuData();

			menuItem.setCategoryTitle(categoryIds[i]);
			if (i == 0)
				menuItem.setCategoryIcons(categoryIcons1);
			if (i == 1)
				menuItem.setCategoryIcons(categoryIcons2);
			if (i == 2)
				menuItem.setCategoryIcons(categoryIcons3);
			if (i == 3)
				menuItem.setCategoryIcons(categoryIcons4);

			listBrowseMenuData.add(menuItem);
		}
	}

	private void prepareSubListData() {
		listBrowseSubMenuData = new ArrayList<BrowseMenuData>();

		// Adding parent data
		for (int i = 0; i < subCategoryIds.length; i++) {
			BrowseMenuData menuItem = new BrowseMenuData();

			menuItem.setCategoryTitle(subCategoryIds[i]);
			if (i == 0)
				menuItem.setCategoryIcons(subCategoryIcons1);
			if (i == 1)
				menuItem.setCategoryIcons(subCategoryIcons2);
			if (i == 2)
				menuItem.setCategoryIcons(subCategoryIcons3);
			if (i == 3)
				menuItem.setCategoryIcons(subCategoryIcons4);

			listBrowseSubMenuData.add(menuItem);
		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
