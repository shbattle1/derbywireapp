package com.derbywire.app.browse;


public class BrowseMenuData {

	String mCategoryTitle;
	Integer[] mCategoryIcons;
		
	public void setCategoryIcons(Integer[] categoryIcons) {
		this.mCategoryIcons = categoryIcons;
	}
	public String getCategoryTitle() {
		return mCategoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.mCategoryTitle = categoryTitle;
	}
	public Integer[] getCategoryIcons() {
		return mCategoryIcons;
	}
	
}
