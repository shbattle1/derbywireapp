package com.derbywire.app.browse;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	public Integer[] mThumbIds;

	// Constructor
	public ImageAdapter(Context c, Integer[] mThumbIds) {
		mContext = c;
		this.mThumbIds = mThumbIds;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// create a new ImageView for each item referenced by the Adapter
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		/*
		 * if (convertView == null) { LayoutInflater infalInflater =
		 * (LayoutInflater) this.mContext
		 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE); convertView =
		 * infalInflater.inflate(R.layout.category, null); }
		 * 
		 * ImageView imageView = (ImageView)
		 * convertView.findViewById(R.id.catImage);
		 */
		ImageView imageView = null;
		if (convertView == null) {
			imageView = new ImageView(mContext);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			// imageView.setLayoutParams(new LinearLayout.LayoutParams(80, 80));
			// imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
		}
		imageView.setImageResource(mThumbIds[position]);
		imageView.setOnClickListener(onClickListener);
		return imageView;
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			DerbyWireUtils.startActivity(mContext, ActivityType.MUSIC_PLAYLIST);
		}
	};

	// Keep all Images in array
	/*
	 * public Integer[] mThumbIds = { R.drawable.sample_2, R.drawable.sample_3,
	 * R.drawable.sample_4, R.drawable.sample_5, R.drawable.sample_6,
	 * R.drawable.sample_7, R.drawable.sample_0, R.drawable.sample_1,
	 * R.drawable.sample_2, R.drawable.sample_3, R.drawable.sample_4,
	 * R.drawable.sample_5, R.drawable.sample_6, R.drawable.sample_7,
	 * R.drawable.sample_0, R.drawable.sample_1, R.drawable.sample_2,
	 * R.drawable.sample_3, R.drawable.sample_4, R.drawable.sample_5,
	 * R.drawable.sample_6, R.drawable.sample_7 };
	 */
}