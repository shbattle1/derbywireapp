package com.derbywire.app.browse;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.derbywire.app.R;

public class BrowseAdapter extends BaseExpandableListAdapter {

	Context mContext;
	List<BrowseMenuData> listBrowseMenuData;
	OnClickListener onClickListener;

	public BrowseAdapter(Context mContext,
			List<BrowseMenuData> listBrowseMenuData,
			OnClickListener onClickListener) {
		this.mContext = mContext;
		this.listBrowseMenuData = listBrowseMenuData;
		this.onClickListener = onClickListener;
	}

	public void updateMenu(List<BrowseMenuData> listBrowseMenuData) {
		this.listBrowseMenuData = listBrowseMenuData;
	}

	@Override
	public int getGroupCount() {
		if (listBrowseMenuData != null && listBrowseMenuData.size() > 0)
			return listBrowseMenuData.size();
		else
			return 0;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		/*
		 * int count = 0; if( listBrowseMenuData != null &&
		 * listBrowseMenuData.get(groupPosition) != null){ if(
		 * listBrowseMenuData.get(groupPosition).getCategoryIcons() != null){
		 * count =
		 * listBrowseMenuData.get(groupPosition).getCategoryIcons().length; } }
		 * if(count > 2) return count/2; else return 0;
		 */
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		if (listBrowseMenuData != null
				&& listBrowseMenuData.get(groupPosition) != null) {
			return listBrowseMenuData.get(groupPosition);
		} else
			return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		Integer[] ids = null;
		if (listBrowseMenuData != null
				&& listBrowseMenuData.get(groupPosition) != null) {
			if (listBrowseMenuData.get(groupPosition).getCategoryIcons() != null) {
				ids = listBrowseMenuData.get(groupPosition).getCategoryIcons();
			}
		}
		return ids;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		BrowseMenuData menuGroup = (BrowseMenuData) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.browse_group, null);
		}

		TextView groupText = (TextView) convertView
				.findViewById(R.id.groupText);
		groupText.setText(menuGroup.getCategoryTitle());

		convertView.setTag(menuGroup);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		Integer[] ids = (Integer[]) getChild(groupPosition, childPosition);
		LinearLayout browsesuncat = null;
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.browse_item, null);
		}
		browsesuncat = (LinearLayout) convertView
				.findViewById(R.id.browseSubCat);

		if (browsesuncat.getChildCount() > 0) {
			browsesuncat.removeAllViews();
		}

		for (int i = 0; i < ids.length; i++) {
			LayoutInflater infalInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = infalInflater.inflate(R.layout.category, null);
			ImageView imageView1 = (ImageView) rowView
					.findViewById(R.id.catImage1);
			ImageView imageView = (ImageView) rowView
					.findViewById(R.id.catImage2);

			imageView1.setImageResource(ids[i]);
			imageView1.setOnClickListener(onClickListener);
			// browsesuncat.addView(imageView1);

			imageView.setImageResource(ids[++i]);
			imageView.setOnClickListener(onClickListener);
			browsesuncat.addView(rowView);

			/*
			 * if( i%2 == 0){ //ImageView imageView1 = new ImageView(mContext);
			 * //imageView1.setScaleType(ImageView.ScaleType.FIT_XY);
			 * LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
			 * LinearLayout.LayoutParams.WRAP_CONTENT); lp.weight = 1;
			 * imageView1.setLayoutParams(lp);
			 * imageView1.setImageResource(ids[i]);
			 * imageView1.setOnClickListener(onClickListener);
			 * browsesuncat.addView(imageView1); }else{ //ImageView imageView =
			 * new ImageView(mContext);
			 * //imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			 * LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
			 * LinearLayout.LayoutParams.WRAP_CONTENT); lp.setMargins(50, 50, 0,
			 * 0); lp.weight = 1; imageView.setLayoutParams(lp);
			 * imageView.setImageResource(ids[i]);
			 * imageView.setOnClickListener(onClickListener);
			 * browsesuncat.addView(imageView); }
			 */

		}

		/*
		 * GridView mGridView = (GridView)
		 * convertView.findViewById(R.id.gridview); mGridView.setNumColumns(2);
		 * mGridView.setAdapter(new ImageAdapter(mContext, ids));
		 */
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}
