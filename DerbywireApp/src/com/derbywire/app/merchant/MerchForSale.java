package com.derbywire.app.merchant;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import com.derbywire.app.R;
import com.derbywire.app.profiles.ImageFetcher;
import com.derbywire.app.profiles.RecyclingImageView;

public class MerchForSale extends Activity {
	private GridView mGridview;
	private ImageAdapter mAdapter;
	private ImageFetcher mImageFetcher;
	private int mImageThumbSize;
	private Context mContext = null;
	private EditText mSearchbar = null;
	private ImageView mSearchicon = null;

	private static int[] imagelist = new int[] { R.drawable.tshirt_1,
			R.drawable.tshirt_2, R.drawable.tshirt_3, R.drawable.tshirt_4,
			R.drawable.tshirt_5 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.merch_sale);
		mContext = this;
		mGridview = (GridView) findViewById(R.id.grid);
		mSearchbar = (EditText) findViewById(R.id.search_bar);
		mSearchicon = (ImageView) findViewById(R.id.search_icon);
		mSearchbar.setVisibility(View.INVISIBLE);
		setResourcesToGridView();
		mSearchicon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mSearchbar.getVisibility() == View.INVISIBLE) {
					mSearchbar.setVisibility(View.VISIBLE);
					mSearchbar.requestFocus();
				} else {
					mSearchbar.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	public void setResourcesToGridView() {
		mAdapter = new ImageAdapter();
		mImageThumbSize = getResources().getDimensionPixelSize(
				R.dimen.image_thumbnail_size);
		mImageFetcher = new ImageFetcher(mContext, mImageThumbSize);
		mImageFetcher.setLoadingImage(R.drawable.empty_photo);
		mGridview.setAdapter(mAdapter);

	}

	private class ImageAdapter extends BaseAdapter {
		public ImageAdapter() {
			super();
		}

		@Override
		public int getCount() {
			return imagelist.length;
		}

		@Override
		public Object getItem(int position) {
			return imagelist[position];
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup container) {
			ImageView imageView;
			if (convertView == null) {
				imageView = new RecyclingImageView(mContext);
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			} else { // Otherwise re-use the converted view
				imageView = (ImageView) convertView;
			}
			/*
			 * mImageFetcher.loadImage(Images.imageThumbUrls[position -
			 * mNumColumns], imageView);
			 */
			imageView.setImageResource(imagelist[position]);
			return imageView;
		}
	}

}
