package com.derbywire.app.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cogniflex on 18/4/15.
 */
public class UserProfile implements Serializable {

	public static final String KEY_PROFILE_OBJECT = "PROFILE_OBJECT";

	public static final String FIRST_NAME = "firstname";
	public static final String LAST_NAME = "lastname";
	public static final String EMAIL = "email";
	public static final String API_KEY = "apiKey";
	public static final String ACC_ID = "account_id";
	public static final String IMAGE = "image";
	public static final String MERCHANT_JSON = "merchant_json";

	private int account_id = 0;
	private String first_name = "";
	private String last_name = "";
	private String email = "";
	private String password = "";
	private String image = "";
	private String apiKey = "";

	private String country = "";
	private String zip = "";

	private MerchantInfo merchant_info;

	public static class MerchantInfo {

		private String account_name = "";
		private String account_number = "";
		private String routing_number = "";
		private String phone_number = "";
		private String street = "";
		private String state = "";
		private String country = "";
		private String postal_code = "";
		private List<String> coverphoto = new ArrayList<String>();

		public MerchantInfo() {

		}

		public void setAccountName(String value) {
			account_name = value;
		}

		public void setAccountNumber(String value) {
			account_number = value;
		}

		public void setRoutingNumber(String value) {
			routing_number = value;
		}

		public void setPhone(String value) {
			phone_number = value;
		}

		public void setStreet(String value) {
			street = value;
		}

		public void setState(String value) {
			state = value;
		}

		public void setCountry(String value) {
			country = value;
		}

		public void setPostalCode(String value) {
			postal_code = value;
		}

		public void setCoverphoto(List<String> value) {
			coverphoto = value;
		}

		public String getAccountName() {
			return account_name;
		}

		public String getAccountNumber() {
			return account_number;
		}

		public String getRoutingNumber() {
			return routing_number;
		}

		public String getPhone() {
			return phone_number;
		}

		public String getStreet() {
			return street;
		}

		public String getState() {
			return state;
		}

		public String getCountry() {
			return country;
		}

		public String getPostalCode() {
			return postal_code;
		}

		public List<String> getCoverphoto() {
			return coverphoto;
		}

	}

	public UserProfile() {

	}

	public int getAccountId() {
		return account_id;
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getFirstName() {
		return first_name;
	}

	public String getLastName() {
		return last_name;
	}

	public String getEmail() {
		return email;
	}

	public String getCountry() {
		return country;
	}

	public String getZip() {
		return zip;
	}

	public String getPassword() {
		return password;
	}

	public String getImage() {
		return image;
	}

	public void setAccountId(int value) {
		this.account_id = value;
	}

	public void setFirstName(String value) {
		this.first_name = value;
	}

	public void setLastName(String value) {
		this.last_name = value;
	}

	public void setEmail(String value) {
		this.email = value;
	}

	public void setCountry(String value) {
		this.country = value;
	}

	public void setZipCode(String value) {
		this.zip = value;
	}

	public void setPassword(String value) {
		this.password = value;
	}

	public void setImage(String value) {
		this.image = value;
	}

	public void setApiKey(String value) {
		apiKey = value;
	}

	public void setMerchantInfo(MerchantInfo merchantInfo) {
		merchant_info = merchantInfo;
	}

	public MerchantInfo getMerchantInfo() {
		return merchant_info;
	}
}
