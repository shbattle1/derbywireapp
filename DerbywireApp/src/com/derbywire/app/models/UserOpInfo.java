package com.derbywire.app.models;

import java.util.List;

/**
 * Created by cogniflex on 2/5/15.
 */
public class UserOpInfo {

	public static final String OP_INFO_JSON = "op_info_json";

	public static final String KEY_OP_INFO_OBJECT = "OP_INFO_OBJECT";

	private String op_profile_id = "";
	private List<CardItem> op_cards;

	public UserOpInfo(String opProfileId, List<CardItem> cards) {
		this.op_profile_id = opProfileId;
		this.op_cards = cards;
	}

	public String getProfileID() {
		return op_profile_id;
	}

	public List<CardItem> getCards() {
		return op_cards;
	}
}
