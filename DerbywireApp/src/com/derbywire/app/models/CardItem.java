package com.derbywire.app.models;

/**
 * Created by cogniflex on 18/4/15.
 */
public class CardItem {

	public static final String TAG = CardItem.class.getSimpleName();

	private String card_id = "";
	private String card_type = "";
	private String last_digits = "";
	private boolean is_default = false;

	public CardItem(String opCardId, String cardType, String lastDigits, boolean isDefault) {
		this.card_id = opCardId;
		this.card_type = cardType;
		this.last_digits = lastDigits;
		this.is_default = isDefault;
	}

	public String getCardId() {
		return card_id;
	}

	public String getLastDigits() {
		return last_digits;
	}

	public String getCardType() {
		return card_type;
	}

	public boolean isDefault() {
		return is_default;
	}

}
