package com.derbywire.app.models;

/**
 * Created by cogniflex on 18/4/15.
 */
public class UserCard {

	public static final String TAG = UserCard.class.getSimpleName();

	private String card_number = "";
	private String cvv_code = "";
	private int exp_month = 0;
	private int exp_year = 0;

	public UserCard(String cardNumber, String cvv, int expMonth, int expYear) {
		this.card_number = cardNumber;
		this.cvv_code = cvv;
		this.exp_month = expMonth;
		this.exp_year = expYear;
	}

	public String getCardNumber() {
		return card_number;
	}

	public String getCvv() {
		return cvv_code;
	}

	public int getExpMonth() {
		return exp_month;
	}

	public int getExpYear() {
		return exp_year;
	}
}
