// TELECA-FILE (HelpHub)
/*
 * This Class should be used for situation if rightIndicator position is needed and
 * expanded group should be always on top of ExpandableListView
 */

package com.derbywire.app.uploads;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;

import com.derbywire.app.R;

public class UploadExpandableListView extends ExpandableListView {

	private Context mContext;
	private boolean mExpandedGroupTop;
	private boolean mRightIndicatorPosition;

	public UploadExpandableListView(Context context) {
		super(context);
		mContext = context;
	}

	public UploadExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);

		final TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.UploadExpandableListView, 0, 0);

		mContext = context;
		mRightIndicatorPosition = a.getBoolean(
				R.styleable.UploadExpandableListView_rightIndicator, false);
		mExpandedGroupTop = a.getBoolean(
				R.styleable.UploadExpandableListView_expandedGroupTop, false);

		// Set indicator position
		setIndicatorPosition();
		// Set listeners
		setListeners();
	}

	public UploadExpandableListView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);

		final TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.UploadExpandableListView, defStyle, 0);

		mContext = context;
		mRightIndicatorPosition = a.getBoolean(
				R.styleable.UploadExpandableListView_rightIndicator, false);
		mExpandedGroupTop = a.getBoolean(
				R.styleable.UploadExpandableListView_expandedGroupTop, false);

		mContext = context;

		// Set indicator position
		setIndicatorPosition();
		// Set listeners
		setListeners();
	}

	private OnGroupExpandListener mGroupExpandListener = new OnGroupExpandListener() {

		@Override
		public void onGroupExpand(int groupPosition) {

			UploadExpandableListAdapter callAdapter = (UploadExpandableListAdapter) getExpandableListAdapter();
			int groupPos = 0;
			int groupCount = callAdapter.getGroupCount();

			while (groupPos < groupCount) {

				if (groupPos != groupPosition) {
					collapseGroup(groupPos);
				}

				groupPos++;
			}

			// Store expanded group position
			callAdapter.setExpandedGroupPosition(groupPosition);
		}
	};

	private OnGroupCollapseListener mGroupCollapseListener = new OnGroupCollapseListener() {

		@Override
		public void onGroupCollapse(int groupPosition) {

			UploadExpandableListAdapter gestureAdapter = (UploadExpandableListAdapter) getExpandableListAdapter();
			gestureAdapter.resetExpandedGroupPosition(groupPosition);
		}
	};

	private OnGroupClickListener mGroupClickListener = new OnGroupClickListener() {

		@Override
		public boolean onGroupClick(ExpandableListView parent, View v,
				int groupPosition, long id) {

			if (isGroupExpanded(groupPosition)) {
				collapseGroup(groupPosition);
			} else {
				expandGroup(groupPosition);
			}

			// Play sound on click
			playSoundEffect(SoundEffectConstants.CLICK);

			// Move new expanded group to top of parent view
			if (isGroupExpanded(groupPosition) && mExpandedGroupTop) {
				setSelectionFromTop(groupPosition, 0);
			}

			return true;
		}
	};

	private void setListeners() {
		// Set on expand listener
		setOnGroupExpandListener(mGroupExpandListener);
		// Set on collapse listener
		setOnGroupCollapseListener(mGroupCollapseListener);
		// Set on group click listener
		setOnGroupClickListener(mGroupClickListener);
	}

	private void setIndicatorPosition() {
		int left = 0;
		int right = 0;
		float width = 0;
		float marginRight = 0;

		width = getResources().getDimension(
				R.dimen.helphub_learn_item_fold_image_width);
		marginRight = getResources().getDimension(
				R.dimen.helphub_learn_item_fold_image_marginRight);

		// If we set right position of indicator, then move it to right
		// will be left by default
		if (mRightIndicatorPosition) {
			final Point displaySize = new Point();
			WindowManager window = (WindowManager) mContext
					.getSystemService(Context.WINDOW_SERVICE);

			window.getDefaultDisplay().getSize(displaySize);

			left = displaySize.x - (int) (width + marginRight);
			right = displaySize.x - (int) (marginRight);

			setIndicatorBounds(left, right);
		} else {

			left = 0;
			right = (int) (width + marginRight);
		}

		setIndicatorBounds(left, right);
	}
}