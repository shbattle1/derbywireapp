package com.derbywire.app.uploads;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.derbywire.app.R;

public class FragVideoUpload extends Fragment implements OnClickListener {

	public static final int VIDEO_REQUEST = 0x01;
	private ImageView img_captured;
	private Button btnCapture_mode;
	private Button btnDone;

	private File VIDEO_FILE;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater
				.inflate(R.layout.frag_upload_camera, container, false);

		initViews(v);

		btnCapture_mode.setOnClickListener(this);
		btnDone.setOnClickListener(this);

		btnCapture_mode.setBackground(getResources().getDrawable(
				R.drawable.take_video_selector));

		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mUpdateUIReceiver,
				new IntentFilter(CaptureActivity.ACTION_VIDEO_CAPTURED));

		return v;
	}

	private void initViews(View v) {

		btnCapture_mode = (Button) v.findViewById(R.id.btnCapture_mode);
		btnDone = (Button) v.findViewById(R.id.btnDone);
		img_captured = (ImageView) v.findViewById(R.id.img_captured);
	}

	private BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// do Stuff
			System.out.println("Video captured in mUpdateUIReceiver");
			/*
			 * String path = intent.getStringExtra("path"); if (path != null) {
			 * VIDEO_FILE = new File(path);
			 */
			if (VIDEO_FILE != null && VIDEO_FILE.exists()) {
				Bitmap bitmap = getBitmapIfExists(VIDEO_FILE.getAbsolutePath());
				img_captured.setImageBitmap(bitmap);
			} else {

			}
			// }
		}
	};

	private Bitmap getBitmapIfExists(String path) {
		Bitmap bmp = null;
		bmp = ThumbnailUtils.createVideoThumbnail(path,
				MediaStore.Images.Thumbnails.MINI_KIND);
		return bmp;
	}

	public static FragVideoUpload newInstance() {

		FragVideoUpload f = new FragVideoUpload();
		return f;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btnCapture_mode:

			Boolean isSDPresent = android.os.Environment
					.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED);
			String timeid;
			if (isSDPresent) {
				// yes SD-card is present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File(Environment
						.getExternalStorageDirectory().toString()
						+ "/Debrywire/Videos");
				folder.mkdirs();
				VIDEO_FILE = new File(folder, timeid + ".mp4");
			} else {
				// No SD-card not present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File("/Debrywire/Videos");
				folder.mkdirs();
				VIDEO_FILE = new File(folder, timeid + ".mp4");
			}

			System.out.println("PHOTO_FILE " + VIDEO_FILE);
			Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

			Uri videoUri = Uri.fromFile(VIDEO_FILE);

			intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
			getActivity().startActivityForResult(intent, VIDEO_REQUEST);

			/*
			 * Intent i = new
			 * Intent("android.provider.MediaStore.ACTION_VIDEO_CAPTURE");
			 * getActivity().startActivityForResult(i,VIDEO_REQUEST);
			 */

			break;

		case R.id.btnDone:
			Intent n = new Intent(getActivity(), UploadActivity.class);
			n.putExtra("FILENAME", VIDEO_FILE);
			getActivity().startActivity(n);
			break;

		default:
			break;
		}
	}
}
