package com.derbywire.app.uploads;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.derbywire.app.R;

public class FragImageUpload extends Fragment implements OnClickListener {

	public static final int CAMERA_REQUEST = 0x00;
	private ImageView img_captured;
	private Button btnCapture_mode;
	private Button btnDone;

	private File PHOTO_FILE;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater
				.inflate(R.layout.frag_upload_camera, container, false);

		initViews(v);

		btnCapture_mode.setOnClickListener(this);
		btnDone.setOnClickListener(this);

		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mUpdateUIReceiver,
				new IntentFilter(CaptureActivity.ACTION_IMAGE_CAPTURED));

		return v;
	}

	private void initViews(View v) {
		btnCapture_mode = (Button) v.findViewById(R.id.btnCapture_mode);
		btnDone = (Button) v.findViewById(R.id.btnDone);
		img_captured = (ImageView) v.findViewById(R.id.img_captured);
	}

	private BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// do Stuff
			System.out.println("Photo captured in mUpdateUIReceiver");
			if (PHOTO_FILE != null && PHOTO_FILE.exists()) {
				Bitmap bitmap = BitmapFactory.decodeFile(PHOTO_FILE
						.getAbsolutePath());
				img_captured.setImageBitmap(bitmap);
			}
		}
	};

	public static FragImageUpload newInstance() {

		FragImageUpload f = new FragImageUpload();
		return f;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btnCapture_mode:

			Boolean isSDPresent = android.os.Environment
					.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED);
			String timeid;
			if (isSDPresent) {
				// yes SD-card is present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File(Environment
						.getExternalStorageDirectory().toString()
						+ "/Debrywire/Images");
				folder.mkdirs();
				PHOTO_FILE = new File(folder, timeid + ".jpg");
			} else {
				// No SD-card not present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File("/Debrywire/Images");
				folder.mkdirs();
				PHOTO_FILE = new File(folder, timeid + ".jpg");
			}

			System.out.println("PHOTO_FILE " + PHOTO_FILE);
			Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
			i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(PHOTO_FILE));
			getActivity().startActivityForResult(i, CAMERA_REQUEST);

			break;

		case R.id.btnDone:
			Intent n = new Intent(getActivity(), UploadActivity.class);
			n.putExtra("FILENAME", PHOTO_FILE);
			getActivity().startActivity(n);
			break;

		default:
			break;
		}
	}
}
