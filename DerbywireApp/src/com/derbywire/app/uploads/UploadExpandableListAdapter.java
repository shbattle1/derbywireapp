// TELECA-FILE (HelpHub)

package com.derbywire.app.uploads;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.derbywire.app.R;

public class UploadExpandableListAdapter extends BaseExpandableListAdapter {

	/**
	 * Icons of groups
	 */
	protected TypedArray mGroupIcons = null;

	/**
	 * Titles of groups
	 */
	protected String[] mGroupTitles = null;

	/**
	 * Determine child layout for each group
	 */
	protected TypedArray mChildLayouts = null;

	/**
	 * Determine child for each group Will be used instead of child layout
	 */
	protected ArrayList<View> mChildren = null;
	private ArrayList<View> mLocalChildren = null;
	protected SparseArray<Parcelable> mChildrenStates = null;

	private Context mContext;
	private int mCurrentExpandedGroupPosition = -1;

	public UploadExpandableListAdapter(Context context, int ItemsTitle,
			int ItemsLayout) {
		mContext = context;

		if (mContext != null) {

			mGroupTitles = mContext.getResources().getStringArray(ItemsTitle);
			mChildLayouts = mContext.getResources().obtainTypedArray(
					ItemsLayout);

			initChildren(null);
		}
	}

	public UploadExpandableListAdapter(Context context, int ItemsTitle,
			int ItemsLayout, int ItemsIcons) {
		mContext = context;

		if (mContext != null) {

			mGroupIcons = mContext.getResources().obtainTypedArray(ItemsIcons);
			mGroupTitles = mContext.getResources().getStringArray(ItemsTitle);
			mChildLayouts = mContext.getResources().obtainTypedArray(
					ItemsLayout);

			initChildren(null);
		}
	}

	public UploadExpandableListAdapter(Context context, int ItemsTitle,
			int ItemsIcons, ArrayList<View> children) {
		mContext = context;

		if (mContext != null) {

			if (ItemsIcons != 0) {
				mGroupIcons = mContext.getResources().obtainTypedArray(
						ItemsIcons);
			}

			mGroupTitles = mContext.getResources().getStringArray(ItemsTitle);
			initChildren(children);
		}
	}

	protected void initChildren(ArrayList<View> children) {
		int index = 0;
		int count = getGroupCount();
		ViewStub view = new ViewStub(mContext);

		if (children != null) {
			mChildren = children;
		} else {
			mLocalChildren = new ArrayList<View>();

			while (index < count) {
				mLocalChildren.add(index, view);
				index++;
			}
		}
	}

	/**
	 * Will save current adapter state Should be invoked from Activity
	 * onSaveInstanceState for correctly retrieving adapter data
	 *
	 * @param outState
	 */
	public void saveAdapterState(Bundle outState) {
		int index = 0;
		outState.putInt("mCurrentExpandedGroupPosition",
				mCurrentExpandedGroupPosition);

		SparseArray<Parcelable> container = new SparseArray<Parcelable>();
		// We save state of local children only
		if (mLocalChildren != null) {
			while (index < mLocalChildren.size()) {
				mLocalChildren.get(index).saveHierarchyState(container);
				index++;
			}
		}

		outState.putSparseParcelableArray("children_state", container);
	}

	/**
	 * Will load previously created adapter state Should be invoked from
	 * Activity onRestoreInstanceState for correctly retrieving adapter data
	 *
	 * @param savedState
	 */
	public void loadAdapterState(Bundle savedState) {
		mCurrentExpandedGroupPosition = savedState.getInt(
				"mCurrentExpandedGroupPosition", -1);
		mChildrenStates = savedState.getSparseParcelableArray("children_state");
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		if (observer != null) {
			super.unregisterDataSetObserver(observer);
		}
	}

	/**
	 * <b>This is method deprecated.</b>
	 * <p>
	 * getChildView should be used instead
	 */
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	public int getExpandedGroupPosition() {
		return mCurrentExpandedGroupPosition;
	}

	public void setExpandedGroupPosition(int groupPosition) {
		mCurrentExpandedGroupPosition = groupPosition;
	}

	public boolean resetExpandedGroupPosition(int groupPosition) {
		boolean retval = false;

		// Reset position of currently expanded group if it collapsing
		if (mCurrentExpandedGroupPosition == groupPosition) {

			mCurrentExpandedGroupPosition = -1;
			retval = true;
		}

		// With collapse group reset saved state,
		// because we can have one group expanded only
		mChildrenStates = null;
		return retval;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getGroupPosition(int groupId) {
		return groupId;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		View view = null;

		// Will inflate list view or general view of child its depends on
		// existing of children list
		if (mChildren != null && mChildren.size() > 0) {
			try {
				view = mChildren.get(groupPosition);
			} catch (IndexOutOfBoundsException ie) {
			}
		}

		if (view == null && mChildLayouts != null) {
			int LayoutResId = mChildLayouts.getResourceId(groupPosition, 0);
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = vi.inflate(LayoutResId, null);

			if (view != null) {
				view.setId(groupPosition);

				if (mChildrenStates != null
						&& mLocalChildren != null
						&& mLocalChildren.get(groupPosition).getId() != view
								.getId()) {
					view.restoreHierarchyState(mChildrenStates);
				}

				// Local children fills with actual data to save state in future
				if (mLocalChildren != null) {
					mLocalChildren.set(groupPosition, view);
				}
			}
		}

		return view;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int childrenCount = 0;

		// Actually will return children count required group
		if (groupPosition < getGroupCount()) {
			childrenCount = 1;
		}

		return childrenCount;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		return mGroupTitles.length;
	}

	/**
	 * Will return required group view
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = convertView;
		GroupViewCache item = null;

		if (view == null) {
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// Inflate items of group depends from icons set
			if (mGroupIcons == null) {
				view = vi.inflate(R.layout.upload_list_item, null);
			}

			view.setId(groupPosition);
			view.setTag(new GroupViewCache(view));
		}

		item = (GroupViewCache) view.getTag();

		if (item != null) {
			// Set icon for item of expandable list if it present
			if (mGroupIcons != null) {

				if (mGroupIcons.getResourceId(groupPosition, 0) != 0) {
					item.mIconImageView.setImageDrawable(mGroupIcons
							.getDrawable(groupPosition));
				} else {
					item.mIconImageView.setImageDrawable(null);
				}
			}

			item.mTitleTextView.setText(mGroupTitles[groupPosition]);

			if (isExpanded) {
				item.mTitleTextView.setTypeface(Typeface.DEFAULT_BOLD);
				item.mFoldImageView
						.setBackgroundResource(R.drawable.expandable_fold_close_btn);
			} else {
				item.mTitleTextView.setTypeface(Typeface.DEFAULT);
				item.mFoldImageView
						.setBackgroundResource(R.drawable.expandable_fold_open_btn);
			}
		}

		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

	private class GroupViewCache {
		ImageView mFoldImageView = null;
		TextView mTitleTextView = null;
		ImageView mIconImageView = null;

		public GroupViewCache(View view) {

			if (mGroupIcons != null) {
				mIconImageView = (ImageView) view
						.findViewById(R.id.expandable_list_item_fold);
			}

			mFoldImageView = (ImageView) view
					.findViewById(R.id.expandable_list_item_fold);
			mTitleTextView = (TextView) view
					.findViewById(R.id.expandable_list_item_title);
		}
	}
}