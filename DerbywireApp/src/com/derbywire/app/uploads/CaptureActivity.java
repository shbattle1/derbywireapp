package com.derbywire.app.uploads;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.derbywire.app.R;

public class CaptureActivity extends FragmentActivity implements
		OnClickListener {

	ViewPager pager;

	private ImageView img_mode_audio;
	private ImageView img_mode_video;
	private ImageView img_mode_camera;

	public static String ACTION_IMAGE_CAPTURED = "COM.DERBYWIRE.UPLOADS.IMAGE_CAPTURED";
	public static String ACTION_VIDEO_CAPTURED = "COM.DERBYWIRE.UPLOADS.VIDEO_CAPTURED";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * Set the Activity to occupy full screen
		 */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_capture);

		/**
		 * Init UI
		 */

		init();

		img_mode_audio.setOnClickListener(this);
		img_mode_video.setOnClickListener(this);
		img_mode_camera.setOnClickListener(this);

		/**
		 * Setting selected image by default
		 */
		img_mode_audio.setImageDrawable(getResources().getDrawable(
				R.drawable.upload_image_audio_selected));

		pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				// TODO Auto-generated method stub
				switch (pos) {
				case 0:

					img_mode_audio.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_audio_selected));
					img_mode_camera.setImageDrawable(getResources()
							.getDrawable(R.drawable.upload_image_camera));
					img_mode_video.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_video));

					break;

				case 1:

					img_mode_audio.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_audio));
					img_mode_camera.setImageDrawable(getResources()
							.getDrawable(R.drawable.upload_image_camera));
					img_mode_video.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_video_selected));

					break;

				case 2:

					img_mode_audio.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_audio));
					img_mode_camera.setImageDrawable(getResources()
							.getDrawable(
									R.drawable.upload_image_camera_selected));
					img_mode_video.setImageDrawable(getResources().getDrawable(
							R.drawable.upload_image_video));

					break;

				default:
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void init() {
		pager = (ViewPager) findViewById(R.id.viewPager);
		img_mode_audio = (ImageView) findViewById(R.id.img_mode_audio);
		img_mode_video = (ImageView) findViewById(R.id.img_mode_video);
		img_mode_camera = (ImageView) findViewById(R.id.img_mode_camera);
	}

	private class MyPagerAdapter extends FragmentPagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {

			case 0:
				return FragAudioUpload.newInstance();
			case 1:
				return FragVideoUpload.newInstance();
			case 2:
				return FragImageUpload.newInstance();
			default:
				return FragAudioUpload.newInstance();
			}
		}

		@Override
		public int getCount() {
			return 3;
		}
	}

	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		// imagePath = cursor.getString(column_index);

		return cursor.getString(column_index);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.img_mode_audio:
			pager.setCurrentItem(0, true);
			break;
		case R.id.img_mode_camera:
			pager.setCurrentItem(1, true);
			break;
		case R.id.img_mode_video:
			pager.setCurrentItem(2, true);
			break;

		default:
			pager.setCurrentItem(0, true);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		System.out.println("the code is catch");

		if (resultCode == RESULT_OK)
			switch (requestCode) {
			case FragImageUpload.CAMERA_REQUEST:
				LocalBroadcastManager.getInstance(this).sendBroadcast(
						new Intent(ACTION_IMAGE_CAPTURED));
				break;

			case FragVideoUpload.VIDEO_REQUEST:

				LocalBroadcastManager.getInstance(this).sendBroadcast(
						new Intent(ACTION_VIDEO_CAPTURED));
				break;

			default:
				break;
			}
	}
}
