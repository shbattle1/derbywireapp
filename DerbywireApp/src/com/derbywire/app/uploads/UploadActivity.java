package com.derbywire.app.uploads;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.customview.WidgetTagAdd;

public class UploadActivity extends Activity implements OnClickListener {

	private LinearLayout tagLayout;

	private ImageView img_upload_file;
	private ImageView img_upload_image;
	private WidgetTagAdd img_add_tag;
	private ImageView mAudio;

	private Spinner spn_categeory;
	private UploadExpandableListView mListView;

	private Button btnDone;

	private String[] itemArr = { "Select", "item 01", "item 02", "item 03" };
	private String titleStr;
	private int categeorySelectedPos;

	private ArrayAdapter<CharSequence> adapterCateogry;

	private static int PICK_IMAGE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * Set the Activity to occupy full screen
		 */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.expandable_layout);
		init();
	}

	private void init() {
		mListView = (UploadExpandableListView) findViewById(R.id.expandable_list);
		mListView.setAdapter(new UploadExpandableListAdapter(this,
				R.array.learn_unlock_item_title,
				R.array.learn_unlock_item_layout));
		mListView
				.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

					@Override
					public boolean onChildClick(ExpandableListView parent,
							View v, int groupPosition, int childPosition,
							long id) {
						// Log.d("Senthil",
						// "view:"+v.getId()+"child pos: "+childPosition +
						// "group pos: "+groupPosition);
						return false;
					}
				});
	}

	private void populateCategeory() {
		adapterCateogry = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_list_item_1, itemArr);
		adapterCateogry
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn_categeory.setAdapter(adapterCateogry);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.img_upload_file:

			break;

		case R.id.img_upload_image:

			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

			break;
		case R.id.upload_audio:
			DerbyWireUtils.startActivity(this, ActivityType.CAPTUREACTIVITY);
			break;

		case R.id.btnDone:
			break;

		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_IMAGE && data != null && data.getData() != null) {
			Uri _uri = data.getData();

			// User had pick an image.
			Cursor cursor = getContentResolver()
					.query(_uri,
							new String[] { android.provider.MediaStore.Images.ImageColumns.DATA },
							null, null, null);
			cursor.moveToFirst();

			// Link to the image
			final String imageFilePath = cursor.getString(0);
			cursor.close();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void onAudioClick(View v) {
		Log.d("Senthil", "View id: " + v.getId());
		DerbyWireUtils.startActivity(this, ActivityType.CAPTUREACTIVITY);
	}

	public void onVideoClick(View v) {
		Log.d("Senthil", "View id: " + v.getId());
		DerbyWireUtils.startActivity(this, ActivityType.CAPTUREACTIVITY);
	}

	public void onImageClick(View v) {
		Log.d("Senthil", "View id: " + v.getId());
		DerbyWireUtils.startActivity(this, ActivityType.CAPTUREACTIVITY);
	}

	public void onChooseImage(View v) {
		Log.d("Senthil", "View id: " + v.getId());
	}

	public void onChooseFile(View v) {
		Log.d("Senthil", "View id: " + v.getId());
	}
}
