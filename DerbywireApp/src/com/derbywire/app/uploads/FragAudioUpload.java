package com.derbywire.app.uploads;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.derbywire.app.R;

public class FragAudioUpload extends Fragment implements OnClickListener {

	protected static final int PROGRESS_UPDATE = 0;
	private Button btnRecord;
	private Button btnSave;
	private ProgressBar progressBar;
	private boolean isRecording;
	MediaRecorder recorder;
	private TextView tvStartTime;
	private File AUDIO_FILE;
	private long startTime;
	CountDownTimer mCountDownTimer;
	private int TOTAL_TIME_IN_SECS = 18000;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_upload_audio, container, false);

		InitViews(v);

		btnRecord.setOnClickListener(this);
		btnSave.setOnClickListener(this);

		mCountDownTimer = new CountDownTimer(TOTAL_TIME_IN_SECS, 1000) { // adjust
																			// the
																			// milli
																			// seconds
																			// here

			@Override
			public void onTick(long millisUntilFinished) {
				millisUntilFinished = TOTAL_TIME_IN_SECS - millisUntilFinished;
				System.out
						.println(TimeUnit.MILLISECONDS
								.toMinutes(millisUntilFinished)
								+ ","
								+ TimeUnit.MILLISECONDS
										.toSeconds(millisUntilFinished)
								+ "-"
								+ (TimeUnit.MILLISECONDS
										.toSeconds(millisUntilFinished) * 100 / (TOTAL_TIME_IN_SECS / 100)));
				tvStartTime
						.setText(""
								+ String.format(
										"%02d:%02d",
										TimeUnit.MILLISECONDS
												.toMinutes(millisUntilFinished),
										TimeUnit.MILLISECONDS
												.toSeconds(millisUntilFinished)
												- TimeUnit.MINUTES
														.toSeconds(TimeUnit.MILLISECONDS
																.toMinutes(millisUntilFinished))));
				// tvStartTime.setText(""+TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));
				progressBar
						.setProgress((int) (TimeUnit.MILLISECONDS
								.toSeconds(millisUntilFinished) * 100 / (TOTAL_TIME_IN_SECS / 100)));
			}

			@Override
			public void onFinish() {
				// _tv.setText("done!");``````````
				if (recorder != null) {
					recorder.stop();
					recorder.release();
					btnRecord.setEnabled(true);
				}
			}
		};

		return v;
	}

	private void InitViews(View v) {
		recorder = new MediaRecorder();
		btnRecord = (Button) v.findViewById(R.id.btnRecord);
		btnSave = (Button) v.findViewById(R.id.btnSave);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
		tvStartTime = (TextView) v.findViewById(R.id.tvStartTime);
	}

	public static FragAudioUpload newInstance() {

		FragAudioUpload f = new FragAudioUpload();

		return f;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnRecord:
			startTime = Calendar.getInstance().getTimeInMillis();
			isRecording = true;
			btnRecord.setEnabled(false);
			Boolean isSDPresent = android.os.Environment
					.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED);
			String timeid;
			mCountDownTimer.start();
			if (isSDPresent) {
				// yes SD-card is present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File(Environment
						.getExternalStorageDirectory().toString()
						+ "/Debrywire/Audio");
				folder.mkdirs();
				AUDIO_FILE = new File(folder, timeid + ".mp3");
			} else {
				// No SD-card not present
				Long tsLong = System.currentTimeMillis() / 1000;
				timeid = tsLong.toString();
				File folder = new File("/Debrywire/Audio");
				folder.mkdirs();
				AUDIO_FILE = new File(folder, timeid + ".mp3");
			}
			try {
				recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
				recorder.setOutputFile(AUDIO_FILE.getAbsolutePath());
				recorder.prepare();
				recorder.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;

		case R.id.btnSave:

			if (recorder != null && isRecording) {
				isRecording = false;
				recorder.stop();
				recorder.release();
			}
			mCountDownTimer.cancel();
			btnRecord.setEnabled(true);

			Intent n = new Intent(getActivity(), UploadActivity.class);
			n.putExtra("FILENAME", AUDIO_FILE);
			getActivity().startActivity(n);

			break;

		default:
			break;
		}
	}
}
