package com.derbywire.app.managers;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.common.KairosObjectRequest;

/**
 * Created by cogniflex on 3/5/15.
 */
public class KairosApiManager {

	private static final String FR_APP_ID = "";
	private static final String FR_APP_KEY = "";

	public interface KairosErrorListener {
		void onError(String msg);
	}

	public class Credentials {

		public static final String KEY_1 = "050891a5"; // ^^^^^^
		public static final String KEY_2 = "80e54bba30c706aba"; // -->
		public static final String KEY_3 = "38d734eae96bd2c"; // <--

		public Credentials() {

		}

		public String getAppKey() {
			return KEY_3 + KEY_2;
		}

		public String getAppId() {
			return KEY_1;
		}

	}

	public class REQUEST {
		public static final String TAG_ENROLL = "TAG_ENROLL";
		public static final String TAG_RECOGNIZE = "TAG_RECOGNIZE";

	}

	// Facial Recognition
	private static String FR_BASE_URL = "https://api.kairos.com/";
	public static String FR_ENROLL_URL = FR_BASE_URL + "enroll";
	public static String FR_RECOGNIZE_URL = FR_BASE_URL + "recognize";
	public static String FR_DETECT_URL = FR_BASE_URL + "detect";
	public static String FR_GALLERY_LIST_URL = FR_BASE_URL + "gallery/list_all";
	public static String FR_REMOVE_GALLERY = FR_BASE_URL
			+ "gallery/remove_subject";

	public class FIELDS {
		public static final String IMAGE = "image";
		public static final String SUBJECT_ID = "subject_id";
		public static final String GALLERY_NAME = "gallery_name";
		public static final String SELECTOR = "selector";
		public static final String SYMMETRIC_FILL = "symmetricFill";
		public static final String THRESHOLD = "threshold";
		public static final String MIN_HEAD_SCALE = "minHeadScale";

	}

	public class HEADERS {
		public static final String APP_ID = "app_id";
		public static final String APP_KEY = "app_key";
	}

	private static final String SELECTOR_MODE = "SETPOSE";

	private Context mContext;
	ApiManager.TimeOutListener mTimeOutListener;

	public KairosApiManager(Context context) {
		this.mContext = context;
		// this.mTimeOutListener = timeOutListener;
	}

	public void uploadPhotos(String userEmail, List<Bitmap> photosList,
			Response.Listener<Object> responseListener) {
		try {
			final Credentials credentials = new Credentials();

			String idPrefix = userEmail.substring(0, userEmail.indexOf("@"));

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.APP_ID, credentials.getAppId());
			headers.put(HEADERS.APP_KEY, credentials.getAppKey());

			JSONObject faceObject = new JSONObject();

			for (int i = 0; i < photosList.size(); i++) {
				faceObject.put(FIELDS.SUBJECT_ID, idPrefix);
				faceObject.put(FIELDS.GALLERY_NAME, idPrefix + "-gallery");
				faceObject.put(FIELDS.SELECTOR, SELECTOR_MODE);
				faceObject.put(FIELDS.SYMMETRIC_FILL, true);
				faceObject.put(FIELDS.IMAGE,
						DerbyWireUtils.getEncodedImage(photosList.get(i)));
				// faceObject.put(FIELDS.IMAGE,
				// "http://www.uni-regensburg.de/Fakultaeten/phil_Fak_II/Psychologie/Psy_II/beautycheck/english/durchschnittsgesichter/m(01-32)_gr.jpg");
				System.out.println("*** Image *** \n\n"
						+ faceObject.get(FIELDS.IMAGE)
						+ "\n\n*** Image *** \n\n");

				AppSession.getInstance().DEBUG(mContext,
						"creating kairos gallery for " + faceObject.toString());

				KairosObjectRequest jsObjRequest = new KairosObjectRequest(
						Request.Method.POST, FR_ENROLL_URL, headers,
						faceObject.toString(), responseListener,
						apiErrorListener);

				AppSession.getInstance().addToRequestQueue(mContext,
						jsObjRequest, REQUEST.TAG_ENROLL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recognize(String userEmail, Bitmap photo,
			Response.Listener<Object> responseListener) {
		try {
			final Credentials credentials = new Credentials();

			String idPrefix = userEmail.substring(0, userEmail.indexOf("@"));

			JSONObject faceObject = new JSONObject();

			faceObject.put(FIELDS.IMAGE, DerbyWireUtils.getEncodedImage(photo));
			faceObject.put(FIELDS.GALLERY_NAME, idPrefix + "-gallery");

			// faceObject.put(FIELDS.IMAGE,
			// "http://www.uni-regensburg.de/Fakultaeten/phil_Fak_II/Psychologie/Psy_II/beautycheck/english/durchschnittsgesichter/m(01-32)_gr.jpg");

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.APP_ID, credentials.getAppId());
			headers.put(HEADERS.APP_KEY, credentials.getAppKey());

			AppSession.getInstance().DEBUG(mContext,
					"Recognizing face for " + faceObject.toString());

			KairosObjectRequest jsObjRequest = new KairosObjectRequest(
					Request.Method.POST, FR_RECOGNIZE_URL, headers,
					faceObject.toString(), responseListener, apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
					REQUEST.TAG_RECOGNIZE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Response.ErrorListener apiErrorListener = new Response.ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError error) {
			try {
				NetworkResponse response = error.networkResponse;
				if (response != null && response.data != null) {
					if (response == null) {
						if (error.getClass().equals(TimeoutError.class)) {
							AppSession.getInstance().DisplayToast(mContext,
									"Oops. Timeout error!");
							mTimeOutListener.onTimeout();
						}

					} else {
						AppSession.getInstance().ERROR(mContext,
								error.toString());
						AppSession.getInstance().DisplayToast(mContext,
								error.getMessage().toString());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

}
