package com.derbywire.app.managers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.StrictMode;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.SettingsEditor;
import com.derbywire.app.models.UserOpInfo;
import com.derbywire.app.models.UserProfile;
import com.google.gson.Gson;

/**
 * Created by cogniflex on 30/4/15.
 */
public class OptimalPayments {

	private static final String BASE_URL = "https://api.test.netbanx.com/";
	public static final String ACC_NUMBER = "89983383";

	public static final String ACCESS_URL = BASE_URL + "customervault/monitor";
	public static final String PROFILE_URL = BASE_URL + "customervault/v1/profiles";

	public static final String ADDRESS_URL_SUFFIX = "/addresses";

	public static final String CARDS_URL_SUFFIX = "/cards";

	public static final String CREDIT_TO_CARD_URL_SUFFIX = "/standalonecredits";

	public static final String CARD_PAYMENTS_URL = BASE_URL + "cardpayments/v1/accounts/"; // <ACCOUNT_NUMBER>
	public static final String CARD_PAYMENTS_URL_SUFFIX = "/auths";

	public static final String DEFAULT_CARD = "DEFAULT_CARD";
	public static final String OP_PROFILE_ID = "OP_PROFILE_ID";

	public static final String MERCHANT_ACCOUNT_NUMBER = "1000031745";
	public static final String STORE_ID = "test";
	public static final String STORE_PWD = "test";

	public static final String DIRECT_DEBIT_URL = "https://webservices.test.optimalpayments.com/directdebitWS/DirectDebitServlet/v1";

	public static final String ACCOUNT_TYPE_PC = "PC";
	public static final String PAY_METHOD_WEB = "WEB";
	public static final String PAY_METHOD_PPD = "PPD";

	public static final String TEST_BANK_NAME = "Barclays";
	public static final String TEST_ROUTING_NUMBER = "207405";
	public static final String TEST_BANK_ACC_NUMBER = "12345676";
	public static final String TEST_CHECK_NUMBER = "12";
	public static final String TEST_PAYEE = "N F TURNER";
	public static final String TEST_STREET = "123 Main Street";
	public static final String TEST_DESCRIPTOR = "a descriptor";
	public static final String TEST_CITY = "Cambridge";
	public static final String TEST_COUNTRY = "GB";
	public static final String TEST_ZIP = "90210";

	private String errorMessage = "";

	public class Credentials {

		public static final String KEY_1 = "302d0214153c8e87c67d4a60d970"; // ^^^^^^
		public static final String KEY_2 = "devcentre4717"; // ####
		public static final String KEY_3 = "B-qa2-0-54b63b3b"; // *****
		public static final String KEY_4 = "eeb6778c102d0a5f02a0d708182610f481a";// -->
		public static final String KEY_5 = "ee895d01ba843ff535c302150084244"; // <--

		public Credentials() {

		}

		public String getAuthKey() {
			return KEY_1 + KEY_5 + KEY_4;
		}

		public String getUser() {
			return KEY_2;
		}

		public String getPwd() {
			return KEY_3;
		}
	}

	public class FIELDS {
		public static final String BILLING_ADDRESS_ID = "billing_address_id";
		public static final String MERCHANT_CUSTOMER_ID = "merchantCustomerId";
		public static final String MERCHANT_REF_NUMBER = "merchantRefNum";
		public static final String LOCALE = "locale";
		public static final String FNAME = "firstName";
		public static final String LNAME = "lastName";
		public static final String EMAIL = "email";
		public static final String LAST_DIGITS = "lastDigits";
		public static final String CARD_NUMBER = "cardNum";
		public static final String CVV = "cvv";
		public static final String CARD_EXP = "cardExpiry";
		public static final String CARD_TYPE = "cardType";
		public static final String EXP_MONTH = "month";
		public static final String EXP_YEAR = "year";
		public static final String UNIQUE_ID = "id";
		public static final String NICK_NAME = "nickName";
		public static final String COUNTRY = "country";
		public static final String ZIP_CODE = "zip";
		public static final String B_ADDRESS_ID = "billingAddressId";

		public static final String ACTIVE = "ACTIVE";
		public static final String PAYMENT_TOKEN = "paymentToken";
		public static final String AMOUNT = "amount";
		public static final String CARD = "card";
		public static final String SETTLE_WITH_AUTH = "settleWithAuth";

	}

	public class REQUEST {
		public static final String TAG_ACCESS = "TAG_ACCESS";
		public static final String TAG_PROFILE = "TAG_PROFILE";
		public static final String TAG_CARD = "TAG_CARD";
	}

	Context mContext;
	ApiManager.TimeOutListener mTimeOutListener;

	SettingsEditor mSettingsEditor;

	public OptimalPayments(Context context, ApiManager.TimeOutListener timeOutListener) {
		this.mContext = context;
		this.mTimeOutListener = timeOutListener;

		mSettingsEditor = new SettingsEditor(context);
	}

	public final void createCard(Response.Listener<JSONArray> responseListener) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void isServiceAccessible(Response.Listener<JSONObject> responseListener) {
		try {
			JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, ACCESS_URL, null,
					responseListener, this.apiErrorListener);
			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_ACCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createProfile(String merchantCustomerId, String locale, String fName, String lName, String email,
			Response.Listener<JSONObject> responseListener) {
		try {
			final Credentials credentials = new Credentials();

			Map<String, String> params = new HashMap<String, String>();
			params.put(FIELDS.MERCHANT_CUSTOMER_ID, merchantCustomerId);
			params.put(FIELDS.LOCALE, locale);
			params.put(FIELDS.FNAME, fName);
			params.put(FIELDS.LNAME, lName);
			params.put(FIELDS.EMAIL, email);
			AppSession.getInstance().DEBUG(mContext, "Data = " + new JSONObject(params));

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(PROFILE_URL, new JSONObject(params),
					responseListener, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_PROFILE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addAddress(String profileId, String nickName, String country, String zip,
			Response.Listener<JSONObject> responseListener) {
		try {
			final Credentials credentials = new Credentials();

			Map<String, String> params = new HashMap<String, String>();
			params.put(FIELDS.NICK_NAME, nickName);
			params.put(FIELDS.COUNTRY, country);
			params.put(FIELDS.ZIP_CODE, zip);

			AppSession.getInstance().DEBUG(mContext, "Data = " + new JSONObject(params));

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(PROFILE_URL + "/" + profileId + ADDRESS_URL_SUFFIX,
					new JSONObject(params), responseListener, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_PROFILE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createCard(String billingAddressId, String merchantCustomerID, String cardNumber, String cvv,
			int expMonth, int expYear, Response.Listener<JSONObject> responseListener) {
		try {
			final Credentials credentials = new Credentials();

			JSONObject params = new JSONObject();
			Map<String, Integer> dateObject = new HashMap<String, Integer>();

			try {
				params.put(FIELDS.CARD_NUMBER, cardNumber);
				params.put(FIELDS.CVV, cvv);
				dateObject.put(FIELDS.EXP_MONTH, expMonth);
				dateObject.put(FIELDS.EXP_YEAR, expYear);
				params.put(FIELDS.CARD_EXP, new JSONObject(dateObject));
				params.put(FIELDS.B_ADDRESS_ID, billingAddressId);

			} catch (Exception e) {
				e.printStackTrace();
			}
			AppSession.getInstance().DEBUG(mContext, "Data = " + params);

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(PROFILE_URL + "/" + merchantCustomerID
					+ CARDS_URL_SUFFIX, params, responseListener, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_CARD);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteCard(String merchantCustomerID, String cardid, Response.Listener<JSONObject> responseListener) {
		try {

			final Credentials credentials = new Credentials();

			AppSession.getInstance().DEBUG(mContext, "Delete card " + cardid);

			JsonObjectRequest transferRequest = new JsonObjectRequest(Request.Method.DELETE, PROFILE_URL + "/"
					+ merchantCustomerID + CARDS_URL_SUFFIX + "/" + cardid, null, responseListener, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};
			AppSession.getInstance().addToRequestQueue(mContext, transferRequest, REQUEST.TAG_CARD);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void chargeCard(final int amount, final Response.Listener<JSONObject> responseListener) {
		try {
			final Credentials credentials = new Credentials();
			UserOpInfo opInfo = new Gson().fromJson(mSettingsEditor.getStringValue(UserOpInfo.OP_INFO_JSON),
					UserOpInfo.class);

			String cardId = "";
			String defaultCard = "";

			for (int i = 0; i < opInfo.getCards().size(); i++) {
				defaultCard = mSettingsEditor.getStringValue(DEFAULT_CARD);
				if (defaultCard.equals(opInfo.getCards().get(i).getCardId())) {
					cardId = opInfo.getCards().get(i).getCardId();
					AppSession.getInstance().DEBUG(mContext,
							"Get payment token for" + opInfo.getCards().get(i).getLastDigits());
					break;
				}
			}

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(PROFILE_URL + "/" + opInfo.getProfileID()
					+ CARDS_URL_SUFFIX + "/" + cardId, null, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject jsonObject) {
					try {
						AppSession.getInstance().DEBUG(mContext, "Look up card info = " + jsonObject.toString());
						if (jsonObject.toString().contains(FIELDS.ACTIVE)
								&& jsonObject.toString().contains(FIELDS.PAYMENT_TOKEN)) {
							JSONObject params = new JSONObject();
							Map<String, String> cardObject = new HashMap<String, String>();

							params.put(FIELDS.MERCHANT_REF_NUMBER, "selfie-" + System.currentTimeMillis());
							params.put(FIELDS.AMOUNT, amount);
							// params.put(FIELDS.SETTLE_WITH_AUTH, true);
							cardObject.put(FIELDS.PAYMENT_TOKEN, jsonObject.getString(FIELDS.PAYMENT_TOKEN));
							params.put(FIELDS.CARD, new JSONObject(cardObject));

							AppSession.getInstance().DEBUG(mContext, "Params = " + params.toString());

							JsonObjectRequest transferRequest = new JsonObjectRequest(Request.Method.POST,
									CARD_PAYMENTS_URL + ACC_NUMBER + CARD_PAYMENTS_URL_SUFFIX, params,
									responseListener, apiErrorListener) {
								@Override
								public Map<String, String> getHeaders() throws AuthFailureError {
									Map<String, String> headers = new HashMap<String, String>();

									String uCredentials = String.format("%s:%s", credentials.getUser(),
											credentials.getPwd() + "-" + credentials.getAuthKey());
									String auth = "Basic "
											+ Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
									headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
									// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
									// ApiManager.HEADERS.CONTENT_TYPE_JSON);
									AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

									return headers;
								}
							};
							AppSession.getInstance().addToRequestQueue(mContext, transferRequest, REQUEST.TAG_CARD);
						} else {
							AppSession.getInstance().DisplayToast(mContext,
									jsonObject.getJSONObject("error").get("message").toString());
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_CARD);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void transferToCard(final UserProfile profile, final int amount,
			final Response.Listener<JSONObject> responseListener) {
		try {
			final Credentials credentials = new Credentials();
			UserOpInfo opInfo = new Gson().fromJson(mSettingsEditor.getStringValue(UserOpInfo.OP_INFO_JSON),
					UserOpInfo.class);

			String cardId = "";

			for (int i = 0; i < opInfo.getCards().size(); i++) {
				if (opInfo.getCards().get(i).isDefault()) {
					cardId = opInfo.getCards().get(i).getCardId();
					AppSession.getInstance().DEBUG(mContext,
							"Get payment token for" + opInfo.getCards().get(i).getLastDigits());
					break;
				}
			}

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(PROFILE_URL + "/" + opInfo.getProfileID()
					+ CARDS_URL_SUFFIX + "/" + cardId, null, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject jsonObject) {
					try {
						AppSession.getInstance().DEBUG(mContext, "Look up card info = " + jsonObject.toString());
						if (jsonObject.toString().contains(FIELDS.ACTIVE)
								&& jsonObject.toString().contains(FIELDS.PAYMENT_TOKEN)) {
							JSONObject params = new JSONObject();
							Map<String, String> cardObject = new HashMap<String, String>();

							params.put(FIELDS.MERCHANT_REF_NUMBER, "selfie-" + System.currentTimeMillis());
							params.put(FIELDS.AMOUNT, amount);
							// params.put(FIELDS.SETTLE_WITH_AUTH, true);
							cardObject.put(FIELDS.PAYMENT_TOKEN, jsonObject.getString(FIELDS.PAYMENT_TOKEN));
							params.put(FIELDS.CARD, new JSONObject(cardObject));

							AppSession.getInstance().DEBUG(mContext, "Params = " + params.toString());

							JsonObjectRequest transferRequest = new JsonObjectRequest(Request.Method.POST,
									CARD_PAYMENTS_URL + ACC_NUMBER + CARD_PAYMENTS_URL_SUFFIX, params,
									responseListener, apiErrorListener) {
								@Override
								public Map<String, String> getHeaders() throws AuthFailureError {
									Map<String, String> headers = new HashMap<String, String>();

									String uCredentials = String.format("%s:%s", credentials.getUser(),
											credentials.getPwd() + "-" + credentials.getAuthKey());
									String auth = "Basic "
											+ Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
									headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
									// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
									// ApiManager.HEADERS.CONTENT_TYPE_JSON);
									AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

									return headers;
								}
							};
							AppSession.getInstance().addToRequestQueue(mContext, transferRequest, REQUEST.TAG_CARD);
						} else {
							AppSession.getInstance().DisplayToast(mContext,
									jsonObject.getJSONObject("error").get("message").toString());
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, apiErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();

					String uCredentials = String.format("%s:%s", credentials.getUser(), credentials.getPwd() + "-"
							+ credentials.getAuthKey());
					String auth = "Basic " + Base64.encodeToString(uCredentials.getBytes(), Base64.NO_WRAP);
					headers.put(ApiManager.HEADERS.AUTHORIZATION, auth);
					// headers.put(ApiManager.HEADERS.CONTENT_TYPE,
					// ApiManager.HEADERS.CONTENT_TYPE_JSON);
					AppSession.getInstance().DEBUG(mContext, "Authenticate " + headers.toString());

					return headers;
				}
			};

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest, REQUEST.TAG_CARD);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Response.ErrorListener apiErrorListener = new Response.ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError error) {
			try {
				NetworkResponse response = error.networkResponse;
				if (response != null && response.data != null) {
					mTimeOutListener.onTimeout();
					AppSession.getInstance().ERROR(mContext, error.toString());
					AppSession.getInstance().DisplayToast(mContext, error.getMessage().toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public void debitCreditAmountToAccount(boolean isCharge, BigDecimal amount, String accountType, String bankName,
			String checkNumber, String bankAccountNumber, String routingNumber, String checkPayMethod,
			String firstName, String lastName, String street, String city, String country, String zipcode,
			String phone, String email, final Response.Listener<JSONObject> responseListener) {

		try {

			// Need it to be blocked so based on response we continue or
			// discontinue process

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

			StrictMode.setThreadPolicy(policy);

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(DIRECT_DEBIT_URL);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			if (isCharge)
				nameValuePairs.add(new BasicNameValuePair("txnMode", "charge"));
			else
				nameValuePairs.add(new BasicNameValuePair("txnMode", "credit"));

			String requestString = getCreditToBankRequest(String.valueOf("selfie-" + System.currentTimeMillis()),
					amount, accountType, bankName, checkNumber, bankAccountNumber, routingNumber, checkPayMethod,
					firstName, lastName, street, city, country, zipcode, phone, email);

			AppSession.getInstance().DEBUG(mContext, "**** REQUEST STRING " + requestString);

			nameValuePairs.add(new BasicNameValuePair("txnRequest", requestString));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			BufferedReader in = null;
			try {
				AppSession.getInstance().DEBUG(mContext, response.getStatusLine().toString());

				in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
				}
				in.close();
				AppSession.getInstance().DEBUG(mContext, "**** Direct debit response = ******" + sb.toString());

				if (sb.toString().contains("ACCEPTED") && sb.toString().contains("Approved")) {
					responseListener.onResponse(new JSONObject("{status:APPROVED}"));
				} else {
					responseListener.onResponse(new JSONObject("{status:REJECTED}"));
				}

			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getCreditToBankRequest(String merchantRefNumber, BigDecimal amount, String accountType,
			String bankName, String checkNumber, String bankAccountNumber, String routingNumber, String checkPayMethod,
			String firstName, String lastName, String street, String city, String country, String zipcode,
			String phone, String email) {
		StringBuffer buf = new StringBuffer();

		buf.append("<ddCheckRequestV1 xmlns=\"http://www.optimalpayments.com/directdebit/xmlschema/v1\">");
		buf.append("<merchantAccount>");
		buf.append("<accountNum>" + MERCHANT_ACCOUNT_NUMBER + "</accountNum>");
		buf.append("<storeID>" + STORE_ID + "</storeID>");
		buf.append("<storePwd>" + STORE_PWD + "</storePwd>");
		buf.append("</merchantAccount>");
		buf.append("<merchantRefNum>" + merchantRefNumber + "</merchantRefNum>");
		buf.append("<amount>" + amount + "</amount>");
		buf.append("<check>");
		buf.append("<accountType>" + accountType + "</accountType>");
		buf.append("<bankName>" + bankName + "</bankName>");
		buf.append("<checkNum>" + checkNumber + "</checkNum>");
		buf.append("<accountNum>" + bankAccountNumber + "</accountNum>");
		buf.append("<routingNum>" + routingNumber + "</routingNum>");
		buf.append("</check>");
		buf.append("<billingDetails>");
		buf.append("<checkPayMethod>" + checkPayMethod + "</checkPayMethod>");
		buf.append("<firstName>" + firstName + "</firstName>");
		buf.append("<lastName>" + lastName + "</lastName>");
		buf.append("<street>" + street + "</street>");
		buf.append("<city>" + city + "</city>");
		buf.append("<country>" + country + "</country>");
		buf.append("<zip>" + zipcode + "</zip>");
		buf.append("<phone>" + phone + "</phone>");
		buf.append("<email>" + email + "</email>");
		buf.append("</billingDetails>");
		buf.append("</ddCheckRequestV1>");
		return buf.toString();
	}

}
