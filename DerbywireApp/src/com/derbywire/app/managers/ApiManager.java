package com.derbywire.app.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.common.JsonArrayRequest;
import com.derbywire.app.common.JsonObjectRequest;
import com.derbywire.app.common.RestAPIConstants;
import com.derbywire.app.common.SettingsEditor;
import com.derbywire.app.models.UserOpInfo;
import com.derbywire.app.models.UserProfile;
import com.derbywire.app.profiles.Utils;
import com.derbywire.app.signin.SignInActivity;
import com.derbywire.app.signin.UserInfo;
import com.google.gson.Gson;

/**
 * Created by cogniflex on 16/4/15.
 */
public class ApiManager {

	public interface TimeOutListener {
		void onTimeout();
	}

	private Context mContext;
	private String mApiKey = "";

	public class HEADERS {
		public static final String AUTHORIZATION = "Authorization";
		public static final String AUTHORIZATION_COLON = "Authorization:";
		public static final String BEARER = "Bearer ";
		public static final String BASIC = "Basic ";
		public static final String CONTENT_TYPE = "Content-Type";
		public static final String CONTENT_TYPE_JSON = "application/json";
	}

	public class REQUEST {
		public static final String TAG_SIGN_UP = "TAG_SIGN_UP";
		public static final String TAG_SIGN_IN = "TAG_SIGN_IN";
		public static final String TAG_USERS = "TAG_USERS";
		public static final String TAG_PRODUCTS = "TAG_PRODUCTS";
		public static final String TAG_OP_INFO = "TAG_OP_INFO";
		public static final String TAG_ORDER = "TAG_ORDER";
		public static final String TAG_ACTIVITIES = "TAG_ACTIVITIES";
	}

	public class SELFIEPAY_API_URL {

		private static final String BASE_URL = "http://selfiepay.co/api/";
		private static final String VERSION = "v1";

		public static final String SIGN_UP = "http://50.56.188.53/api/services/Signup/format/json";
		public static final String SIGN_IN = "http://50.56.188.53/api/services/Login/format/json";
		public static final String USERS = BASE_URL + VERSION + "/users";
		public static final String PRODUCTS = BASE_URL + VERSION + "/products";
		public static final String OP_INFO = BASE_URL + VERSION
				+ "/optimalpaymentsinfo";
		public static final String UPDATE_OP_INFO = BASE_URL + VERSION
				+ "/updateoptimalpaymentsinfo";
		public static final String ORDERS = BASE_URL + VERSION + "/orders";
		public static final String ACTIVITIES = BASE_URL + VERSION
				+ "/activities";
		public static final String USER_INFO = BASE_URL + VERSION + "/user";
		public static final String PRODUCTS_FORACCOUNT = BASE_URL + VERSION
				+ "/getAllProductsForAccount";
	}

	public class FIELDS {
		public static final String FIRST_NAME = "first_name";
		public static final String LAST_NAME = "last_name";
		public static final String EMAIL = "email";
		public static final String PASSWORD = "password";
		public static final String IMAGE = "image";
		public static final String DATA = "data";
		public static final String IMEI = "imei";
		public static final String REQUESTFROM = "requestfrom";
		public static final String GENDER = "gender";
		public static final String DAY = "date";
		public static final String MONTH = "month";
		public static final String CODEWIRE = "codewire";
		public static final String USERTYPE = "usertype";
		public static final String YEAR = "year";
		public static final String CONFIRMPASSWORD = "confirmpassword";
	}

	public class RESPONSE {
		public static final String ERROR = "error";
		public static final String MESSAGE = "message";
		public static final String STATUS = "status";

	}

	TimeOutListener mTimeOutListener;

	public ApiManager(Context context, final TimeOutListener timeOutListener) {
		this.mContext = context;
		SettingsEditor settingsEditor = new SettingsEditor(context);
		this.mApiKey = settingsEditor.getStringValue(UserProfile.API_KEY);
		this.mTimeOutListener = timeOutListener;
		/*
		 * (try { new CountDownTimer(60000, 1000) {
		 * 
		 * public void onTick(long millisUntilFinished) {
		 * AppSession.getInstance().DEBUG(mContext, "seconds remaining: " +
		 * millisUntilFinished / 1000); }
		 * 
		 * public void onFinish() { AppSession.getInstance().DEBUG(mContext,
		 * "Network timeout!"); mTimeOutListener.onTimeout(); } }.start(); }
		 * catch (Exception e) { e.printStackTrace(); }
		 */
	}

	public final void registerAccount(UserInfo mSignUpData,
			Response.Listener<JSONObject> responseListener) {
		try {

			String email = mSignUpData.getEmail().toString();
			String pass = mSignUpData.getPassword().toString();
			Integer day = mSignUpData.getDate();
			Integer month = mSignUpData.getMonth();
			Integer year = mSignUpData.getYear();

			JSONObject jsonObjMetaData = new JSONObject();

			jsonObjMetaData.put(FIELDS.EMAIL, email);
			jsonObjMetaData.put(FIELDS.PASSWORD, pass);
			jsonObjMetaData.put(FIELDS.CONFIRMPASSWORD, pass);
			jsonObjMetaData.put(FIELDS.MONTH, String.valueOf(month));
			jsonObjMetaData.put(FIELDS.DAY, String.valueOf(day));
			jsonObjMetaData.put(FIELDS.YEAR, String.valueOf(year));
			jsonObjMetaData.put(FIELDS.GENDER, "Male");
			jsonObjMetaData.put(FIELDS.USERTYPE, "Fan");
			jsonObjMetaData.put(FIELDS.CODEWIRE, "codewire");
			jsonObjMetaData.put(FIELDS.REQUESTFROM, "mobile");
			String requestBody = jsonObjMetaData.toString();

			AppSession.getInstance().DisplayToast(mContext, requestBody);

			Map<String, String> params = new HashMap<String, String>();
			params.put(FIELDS.DATA, requestBody);

			AppSession.getInstance().DEBUG(mContext,
					"signup user " + requestBody);
			JsonObjectRequest jsObjRequest = new JsonObjectRequest(
					Request.Method.POST, SELFIEPAY_API_URL.SIGN_UP, params,
					responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
					REQUEST.TAG_SIGN_UP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void opInfo(final String accountID,
			final String merchantCustomerId, final List<String> cards,
			Response.Listener<JSONObject> responseListener) {
		try {
			/*
			 * Map<String, String> params = new HashMap<String, String>();
			 * params.put(FIELDS.FIRST_NAME, firstName);
			 * params.put(FIELDS.LAST_NAME, lastName); params.put(FIELDS.EMAIL,
			 * email); params.put(FIELDS.PASSWORD, password);
			 * params.put(FIELDS.IMAGE, SelfieUtil.getEncodedImage(image));
			 * AppSession.getInstance().DEBUG(mContext, "Register user " +
			 * email); JsonObjectRequest jsObjRequest = new
			 * JsonObjectRequest(Request.Method.POST, SELFIEPAY_API_URL.SIGN_UP,
			 * params, responseListener, this.apiErrorListener);
			 * 
			 * AppSession.getInstance().addToRequestQueue(mContext,
			 * jsObjRequest, REQUEST.TAG_SIGN_UP);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void doLogin(final String username, final String password,
			Response.Listener<JSONObject> responseListener) {
		try {

			String email = username;
			String pass = password;

			JSONObject jsonObjMetaData = new JSONObject();

			jsonObjMetaData.put(FIELDS.EMAIL, email);
			jsonObjMetaData.put(FIELDS.PASSWORD, pass);
			jsonObjMetaData.put(FIELDS.REQUESTFROM, "mobile");
			String requestBody = jsonObjMetaData.toString();

			AppSession.getInstance().DisplayToast(mContext, requestBody);

			Map<String, String> params = new HashMap<String, String>();
			params.put(FIELDS.DATA, requestBody);

			AppSession.getInstance().DEBUG(mContext,
					"login user " + requestBody);
			JsonObjectRequest jsObjRequest = new JsonObjectRequest(
					Request.Method.POST, SELFIEPAY_API_URL.SIGN_IN, params,
					responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
					REQUEST.TAG_SIGN_IN);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void addMerchantInfo(UserProfile.MerchantInfo merchantInfo,
			Response.Listener<JSONObject> responseListener) {
		try {
			String merchantJson = new Gson().toJson(merchantInfo);

			Map<String, String> params = new HashMap<String, String>();
			params.put(UserProfile.MERCHANT_JSON, merchantJson);

			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			AppSession.getInstance().DEBUG(
					mContext,
					"updating merchant info " + merchantJson + " for apikey "
							+ mApiKey);

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(
					Request.Method.PUT, SELFIEPAY_API_URL.USER_INFO, params,
					headers, responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
					REQUEST.TAG_USERS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void getUsers(Response.Listener<JSONArray> responseListener) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			JsonArrayRequest jsArrayRequest = new JsonArrayRequest(
					Request.Method.GET, SELFIEPAY_API_URL.USERS, null, headers,
					responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext,
					jsArrayRequest, REQUEST.TAG_USERS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void getProducts(Response.Listener<JSONArray> responseListener) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			JsonArrayRequest jsArrayRequest = new JsonArrayRequest(
					Request.Method.GET, SELFIEPAY_API_URL.PRODUCTS, null,
					headers, responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext,
					jsArrayRequest, REQUEST.TAG_PRODUCTS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void insertOpInfo(boolean isUpdate, UserOpInfo opInfo,
			Response.Listener<JSONObject> responseListener) {
		try {
			String opInfoJson = new Gson().toJson(opInfo);

			Map<String, String> params = new HashMap<String, String>();
			params.put(UserOpInfo.OP_INFO_JSON, opInfoJson);

			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			AppSession.getInstance().DEBUG(
					mContext,
					"inserting op info " + opInfoJson + " for apikey "
							+ mApiKey);

			String url = RestAPIConstants.getInstance().CARDINFO_SAVE;

			// if (isUpdate)
			// url = SELFIEPAY_API_URL.UPDATE_OP_INFO;

			JsonObjectRequest jsObjRequest = new JsonObjectRequest(
					Request.Method.GET, url, params, headers, responseListener,
					this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
					REQUEST.TAG_OP_INFO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final void getOpInfo(Response.Listener<JSONArray> responseListener) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			JsonArrayRequest jsArrayRequest = new JsonArrayRequest(
					Request.Method.GET, SELFIEPAY_API_URL.OP_INFO, null,
					headers, responseListener, this.apiErrorListener);

			AppSession.getInstance().addToRequestQueue(mContext,
					jsArrayRequest, REQUEST.TAG_OP_INFO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public final void getUserforAccountId(int accountId,
	 * Response.Listener<JSONObject> responseListener) { try {
	 * 
	 * Map<String, String> params = new HashMap<String, String>();
	 * params.put(UserActivity.ACCOUNT_ID, String.valueOf(accountId));
	 * 
	 * Map<String, String> headers = new HashMap<String, String>();
	 * headers.put(HEADERS.AUTHORIZATION, mApiKey);
	 * 
	 * AppSession.getInstance().DEBUG( mContext, "get user info for account " +
	 * accountId + " for apikey " + mApiKey);
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.POST, SELFIEPAY_API_URL.USER_INFO, params, headers,
	 * responseListener, this.apiErrorListener);
	 * 
	 * AppSession.getInstance().addToRequestQueue(mContext, jsObjRequest,
	 * REQUEST.TAG_USERS); } catch (Exception e) { e.printStackTrace(); } }
	 */

	public final void getUserActivities(
			Response.Listener<JSONArray> responseListener) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put(HEADERS.AUTHORIZATION, mApiKey);

			JsonArrayRequest jsArrayRequest = new JsonArrayRequest(
					Request.Method.GET, SELFIEPAY_API_URL.ACTIVITIES, null,
					headers, responseListener, this.apiErrorListener);
			jsArrayRequest.setShouldCache(false);
			AppSession.getInstance().addToRequestQueue(mContext,
					jsArrayRequest, REQUEST.TAG_ACTIVITIES);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Response.ErrorListener apiErrorListener = new Response.ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError error) {
			try {
				NetworkResponse response = error.networkResponse;
				if (response != null && response.data != null) {
					mTimeOutListener.onTimeout();
					AppSession.getInstance().ERROR(mContext,
							error.toString());
					AppSession.getInstance().DisplayToast(mContext,
							error.getMessage().toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
}
