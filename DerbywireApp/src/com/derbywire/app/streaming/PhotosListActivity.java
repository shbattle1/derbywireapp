package com.derbywire.app.streaming;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.common.LogFile;
import com.derbywire.app.streaming.IPlayListListener.IAudioDetailsListener;
import com.derbywire.app.streaming.IPlayListListener.IAudioPlayListListener;
import com.derbywire.app.streaming.IPlayListListener.IPhotoDetailsListener;
import com.derbywire.app.streaming.IPlayListListener.IPhotoPlayListListener;

public class PhotosListActivity extends Activity {

	Context mContext = null;
	PlayListHandler playListHandler = null;
	TextView logView = null;
	Button btn1 = null;
	Button btn2 = null;
	Button btn3 = null;
	Button btn4 = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photo_layout);
		mContext = this;

		intResources();
		setClickListerners();
		playListHandler = new PlayListHandler();

	}

	private void getPhotoPlayList(int userId) {
		if (playListHandler != null) {
			playListHandler.getPhotoPlayList(userId,
					new IPhotoPlayListListener() {

						@Override
						public void onPhotoPlayListSuccess(
								PhotoPlayListResponse photoPlayListResponse) {
							if (photoPlayListResponse != null
									&& photoPlayListResponse.getPhotoPlayList() != null) {
								StringBuilder log = photoPlayListResponse
										.printPhotoList();
								updateResponse(log);
							}
						}

						@Override
						public void onPhotoPlayListFail(
								PhotoPlayListResponse photoPlayListResponse) {
							if (photoPlayListResponse != null) {
								LogFile.ShowLog(
										"getPhotoPlayList",
										"error message: "
												+ photoPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getPhotoPlayList",
										"error status: "
												+ photoPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}

	private void updateResponse(final StringBuilder log) {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				logView.setText(log.toString());
			}
		});
	}

	private void getPhotoDetails(int photoId) {
		if (playListHandler != null) {
			playListHandler.getPhotoDetails(photoId,
					new IPhotoDetailsListener() {

						@Override
						public void onPhotoDetailsSuccess(
								PhotoPlayListResponse photoPlayListResponse) {
							if (photoPlayListResponse != null
									&& photoPlayListResponse.getPhotoDetails() != null) {
								PhotoDetails photoDetails = photoPlayListResponse
										.getPhotoDetails();
								if (photoDetails != null) {
									StringBuilder log1 = photoDetails
											.printPhotoDetails();
									updateResponse(log1);
								}
							}
						}

						@Override
						public void onPhotoDetailsFail(
								PhotoPlayListResponse photoPlayListResponse) {
							if (photoPlayListResponse != null) {

								LogFile.ShowLog(
										"getPhotoDetails",
										"error message: "
												+ photoPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getPhotoDetails",
										"error status: "
												+ photoPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}

	private void intResources() {
		logView = (TextView) findViewById(R.id.log);
		btn1 = (Button) findViewById(R.id.btn1);
		btn2 = (Button) findViewById(R.id.btn2);
		btn3 = (Button) findViewById(R.id.btn3);
		btn4 = (Button) findViewById(R.id.btn4);
	}

	private void setClickListerners() {
		btn1.setOnClickListener(BtnListener);
		btn2.setOnClickListener(BtnListener);
		btn3.setOnClickListener(BtnListener);
		btn4.setOnClickListener(BtnListener);
	}

	OnClickListener BtnListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.btn1:
				logView.setText("Loading.....");
				getPhotoPlayList(28);
				break;

			case R.id.btn2:
				logView.setText("Loading.....");
				getPhotoDetails(5);
				break;
			case R.id.btn3:
				logView.setText("Loading.....");
				getAudioPlayList(28);
				break;
			case R.id.btn4:
				logView.setText("Loading.....");

				getAudioDetails(5);
				break;
			}
		}
	};

	private void getAudioPlayList(int userId) {
		if (playListHandler != null) {
			playListHandler.getAudioPlayList(userId,
					new IAudioPlayListListener() {

						@Override
						public void onAudioPlayListSuccess(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null
									&& audioPlayListResponse.getAudioPlayList() != null) {
								StringBuilder log2 = audioPlayListResponse
										.printAudioList();
								updateResponse(log2);
							}
						}

						@Override
						public void onAudioPlayListFail(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null) {
								LogFile.ShowLog(
										"getAudioPlayList",
										"error message: "
												+ audioPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getAudioPlayList",
										"error status: "
												+ audioPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}

	private void getAudioDetails(int audioId) {
		if (playListHandler != null) {
			playListHandler.getAudioDetails(audioId,
					new IAudioDetailsListener() {

						@Override
						public void onAudioDetailsSuccess(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null
									&& audioPlayListResponse.getAudioDetails() != null) {
								AudioDetails audioDetails = audioPlayListResponse
										.getAudioDetails();
								if (audioDetails != null) {
									StringBuilder log3 = audioDetails
											.printAudioDetails();
									updateResponse(log3);
								}
							}
						}

						@Override
						public void onAudioDetailsFail(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null) {
								LogFile.ShowLog(
										"getAudioDetails",
										"error message: "
												+ audioPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getAudioDetails",
										"error status: "
												+ audioPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}
}
