package com.derbywire.app.streaming;

import java.util.List;

import android.util.Log;

import com.derbywire.app.core.network.NetworkErrorResponse;

public class AudioPlayListResponse extends NetworkErrorResponse{
	private List<AudioDetails> audioPlayList = null;
	private AudioDetails audioDetails = null;
	
	public AudioDetails getAudioDetails() {
		return audioDetails;
	}

	public void setAudioDetails(AudioDetails audioDetails) {
		this.audioDetails = audioDetails;
	}

	public List<AudioDetails> getAudioPlayList() {
		return audioPlayList;
	}

	public void setAudioPlayList(List<AudioDetails> audioPlayList) {
		this.audioPlayList = audioPlayList;
	}
	
	public StringBuilder printAudioList(){
		StringBuilder retString = new StringBuilder();

		if( audioPlayList != null){
			for( int i =0; i<audioPlayList.size(); i++){
				retString.append("\n Audio..........................Index: "+i+"\n");
				Log.i("printPhotoList", "Audio..........................Index: "+i);
				retString.append(audioPlayList.get(i).printAudioDetails());
			}
		}
		return retString;
	}
}
