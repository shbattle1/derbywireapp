package com.derbywire.app.streaming;

import java.util.ArrayList;

import android.util.Log;

public class ButtonDetails {

	private final String TAG = "ButtonDetails";
	
	ArrayList<Integer> buttonIds = null;
	
	public ArrayList<Integer> getButtonIds() {
		return buttonIds;
	}

	public void setButtonIds(ArrayList<Integer> buttonIds) {
		this.buttonIds = buttonIds;
	}

	public StringBuilder printPhotoDetails(){
		StringBuilder logString = new StringBuilder();
		
		for(int i=0; i <buttonIds.size(); i++){
			logString.append("buttonIds:["+i+"] "+buttonIds.get(i) ).append("\n");
			Log.i(TAG,"buttonIds:["+i+"] "+buttonIds.get(i) );
		}		return logString;
	}
}
