package com.derbywire.app.streaming;

public class AudioDetails {
	
	int audio_track_id = 0;
	String audio_file_name = null;
	String audio_content_type = null;
	long audio_file_size = 0;
	String audio_orginal_name = null;
	String audio_updated_at = null;
	String title = null;
	
	public int getAudio_track_id() {
		return audio_track_id;
	}

	public void setAudio_track_id(int audio_track_id) {
		this.audio_track_id = audio_track_id;
	}

	public String getAudio_file_name() {
		return audio_file_name;
	}

	public void setAudio_file_name(String audio_file_name) {
		this.audio_file_name = audio_file_name;
	}

	public String getAudio_content_type() {
		return audio_content_type;
	}

	public void setAudio_content_type(String audio_content_type) {
		this.audio_content_type = audio_content_type;
	}

	public long getAudio_file_size() {
		return audio_file_size;
	}

	public void setAudio_file_size(long audio_file_size) {
		this.audio_file_size = audio_file_size;
	}

	public String getAudio_orginal_name() {
		return audio_orginal_name;
	}

	public void setAudio_orginal_name(String audio_orginal_name) {
		this.audio_orginal_name = audio_orginal_name;
	}

	public String getAudio_updated_at() {
		return audio_updated_at;
	}

	public void setAudio_updated_at(String audio_updated_at) {
		this.audio_updated_at = audio_updated_at;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getAlbum_image_id() {
		return album_image_id;
	}

	public void setAlbum_image_id(int album_image_id) {
		this.album_image_id = album_image_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	int user_id = 0;
	int album_image_id = 0;
	String description = null;
	
	public StringBuilder printAudioDetails(){
		StringBuilder logString = new StringBuilder();
		
		logString.append("audio_track_id:" + getAudio_track_id()).append("\n")
		.append("audio_file_name:" + getAudio_file_name()).append("\n")
		.append("audio_content_type:" + getAudio_content_type()).append("\n")
		.append("audio_file_size:" + getAudio_file_size()).append("\n")
		.append("audio_orginal_name:" + getAudio_orginal_name()).append("\n")
		.append("audio_updated_at:" + getAudio_updated_at()).append("\n")
		.append("title:" + getTitle()).append("\n");

		return logString;
	}
}
