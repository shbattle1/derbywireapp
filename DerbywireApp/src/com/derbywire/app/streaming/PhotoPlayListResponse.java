package com.derbywire.app.streaming;

import java.util.List;

import android.util.Log;

import com.derbywire.app.core.network.NetworkErrorResponse;

public class PhotoPlayListResponse extends NetworkErrorResponse{
	private List<PhotoDetails> photoPlayList = null;
	private PhotoDetails photoDetails = null;
	
	public PhotoDetails getPhotoDetails() {
		return photoDetails;
	}

	public void setPhotoDetails(PhotoDetails photoDetails) {
		this.photoDetails = photoDetails;
	}

	public List<PhotoDetails> getPhotoPlayList() {
		return photoPlayList;
	}

	public void setPhotoPlayList(List<PhotoDetails> photoPlayList) {
		this.photoPlayList = photoPlayList;
	}
	
	public StringBuilder printPhotoList(){
		StringBuilder retString = new StringBuilder();
		if( photoPlayList != null){
			for( int i =0; i<photoPlayList.size(); i++){
				retString.append("\n Photo..........................Index: "+i+"\n");
				Log.i("printPhotoList", "Photo..........................Index: "+i);
				retString.append(photoPlayList.get(i).printPhotoDetails());
			}
		}
		return retString;
	}
}
