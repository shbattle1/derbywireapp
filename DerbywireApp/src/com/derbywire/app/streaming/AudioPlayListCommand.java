package com.derbywire.app.streaming;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.derbywire.app.common.AppConstants;
import com.derbywire.app.common.RestAPIConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.ICommandListener;
import com.derbywire.app.core.IDependencyTaskListener;
import com.derbywire.app.core.network.ConnectionManager;
import com.derbywire.app.core.network.NetworkResponse;

public class AudioPlayListCommand extends Command {

	private AudioPlayListResponse audioPlayListResponse = null;
	private int userId = 0;

	public AudioPlayListResponse getAudioPlayListResponse() {
		return audioPlayListResponse;
	}

	public void setAudioPlayListResponse(
			AudioPlayListResponse audioPlayListResponse) {
		this.audioPlayListResponse = audioPlayListResponse;
	}

	public AudioPlayListCommand(Command_Id commandId, String commandName,
			ICommandListener listener,
			IDependencyTaskListener dependecyTaskListener, int priority,
			Bundle extraParams) {
		super(commandId, commandName, listener, dependecyTaskListener,
				priority, extraParams);
		if (extraParams != null) {
			userId = extraParams.getInt(AppConstants.IdPref);
		}
	}

	private void doNetworkOpration() {
		String networkUrl = String.format(
				RestAPIConstants.getInstance().AUDIO_LIST_URL, userId);
		byte[] buffer = null;
		NetworkResponse response;
		String content = "";

		try {
			response = ConnectionManager.connect(networkUrl,
					HttpGet.METHOD_NAME, createHeaders(""), buffer);
			content = response.getContent();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!TextUtils.isEmpty(content)) {
			parseResponse(content);
		}
	}

	private void parseResponse(String content) {
		audioPlayListResponse = new AudioPlayListResponse();
		List<AudioDetails> audioPlayList = new ArrayList<AudioDetails>();

		if (content != null && audioPlayList != null) {
			JSONObject root;
			try {
				root = new JSONObject(content);
				JSONArray entries = root.optJSONArray("play_list");

				for (int index = 0; index < entries.length(); index++) {
					AudioDetails audioDetails = new AudioDetails();
					JSONArray subentries = entries.optJSONArray(index);

					JSONObject jsonObj = subentries.optJSONObject(0);
					audioDetails.setAudio_track_id(jsonObj
							.optInt("audio_track_id"));
					audioDetails.setAudio_file_name(jsonObj
							.optString("audio_file_name"));
					audioDetails.setAudio_content_type(jsonObj
							.optString("audio_content_type"));
					audioDetails.setAudio_file_size(jsonObj
							.optLong("audio_file_size"));
					audioDetails.setAudio_orginal_name(jsonObj
							.optString("audio_orginal_name"));
					audioDetails.setAudio_updated_at(jsonObj
							.optString("audio_updated_at"));

					audioPlayList.add(audioDetails);
				}
				audioPlayListResponse.setAudioPlayList(audioPlayList);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void clean() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute() throws Exception {
		doNetworkOpration();
	}

	@Override
	public void init() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void errorOcurredWhileExecutingCommand() {
		// TODO Auto-generated method stub

	}
}
