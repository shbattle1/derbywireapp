package com.derbywire.app.streaming;


public interface IPlayListListener {
	public interface IPhotoPlayListListener {
		public void onPhotoPlayListSuccess(PhotoPlayListResponse photoPlayListResponse);
		public void onPhotoPlayListFail(PhotoPlayListResponse photoPlayListResponse);
	}
	
	public interface IPhotoDetailsListener {
		public void onPhotoDetailsSuccess(PhotoPlayListResponse photoPlayListResponse);
		public void onPhotoDetailsFail(PhotoPlayListResponse photoPlayListResponse);
	}
	
	public interface IAudioPlayListListener {
		public void onAudioPlayListSuccess(AudioPlayListResponse audioPlayListResponse);
		public void onAudioPlayListFail(AudioPlayListResponse audioPlayListResponse);
	}
	
	public interface IAudioDetailsListener {
		public void onAudioDetailsSuccess(AudioPlayListResponse audioPlayListResponse);
		public void onAudioDetailsFail(AudioPlayListResponse audioPlayListResponse);
	}
}
