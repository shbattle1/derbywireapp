package com.derbywire.app.streaming;

import android.os.Bundle;

import com.derbywire.app.common.AppConstants;
import com.derbywire.app.common.CommandConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.CommandController;
import com.derbywire.app.core.ICommandListener;
import com.derbywire.app.streaming.IPlayListListener.IAudioDetailsListener;
import com.derbywire.app.streaming.IPlayListListener.IAudioPlayListListener;
import com.derbywire.app.streaming.IPlayListListener.IPhotoDetailsListener;
import com.derbywire.app.streaming.IPlayListListener.IPhotoPlayListListener;

public class PlayListHandler {
	private IPhotoPlayListListener photoPlayListListener = null;
	private IPhotoDetailsListener photoDetailsListener = null;
	private IAudioPlayListListener audioPlayListListener = null;
	private IAudioDetailsListener audioDetailsListener = null;
	
	public void getAudioPlayList(int userId, IAudioPlayListListener audioPlayListListener) {
		this.audioPlayListListener = audioPlayListListener;
		Bundle bundle = new Bundle();
		bundle.putInt(AppConstants.IdPref, userId);
		
		AudioPlayListCommand command = new AudioPlayListCommand(Command_Id.COMMAND_ID_AUDIO_LIST, 
				CommandConstants.COMMAND_TAG_NULL, commandListener, null, Command.PRIORITY_SEQUENTIAL, 
				bundle);		
		try {
			CommandController.getInstance().queueCommand(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getAudioDetails(int audioId, IAudioDetailsListener audioDetailsListener) {
		this.audioDetailsListener = audioDetailsListener;
		Bundle bundle = new Bundle();
		bundle.putInt(AppConstants.IdPref, audioId);
		
		AudioDetailsCommand command = new AudioDetailsCommand(Command_Id.COMMAND_ID_AUDIO_DETAILS, 
				CommandConstants.COMMAND_TAG_NULL, commandListener, null, Command.PRIORITY_SEQUENTIAL, 
				bundle);		
		try {
			CommandController.getInstance().queueCommand(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getPhotoPlayList(int userId, IPhotoPlayListListener photoPlayListListener) {
		this.photoPlayListListener = photoPlayListListener;
		Bundle bundle = new Bundle();
		bundle.putInt(AppConstants.IdPref, userId);
		
		PhotoPlayListCommand command = new PhotoPlayListCommand(Command_Id. COMMAND_ID_PHOTO_LIST, 
				CommandConstants.COMMAND_TAG_NULL, commandListener, null, Command.PRIORITY_SEQUENTIAL, 
				bundle);		
		try {
			CommandController.getInstance().queueCommand(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getPhotoDetails(int photoId, IPhotoDetailsListener photoDetailsListener) {
		this.photoDetailsListener = photoDetailsListener;
		Bundle bundle = new Bundle();
		bundle.putInt(AppConstants.IdPref, photoId);
		
		PhotoDetailsCommand command = new PhotoDetailsCommand(Command_Id.COMMAND_ID_PHOTO_DETAILS, 
				CommandConstants.COMMAND_TAG_NULL, commandListener, null, Command.PRIORITY_SEQUENTIAL, 
				bundle);		
		try {
			CommandController.getInstance().queueCommand(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private ICommandListener commandListener = new ICommandListener() {
		
		@Override
		public void commandStarted(Command command) {
		}
		
		@Override
		public void commandQueued(Command command) {
		}
		
		@Override
		public void commandInProgress(Command command) {
			
		}
		
		@Override
		public void commandFailed(Command command) {
			switch(command.getCommandId()){
			case COMMAND_ID_PHOTO_LIST:
				photoPlayListListener.onPhotoPlayListFail(
						((PhotoPlayListCommand)command).getPhotoPlayListResponse());
				break;
			case COMMAND_ID_PHOTO_DETAILS:
				photoDetailsListener.onPhotoDetailsFail(
						((PhotoDetailsCommand)command).getPhotoPlayListResponse());
				break;
			case COMMAND_ID_AUDIO_LIST:
				audioPlayListListener.onAudioPlayListFail(
						((AudioPlayListCommand)command).getAudioPlayListResponse());
				break;
			case COMMAND_ID_AUDIO_DETAILS:
				audioDetailsListener.onAudioDetailsFail(
						((AudioDetailsCommand)command).getAudioPlayListResponse());
				break;
			default:
				break;
			}			
		}
		
		@Override
		public void commandCompleted(Command command) {
			switch(command.getCommandId()){
				case COMMAND_ID_PHOTO_LIST:
					photoPlayListListener.onPhotoPlayListSuccess(
							((PhotoPlayListCommand)command).getPhotoPlayListResponse());
					break;
				case COMMAND_ID_PHOTO_DETAILS:
					photoDetailsListener.onPhotoDetailsSuccess(
							((PhotoDetailsCommand)command).getPhotoPlayListResponse());
					break;
				case COMMAND_ID_AUDIO_LIST:
					audioPlayListListener.onAudioPlayListSuccess(
							((AudioPlayListCommand)command).getAudioPlayListResponse());
					break;
				case COMMAND_ID_AUDIO_DETAILS:
					audioDetailsListener.onAudioDetailsSuccess(
							((AudioDetailsCommand)command).getAudioPlayListResponse());
					break;
				default:
					break;
			}
		}
		
		@Override
		public void commandCancelled(Command command) {	
		}
	};
}
