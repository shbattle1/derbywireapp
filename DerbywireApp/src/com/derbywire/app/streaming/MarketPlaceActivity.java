package com.derbywire.app.streaming;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants;
import com.derbywire.app.common.LogFile;
import com.derbywire.app.streaming.IPlayListListener.IAudioDetailsListener;
import com.derbywire.app.streaming.IPlayListListener.IAudioPlayListListener;

public class MarketPlaceActivity extends FragmentActivity {
	final String TAG = MarketPlaceActivity.class.getSimpleName();
	Context mContext = null;
	TextView title = null;

	private LinearLayout mainLayout;
	private View cell;
	private ViewPager viewPager;
	private ViewPager trackDetailedView;
	FragmentPagerAdapter adapterViewPager;
	private HorizontalScrollView gallery;
	boolean isCollapseGallery = true;
	PlayListHandler playListHandler = null;

	/** An array of strings to populate dropdown list */
	String[] actions = new String[] { "music", "video", "photo" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_layout);
		mContext = this;

		intResources();
		setClickListerners();

		adapterViewPager = new MyPagerAdapter1(getSupportFragmentManager());
		trackDetailedView.setAdapter(adapterViewPager);

		setActionBarProperties();

		addgalleryViews();
		playListHandler = new PlayListHandler();

		/*
		 * getAudioPlayList(28); getAudioDetails(5);
		 */

	}

	private void getAudioPlayList(int userId) {
		if (playListHandler != null) {
			playListHandler.getAudioPlayList(userId,
					new IAudioPlayListListener() {

						@Override
						public void onAudioPlayListSuccess(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null
									&& audioPlayListResponse.getAudioPlayList() != null) {
								audioPlayListResponse.printAudioList();
							}
						}

						@Override
						public void onAudioPlayListFail(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null) {
								LogFile.ShowLog(
										"getAudioPlayList",
										"error message: "
												+ audioPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getAudioPlayList",
										"error status: "
												+ audioPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}

	private void getAudioDetails(int audioId) {
		if (playListHandler != null) {
			playListHandler.getAudioDetails(audioId,
					new IAudioDetailsListener() {

						@Override
						public void onAudioDetailsSuccess(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null
									&& audioPlayListResponse.getAudioPlayList() != null) {
								AudioDetails audioDetails = audioPlayListResponse
										.getAudioPlayList().get(0);
								if (audioDetails != null) {
									audioDetails.printAudioDetails();
								}
							}
						}

						@Override
						public void onAudioDetailsFail(
								AudioPlayListResponse audioPlayListResponse) {
							if (audioPlayListResponse != null) {
								LogFile.ShowLog(
										"getAudioDetails",
										"error message: "
												+ audioPlayListResponse
														.getMessage());
								LogFile.ShowLog(
										"getAudioDetails",
										"error status: "
												+ audioPlayListResponse
														.getStatus());
							}
						}
					});
		}
	}

	private void setActionBarProperties() {
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#FF6A00")));
		actionBar.setTitle(" playlist | music");
		actionBar.setLogo(R.drawable.ic_intro_screen_b);
		// actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		// setActionBarDropDown();
	}

	@SuppressWarnings("unused")
	private void setActionBarDropDown() {
		/** Create an array adapter to populate dropdownlist */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getBaseContext(),
				android.R.layout.simple_spinner_dropdown_item, actions);

		/** Defining Navigation listener */
		ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition,
					long itemId) {
				Toast.makeText(getBaseContext(),
						"You selected : " + actions[itemPosition],
						Toast.LENGTH_SHORT).show();
				return false;
			}
		};

		/**
		 * Setting dropdown items and item navigation listener for the actionbar
		 */
		getActionBar().setListNavigationCallbacks(adapter, navigationListener);
	}

	private void addgalleryViews() {
		for (int i = 0; i < AppConstants.tracksImageId.length; i++) {

			cell = getLayoutInflater().inflate(R.layout.cell, null);
			final ImageView imageView = (ImageView) cell
					.findViewById(R.id._image);

			imageView.setOnClickListener(imageViewClick);
			imageView.setId(i);
			imageView.setImageResource(AppConstants.tracksImageId[i]);

			mainLayout.addView(cell);
		}
	}

	private void intResources() {
		mainLayout = (LinearLayout) findViewById(R.id._linearLayout);
		viewPager = (ViewPager) findViewById(R.id._viewPager);
		trackDetailedView = (ViewPager) findViewById(R.id.trackDetailedView);
		gallery = (HorizontalScrollView) findViewById(R.id.gallery);
	}

	private void setClickListerners() {

	}

	OnClickListener imageViewClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			viewPager.setVisibility(View.VISIBLE);
			viewPager.setAdapter(new GalleryPagerAdapter(
					MarketPlaceActivity.this, AppConstants.tracksImageId));
			viewPager.setCurrentItem(v.getId());
		}
	};

	@Override
	public void onBackPressed() {

		if (viewPager != null && viewPager.isShown()) {

			viewPager.setVisibility(View.GONE);
		} else {

			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.marketBtn:
			Toast.makeText(this, "MarketPlace selected", Toast.LENGTH_SHORT)
					.show();
			break;
		// action with ID action_settings was selected
		case R.id.favBtn:
			Toast.makeText(this, "Fav selected", Toast.LENGTH_SHORT).show();
			break;
		case R.id.profileBtn:
			Toast.makeText(this, "Profile selected", Toast.LENGTH_SHORT).show();
			break;
		// action with ID action_settings was selected
		case R.id.addBtn:
			Toast.makeText(this, "Add selected", Toast.LENGTH_SHORT).show();
			break;
		case R.id.collapsBtn:
			Toast.makeText(this, "View Collapse selected", Toast.LENGTH_SHORT)
					.show();
			if (isCollapseGallery == true) {
				gallery.setVisibility(View.GONE);
			} else {
				gallery.setVisibility(View.VISIBLE);
			}
			isCollapseGallery = !isCollapseGallery;
			break;
		default:
			break;
		}

		return true;
	}

	private class MyPagerAdapter1 extends FragmentPagerAdapter {

		public MyPagerAdapter1(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {

			case 0:
				return new DetailsFirstPage();
			case 1:
				return new DetailsSecondPage();
			case 2:
			default:
				return new DetailsFirstPage();
			}
		}

		@Override
		public int getCount() {
			return 2;
		}

		// Returns the page title for the top indicator
		@Override
		public CharSequence getPageTitle(int position) {
			return "Page " + position;
		}
	}

}
