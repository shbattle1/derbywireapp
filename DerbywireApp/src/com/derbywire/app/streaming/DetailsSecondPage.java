package com.derbywire.app.streaming;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.detailsPageNo;

public class DetailsSecondPage extends Fragment {
	private ListView tracksList;
	private ImageButton prevBtn;

	/*
	 * // newInstance constructor for creating fragment with arguments public
	 * static DetailsSecondPage newInstance(int page, String title) {
	 * DetailsSecondPage fragmentFirst = new DetailsSecondPage(); Bundle args =
	 * new Bundle(); args.putInt("someInt", page); args.putString("someTitle",
	 * title); fragmentFirst.setArguments(args); return fragmentFirst; }
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View myFragmentView = inflater.inflate(R.layout.details_second, null,
				false);

		tracksList = (ListView) myFragmentView.findViewById(R.id.trackList);
		tracksList.setAdapter(new TracksListAdapter(getActivity(),
				detailsPageNo.PAGE_TWO));

		prevBtn = (ImageButton) myFragmentView.findViewById(R.id.prevPage);
		prevBtn.setOnClickListener(prevPageClicked);
		return myFragmentView;
	}

	OnClickListener prevPageClicked = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			MusicListActivity musicListActivity = (MusicListActivity) getActivity();
			musicListActivity.showPrevDetailPage();
		}
	};

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("DO NOT CRASH", "OK");
	}
}
