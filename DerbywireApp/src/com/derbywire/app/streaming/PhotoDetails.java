package com.derbywire.app.streaming;

import android.util.Log;

public class PhotoDetails {

	private final String TAG = "PhotoDetails";
	
	int images_track_id;
	String attachment_file_name;
	String image_orginal_name;
	String attachment_content_type;
	int attachment_file_size;
	String attachment_updated_at;
	String title;
	int user_id;
	int album_image_id;
	int published;
	String description;
	
	public int getImages_track_id() {
		return images_track_id;
	}

	public void setImages_track_id(int images_track_id) {
		this.images_track_id = images_track_id;
	}

	public String getAttachment_file_name() {
		return attachment_file_name;
	}

	public void setAttachment_file_name(String attachment_file_name) {
		this.attachment_file_name = attachment_file_name;
	}

	public String getImage_orginal_name() {
		return image_orginal_name;
	}

	public void setImage_orginal_name(String image_orginal_name) {
		this.image_orginal_name = image_orginal_name;
	}

	public String getAttachment_content_type() {
		return attachment_content_type;
	}

	public void setAttachment_content_type(String attachment_content_type) {
		this.attachment_content_type = attachment_content_type;
	}

	public int getAttachment_file_size() {
		return attachment_file_size;
	}

	public void setAttachment_file_size(int attachment_file_size) {
		this.attachment_file_size = attachment_file_size;
	}

	public String getAttachment_updated_at() {
		return attachment_updated_at;
	}

	public void setAttachment_updated_at(String attachment_updated_at) {
		this.attachment_updated_at = attachment_updated_at;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getAlbum_image_id() {
		return album_image_id;
	}

	public void setAlbum_image_id(int album_image_id) {
		this.album_image_id = album_image_id;
	}

	public int getPublished() {
		return published;
	}

	public void setPublished(int published) {
		this.published = published;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StringBuilder printPhotoDetails(){
		StringBuilder logString = new StringBuilder();
		
		logString.append("images_track_id:" + getImages_track_id()).append("\n")
		.append("attachment_file_name:" + getAttachment_file_name()).append("\n")
		.append("image_orginal_name:" + getImage_orginal_name()).append("\n")
		.append("attachment_content_type:" + getAttachment_content_type()).append("\n")
		.append("attachment_file_size:" + getAttachment_file_size()).append("\n")
		.append("attachment_updated_at:" + getAttachment_updated_at()).append("\n")
		.append("title:" + getTitle()).append("\n")
		.append("user_id:" + getUser_id()).append("\n")
		.append("album_image_id:" + getAlbum_image_id()).append("\n")
		.append("published:" + getPublished()).append("\n")
		.append("description:" + getDescription());

		Log.i(TAG, "images_track_id:" + getImages_track_id());
		Log.i(TAG, "attachment_file_name:" + getAttachment_file_name());
		Log.i(TAG, "image_orginal_name:" + getImage_orginal_name());
		Log.i(TAG, "attachment_content_type:" + getAttachment_content_type());
		Log.i(TAG, "attachment_file_size:" + getAttachment_file_size());
		Log.i(TAG, "attachment_updated_at:" + getAttachment_updated_at());
		Log.i(TAG, "title:" + getTitle());
		Log.i(TAG, "user_id:" + getUser_id());
		Log.i(TAG, "album_image_id:" + getAlbum_image_id());
		Log.i(TAG, "published:" + getPublished());
		Log.i(TAG, "description:" + getDescription());

		return logString;
	}
}
