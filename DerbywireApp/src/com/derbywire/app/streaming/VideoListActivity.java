package com.derbywire.app.streaming;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.derbywire.app.R;

public class VideoListActivity extends Activity {
	private VideoView mVideoView;

	MediaController mediaController;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.video_player);
		mVideoView = (VideoView) findViewById(R.id.surface_view);

		mediaController = new MediaController(this);
		// set the path for video here
		Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.wildlife);
		// set the VideoView id in xml file here
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		mVideoView.setMediaController(mediaController);
		mVideoView.setVideoURI(video);
		mVideoView.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				// first starting the video, when loaded
				mVideoView.start();
				// then waiting for 1 millisecond
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// then pausing the video. i guess it's the first frame
				mVideoView.pause();
				// showing the control buttons here
				mediaController.show();
			}
		});
	}
}
