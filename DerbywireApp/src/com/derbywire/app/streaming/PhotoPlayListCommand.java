package com.derbywire.app.streaming;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.derbywire.app.common.AppConstants;
import com.derbywire.app.common.RestAPIConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.ICommandListener;
import com.derbywire.app.core.IDependencyTaskListener;
import com.derbywire.app.core.network.ConnectionManager;
import com.derbywire.app.core.network.NetworkResponse;

public class PhotoPlayListCommand extends Command{

	private PhotoPlayListResponse photoPlayListResponse = null;
	private int userId = 0;
	
	public PhotoPlayListResponse getPhotoPlayListResponse() {
		return photoPlayListResponse;
	}

	public void setPhotoPlayListResponse(PhotoPlayListResponse photoPlayListResponse) {
		this.photoPlayListResponse = photoPlayListResponse;
	}

	public PhotoPlayListCommand(Command_Id commandId, String commandName,
			ICommandListener listener,
			IDependencyTaskListener dependecyTaskListener, int priority,
			Bundle extraParams) {
		super(commandId, commandName, listener, dependecyTaskListener, priority,
				extraParams);
		if( extraParams != null ){
			userId = extraParams.getInt(AppConstants.IdPref);
		}
	}
	
	private void doNetworkOpration() {
		String networkUrl = String.format(
				RestAPIConstants.getInstance().PHOTO_LIST_URL, userId);
		byte[] buffer = null;
		NetworkResponse response;
		String content = "";
		
		try {
			response = ConnectionManager.connect(networkUrl,
					HttpGet.METHOD_NAME, createHeaders(""), buffer);
			content = response.getContent();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!TextUtils.isEmpty(content)) {
			parseResponse(content);
		}	
	}
	
	private void parseResponse(String content){		
		photoPlayListResponse = new PhotoPlayListResponse();
		List<PhotoDetails> photoPlayList = new ArrayList<PhotoDetails>();
		
		if( content != null && photoPlayList != null ){
			JSONObject root;
			try {
				root = new JSONObject(content);
				JSONArray entries = root.optJSONArray("play_list");
				
				for(int index=0 ;index<entries.length();index++){
					PhotoDetails photoDetails = new PhotoDetails();			
					JSONArray subentries = entries.optJSONArray(index);

					JSONObject jsonObj = subentries.optJSONObject(0);
					
					photoDetails.setImages_track_id(jsonObj.optInt("images_track_id"));
					photoDetails.setAttachment_file_name(jsonObj.optString("attachment_file_name"));
					photoDetails.setImage_orginal_name(jsonObj.optString("image_orginal_name"));
					photoDetails.setAttachment_content_type(jsonObj.optString("attachment_content_type"));
					photoDetails.setAttachment_file_size(jsonObj.optInt("attachment_file_size"));
					photoDetails.setAttachment_updated_at(jsonObj.optString("attachment_updated_at"));
					photoDetails.setTitle(jsonObj.optString("title"));
					photoDetails.setUser_id(jsonObj.optInt("user_id"));
					photoDetails.setAlbum_image_id(jsonObj.optInt("album_image_id"));
					photoDetails.setPublished(jsonObj.optInt("album_image_id"));
					photoDetails.setDescription(jsonObj.optString("description"));
					
					photoPlayList.add(photoDetails);
				}
				photoPlayListResponse.setPhotoPlayList(photoPlayList);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}

		
	}
	
	@Override
	public void clean() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute() throws Exception {
		doNetworkOpration();
	}

	@Override
	public void init() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void errorOcurredWhileExecutingCommand() {
		// TODO Auto-generated method stub
		
	}

}
