package com.derbywire.app.streaming;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.detailsPageNo;

/**
 *
 * @author Snehal Shinde
 *
 */
public class TracksListAdapter extends BaseAdapter {

	private Context context = null;
	private LayoutInflater mInflater = null;
	private detailsPageNo pageNo = detailsPageNo.PAGE_ONE;
	View view;

	public TracksListAdapter(Context context, detailsPageNo pageNo) {
		this.context = context;
		this.pageNo = pageNo;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 12;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (pageNo == detailsPageNo.PAGE_ONE) {
			view = mInflater.inflate(R.layout.playlist1, null, false);
		} else if (pageNo == detailsPageNo.PAGE_TWO) {
			view = mInflater.inflate(R.layout.playlist2, null, false);
		}

		if (position % 2 == 0) {
			setBackground(
					view,
					context.getResources().getColor(
							R.color.menu_left_background),
					detailsPageNo.PAGE_ONE);
		} else {
			setBackground(
					view,
					context.getResources().getColor(
							R.color.menu_right_background),
					detailsPageNo.PAGE_TWO);
		}
		return view;
	}

	private void setBackground(View view, int color, detailsPageNo pageNum) {
		TextView textView1 = (TextView) view.findViewById(R.id.t1);
		TextView textView2 = (TextView) view.findViewById(R.id.t2);
		TextView textView3 = (TextView) view.findViewById(R.id.t3);
		TextView textView4 = (TextView) view.findViewById(R.id.t4);
		TextView textView5 = (TextView) view.findViewById(R.id.t5);

		textView1.setBackgroundColor(color);
		textView2.setBackgroundColor(color);
		textView3.setBackgroundColor(color);
		textView4.setBackgroundColor(color);
		textView5.setBackgroundColor(color);

		if (pageNo == detailsPageNo.PAGE_ONE) {
		} else if (pageNo == detailsPageNo.PAGE_TWO) {
			LinearLayout priceSection = (LinearLayout) view
					.findViewById(R.id.price_section);
			priceSection.setBackgroundColor(color);
			LinearLayout commentsSection = (LinearLayout) view
					.findViewById(R.id.comments_section);
			commentsSection.setBackgroundColor(color);
			/*
			 * ImageButton priceBtn = (ImageButton)
			 * view.findViewById(R.id.priceBtn);
			 * priceBtn.setBackgroundColor(color);
			 */
		}

	}

	/*
	 * private static final String TAG =
	 * TracksListAdapter.class.getSimpleName();
	 * 
	 * private Context context = null; private List<NotesInfo> mNotesList =
	 * null; private LayoutInflater mInflater = null; private boolean
	 * mSelectionMode = false; private boolean mAnimationEnabled = false;
	 * private ArrayList<BookNote> mSelectedNotes = new ArrayList<BookNote>();
	 * 
	 * public TracksListAdapter(Context context, List<NotesInfo> notesList) {
	 * this.context = context; this.mNotesList = notesList; mInflater =
	 * (LayoutInflater) context
	 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE); }
	 * 
	 * @Override public int getCount() { if (null != mNotesList &&
	 * mNotesList.isEmpty() == false) { return mNotesList.size(); } else return
	 * 0; }
	 * 
	 * @Override public Object getItem(int position) { if( null != mNotesList &&
	 * mNotesList.isEmpty() == false ){ return mNotesList.get(position); }else
	 * return null; }
	 * 
	 * @Override public long getItemId(int position) { return position; }
	 * 
	 * @Override public View getView(int position, View convertView, ViewGroup
	 * parent) {
	 * 
	 * View rowView = null;
	 * 
	 * if (convertView == null) { rowView =
	 * mInflater.inflate(R.layout.notesrow_layout, null, false); } else {
	 * rowView = convertView; }
	 * 
	 * final ImageView checkBox = (ImageView)
	 * rowView.findViewById(R.id.selectnote);
	 * checkBox.setOnClickListener(mCheckListener);
	 * 
	 * RelativeLayout headerRow =
	 * (RelativeLayout)rowView.findViewById(R.id.headerRow); RelativeLayout
	 * noteRow = (RelativeLayout)rowView.findViewById(R.id.noteRow);
	 * 
	 * TextView textView1 = (TextView) rowView.findViewById(R.id.NotesTitle1);
	 * TextView textView2 = (TextView) rowView.findViewById(R.id.NotesTitle2);
	 * TextView textView3 = (TextView)rowView.findViewById(R.id.NotesTitle3);
	 * ImageView colorCode = (ImageView) rowView.findViewById(R.id.colorCode);
	 * ImageView sharedNote = (ImageView)
	 * rowView.findViewById(R.id.sharedLable); TextView chapterNo = (TextView)
	 * rowView.findViewById(R.id.chapterNo); TextView notesNo = (TextView)
	 * rowView.findViewById(R.id.notesNo);
	 * 
	 * NotesInfo bookNotesInfo = mNotesList.get(position);
	 * 
	 * if( bookNotesInfo != null){ if( bookNotesInfo.getIsHeaderNote() == true
	 * ){ headerRow.setVisibility(View.VISIBLE);
	 * noteRow.setVisibility(View.GONE);
	 * 
	 * chapterNo.setText(bookNotesInfo.getChapterName());
	 * notesNo.setText(bookNotesInfo.getNoOfNotes() + " Notes");
	 * 
	 * 
	 * }else {
	 * 
	 * headerRow.setVisibility(View.GONE); noteRow.setVisibility(View.VISIBLE);
	 * noteRow
	 * .setBackgroundColor(context.getResources().getColor(R.color.white));
	 * 
	 * BookNote noteInfo = bookNotesInfo.getBookNote();
	 * 
	 * if (null != noteInfo) {
	 * 
	 * textView1.setText(getTruncatedText(noteInfo.getBookNoteData()));
	 * textView2.setText(getTruncatedText(noteInfo.getBookNoteTitle()));
	 * textView3.setText(getNotesDate(noteInfo.getNoteId()));
	 * 
	 * rowView.setTag(noteInfo); checkBox.setTag(noteInfo);
	 * 
	 * if (noteInfo.isSelected()) {
	 * checkBox.setImageResource(R.drawable.check_true); } else {
	 * checkBox.setImageResource(R.drawable.check_false); }
	 * 
	 * if (mSelectionMode) { checkBox.setVisibility(View.VISIBLE);
	 * setInAnimation(checkBox); } else { setOutAnimation(checkBox); }
	 * 
	 * try { if ( noteInfo.getNoteColor() != null &&
	 * noteInfo.getNoteColor().isEmpty() == false) {
	 * 
	 * System.out.println("color:" + noteInfo.getNoteColor() );
	 * 
	 * if( noteInfo.getNoteColor().equals("#FFD079") == true ||
	 * noteInfo.getNoteColor().equals("#FFD231") == true ){ //yellow color
	 * colorCode.setImageResource(R.drawable.yellow_note_corner); }else if(
	 * noteInfo.getNoteColor().equals("#66FFFF") == true ||
	 * noteInfo.getNoteColor().equals("#96D4D6") == true ){ //blue color
	 * colorCode.setImageResource(R.drawable.blue_note_corner);
	 * noteRow.setBackgroundColor(Color.parseColor("#E8F9F9"));
	 * 
	 * }else if( noteInfo.getNoteColor().equals("#54DF48") == true){ //green
	 * color colorCode.setImageResource(R.drawable.green_note_corner); }else if(
	 * noteInfo.getNoteColor().equals("#FB91CF") == true ||
	 * noteInfo.getNoteColor().equals("#FFACB4") == true ){ //pink color
	 * colorCode.setImageResource(R.drawable.pink_note_corner); }else {
	 * colorCode.setImageResource(R.drawable.yellow_note_corner); } } else {
	 * colorCode.setImageResource(R.drawable.yellow_note_corner); } } catch
	 * (IllegalArgumentException Ex) {
	 * colorCode.setImageResource(R.drawable.yellow_note_corner); }
	 * 
	 * if (noteInfo.getIsSharable() == true) {
	 * sharedNote.setVisibility(View.VISIBLE); } else {
	 * sharedNote.setVisibility(View.INVISIBLE); }
	 * 
	 * } } }
	 * 
	 * 
	 * return rowView; }
	 * 
	 * private CharSequence getTruncatedText(String bookNoteData) { String
	 * finalText = ""; if( bookNoteData != null && bookNoteData.isEmpty() ==
	 * false ){ finalText = bookNoteData.replace('\n',' '); } return finalText;
	 * }
	 * 
	 * private CharSequence getNotesDate(String noteId) { SimpleDateFormat
	 * formatter = new SimpleDateFormat("dd/MM/yyyy"); String dateInString = "";
	 * 
	 * try { long timeStramp = Long.valueOf(noteId); Date date = (Date) new
	 * Date(timeStramp); dateInString = formatter.format(date);
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } return dateInString; }
	 * 
	 * private void setInAnimation(View checkBox) { if (mAnimationEnabled) {
	 * TransitionUtil.alphaIn(checkBox, 500, null); mAnimationEnabled = false; }
	 * }
	 * 
	 * private void setOutAnimation(final View checkBox) { if
	 * (mAnimationEnabled) { TransitionUtil.alphaOut(checkBox, 500, new
	 * AnimationListener() {
	 * 
	 * @Override public void onAnimationStart(Animation arg0) { }
	 * 
	 * @Override public void onAnimationRepeat(Animation arg0) { }
	 * 
	 * @Override public void onAnimationEnd(Animation arg0) {
	 * checkBox.setVisibility(View.GONE); } }); } }
	 * 
	 * public void toggleSelectionMode() { clearSelection(); mAnimationEnabled =
	 * true; mSelectionMode = !mSelectionMode; notifyDataSetChanged(); }
	 * 
	 * private void clearSelection() { for (int index = 0; index <
	 * mSelectedNotes.size(); index++) { BookNote noteInfo =
	 * mSelectedNotes.get(index); noteInfo.setSelected(false); }
	 * mSelectedNotes.clear(); }
	 * 
	 * private OnCheckedChangeListener mCheckChangeListener = new
	 * OnCheckedChangeListener() {
	 * 
	 * @Override public void onCheckedChanged(CompoundButton cmpButton, boolean
	 * selected) { BookNote noteInfo = (BookNote) cmpButton.getTag(); if
	 * (noteInfo != null) { noteInfo.setSelected(selected); Log.i(TAG,
	 * "note selected= " + noteInfo); if (selected) {
	 * mSelectedNotes.add(noteInfo); } else { mSelectedNotes.remove(noteInfo); }
	 * notifyDataSetChanged(); } } };
	 * 
	 * private OnClickListener mCheckListener = new OnClickListener() {
	 * 
	 * @Override public void onClick(View view) { BookNote noteInfo = (BookNote)
	 * view.getTag(); boolean selection = !noteInfo.isSelected(); if (noteInfo
	 * != null) { noteInfo.setSelected(selection); if (selection) {
	 * ((ImageView)view).setImageResource(R.drawable.check_true);
	 * mSelectedNotes.add(noteInfo); } else {
	 * ((ImageView)view).setImageResource(R.drawable.check_false);
	 * mSelectedNotes.remove(noteInfo); }
	 * 
	 * notifyDataSetChanged( ); } } };
	 * 
	 * public ArrayList<BookNote> getSelectedNotes() { return mSelectedNotes; }
	 */
}