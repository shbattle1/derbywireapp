package com.derbywire.app.core;


public interface IDependencyTaskListener {
	public void dependencyTaskCompleted(Command command);
}
