package com.derbywire.app.core.thumbnail;

import java.io.InputStream;

import android.os.Bundle;

import com.derbywire.app.cache.CacheConstants;
import com.derbywire.app.cache.CacheUtil;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.IDependencyTaskListener;
import com.derbywire.app.core.network.ConnectionManager;
import com.derbywire.app.core.network.NetworkResponse;

public class ThumbnailCommand extends Command{

	//private static final String TAG = ThumbnailCommand.class.getSimpleName();

	private ImageEntity mEntity;

	public ThumbnailCommand(Command_Id commandId, String commandName,
			IThumbCommandListener listener, IDependencyTaskListener dependecyTaskListener, int priority, Bundle extraParams) {
		
		super(commandId, commandName, listener, dependecyTaskListener, priority, extraParams);
		
		mEntity = (ImageEntity) extraParams.getSerializable(CacheConstants.KEY_ENTITY_OBJECT);
	}

	@Override
	public void clean() throws Exception {
		
	}

	@Override
	public void execute() throws Exception {
		String localFileName = mEntity.getImageLocalFileName();
		
		String filePath = CacheConstants.THUMB_DIR + localFileName;
		
		if(CacheUtil.checkIfFileExists(CacheConstants.CACHE_DIR+filePath)){
			((IThumbCommandListener)mListener).onThumbnailFetched(CacheConstants.CACHE_DIR+filePath);
			return;
		}
		
		String mediaUrl = mEntity.getImageUrl();
		
		NetworkResponse response = ConnectionManager.connect(mediaUrl, ConnectionManager.REQUEST_METHOD_GET, filePath, null, null);
		
		InputStream mediaStream = response.getStream();
		
		if(mediaStream != null){
			mediaStream.close();
		}
		
		((IThumbCommandListener)mListener).onThumbnailFetched(CacheConstants.CACHE_DIR+filePath);
	}

	@Override
	public void init() throws Exception {
		
	}

	@Override
	public void errorOcurredWhileExecutingCommand() {
		// TODO Auto-generated method stub
		
	}

}