package com.derbywire.app.core.thumbnail;

import com.derbywire.app.core.ICommandListener;


public interface IThumbCommandListener extends ICommandListener{

	public void onThumbnailFetched(String thumbnailPath);
}
