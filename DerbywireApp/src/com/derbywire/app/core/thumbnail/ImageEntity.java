package com.derbywire.app.core.thumbnail;


/**
 * 
 * @author snehal.shinde
 *
 */

public class ImageEntity {
	
	private String mImageUrl = "";
	private String mImageLocalFileName = "";
	private String mImageLocalFilePath = "";
	
	public void setImageUrl(String imageUrl) {
		this.mImageUrl = imageUrl;
	}

	public String getImageUrl() {
		return mImageUrl;
	}

	public void setImageLocalFilePath(String imageLocalFilePath) {
		this.mImageLocalFilePath = imageLocalFilePath;
	}

	public String getImageLocalFilePath() {
		return mImageLocalFilePath;
	}

	public String getImageLocalFileName() {
		return mImageLocalFileName;
	}

	public void setImageLocalFileName(String imageLocalFileName) {
		this.mImageLocalFileName = imageLocalFileName;
	}

}