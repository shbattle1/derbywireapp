package com.derbywire.app.core;

import java.util.HashMap;

import org.apache.http.protocol.HTTP;

import android.os.Bundle;

import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.network.NetworkStatus;


public abstract class Command{
	
	public static final int PRIORITY_SEQUENTIAL = 0;
	public static final int PRIORITY_MAX = 1;
	public static final int PRIORITY_PARALLEL = 2;
	public static final int PRIORITY_THUMBNAIL = 3;
	
	
	private Command_Id mCommandId;
	protected String mCommandName;
	
	private Exception commandException = null;
	
	protected static CommandController mCommandController = null;
	protected ICommandListener mListener;
	protected int mPriority = PRIORITY_SEQUENTIAL;
		
	public static String[] errorStrings = new String[]{
											"Network Problem",
											"Internal Error has Occurred. Please retry."
											};
	
	protected boolean mCommandCancelled = false;
	
	protected IDependencyTaskListener mDependecyTaskListener;
	private NetworkStatus networkStatus;
	
	public Command(Command_Id commandId, String commandName, ICommandListener listener, IDependencyTaskListener dependecyTaskListener, int priority, Bundle extraParams){
		mCommandId = commandId;
		mCommandName = commandName;
		mListener = listener;
		mPriority = priority;
		mDependecyTaskListener = dependecyTaskListener;
	}
	
	public abstract void clean() throws Exception;
	
	public abstract void execute() throws Exception;
	
	public abstract void init() throws Exception;
	
	public abstract void errorOcurredWhileExecutingCommand();

	public int getPriority(){
		return mPriority;
	}

	public Command_Id getCommandId() {
		return mCommandId;
	}
	
	public void destroy() {
		mListener = null;
	}

	public Exception getCommandException() {
		return commandException;
	}

	public void setCommandId(Command_Id mCommandId) {
		this.mCommandId = mCommandId;
	}
	
	public void setCommandName(String commandName) {
		this.mCommandName = commandName;
	}

	public void setStatus(NetworkStatus networkStatus) {
		this.networkStatus = networkStatus;
	}
	
	public NetworkStatus getStatus() {
		return this.networkStatus ;
	}
	
	protected HashMap<String, String> createHeaders(String authToken) {	
		HashMap<String, String> postParameters = new HashMap<String, String>();

		postParameters.put(HTTP.CONTENT_TYPE, "application/json");
		postParameters.put("authToken", authToken);
		return postParameters;
	}
}