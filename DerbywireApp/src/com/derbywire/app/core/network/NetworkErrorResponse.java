package com.derbywire.app.core.network;

public class NetworkErrorResponse {

	private String mDevMsg;
	private String mMsg;
	private String mCode;
	private String mStatus;

	public void setStatus(String status) {
		mStatus = status;
	}

	public void setCode(String code) {
		mCode = code;
	}

	public void setMessage(String msg) {
		mMsg = msg;
	}

	public void setDevMessage(String devMsg) {
		mDevMsg = devMsg;
	}

	public String getStatus() {
		return mStatus;
	}

	public String getCode() {
		return mCode;
	}

	public String getMessage() {
		return mMsg;
	}

	public String getDevMessage() {
		return mDevMsg;
	}

}
