package com.derbywire.app.core.network;

import android.util.Log;

public class NetworkStatus {

	public static final int STATUS_CODE_SUCCESS = 0;
	public static final int STATUS_CODE_NO_NETWORK = 1;
	public static final int STATUS_CODE_INTERNAL_ERROR = 2;
	private static final String TAG = NetworkStatus.class.getSimpleName();
	
	private static String[] statusMessages = {
		"Success",
		"No Network Connectivity. Please try again later",
		"Unknown error has ocurred",
	};
	
	private int statusCode = STATUS_CODE_SUCCESS;
//	private String statusMessage;
	private Exception exception;

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the errorCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		String statusMessage = "Invalid Error Message";
		try{
			statusMessage = statusMessages[statusCode];	
		}catch(Exception e){
			Log.e(TAG, ""+e);
		}
		
		return statusMessage;
	}

	/**
	 * @param statusMessage the statusMessage to set
	 */
	/*public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}*/

}