package com.derbywire.app.core.network;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpHead;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.derbywire.app.cache.CacheConstants;
import com.derbywire.app.cache.CacheUtil;

public class ConnectionManager {
	
	public static final String REQUEST_METHOD_POST = "POST";
	public static final String REQUEST_METHOD_GET = "GET";
	
	public static Map<String, List<String>> connectHeaders(String networkUrl) throws Exception {
		URL url = new URL(networkUrl);
		
		HttpURLConnection mHttpConnection = null;
		if (url != null) {

			mHttpConnection = (HttpURLConnection) url.openConnection();

			mHttpConnection.setConnectTimeout(CacheConstants.TIME_OUT_READ);
			mHttpConnection.setReadTimeout(CacheConstants.TIME_OUT_READ);

			mHttpConnection.setRequestMethod(HttpHead.METHOD_NAME);
			
			mHttpConnection.connect();
		}
		
		Map<String, List<String>> headers = null;
		
		if (mHttpConnection != null) {
			if(mHttpConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
				headers = mHttpConnection.getHeaderFields();
			}
			
			if(mHttpConnection != null){
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}
		}

		return headers;
	}
	
	public static NetworkResponse connect(String networkUrl, String requestMethod, String responseFileName, HashMap<String, String> postParameters, byte[] postContent) throws Exception {
		NetworkResponse networkResponse = new NetworkResponse();
		
		InputStream stream = null;
		
		if(TextUtils.isEmpty(responseFileName)){
			responseFileName = CacheConstants.RESPONSE_FILE_NAME;
		}
		
		if(requestMethod == null){
			requestMethod = REQUEST_METHOD_GET;
		}
		
		URL url = new URL(networkUrl);
		Log.i("NETWORK URL LIB:", ""+networkUrl);
		HttpURLConnection mHttpConnection = null;
		if (url != null) {

			mHttpConnection = (HttpURLConnection) url.openConnection();

			mHttpConnection.setConnectTimeout(CacheConstants.TIME_OUT_READ);
			mHttpConnection.setReadTimeout(CacheConstants.TIME_OUT_READ);

			mHttpConnection.setRequestMethod(requestMethod);
			
			if(postParameters != null){
				setRequestProperties(mHttpConnection, postParameters);
			}

			if(Build.VERSION.SDK_INT <= CacheConstants.VERSION_HONEYCOMB && (!HttpDelete.METHOD_NAME.equals(requestMethod))){
				mHttpConnection.setDoOutput(true);
			}

			mHttpConnection.connect();
		}
		
		if (mHttpConnection != null) {

			if (postContent != null) { 
				DataOutputStream dataOutputStream = new DataOutputStream(
						mHttpConnection.getOutputStream());
				dataOutputStream.write(postContent);
				dataOutputStream.close();
			}
			
			if(mHttpConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
				stream = mHttpConnection.getInputStream();
				stream = CacheUtil.writeInputStreamToFile(
						responseFileName, stream);
				networkResponse.setStream(stream);
			}else{
				
				NetworkErrorResponse derbywireError = new NetworkErrorResponse();
				
				networkResponse.setResponseCode(mHttpConnection.getResponseCode());
				networkResponse.setderbywireError(derbywireError);
				networkResponse.setContent(null);
				networkResponse.setStream(null);
			}
			
			if(mHttpConnection != null){
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}
		}

		return networkResponse;
	}
	
	public static NetworkResponse connectPlain(String networkUrl, String requestMethod, HashMap<String, String> postParameters, byte[] postContent) throws Exception {
		NetworkResponse networkResponse = new NetworkResponse();
		
		InputStream stream = null;
		
		if(requestMethod == null){
			requestMethod = REQUEST_METHOD_GET;
		}
		
		URL url = new URL(networkUrl);
		Log.i("NETWORK URL LIB:", ""+networkUrl);
		HttpURLConnection mHttpConnection = null;
		if (url != null) {

			mHttpConnection = (HttpURLConnection) url.openConnection();

			mHttpConnection.setConnectTimeout(CacheConstants.TIME_OUT_READ);
			mHttpConnection.setReadTimeout(CacheConstants.TIME_OUT_READ);

			mHttpConnection.setRequestMethod(requestMethod);
			
			if(postParameters != null){
				setRequestProperties(mHttpConnection, postParameters);
			}

			if(Build.VERSION.SDK_INT <= CacheConstants.VERSION_HONEYCOMB && (!HttpDelete.METHOD_NAME.equals(requestMethod))){
				mHttpConnection.setDoOutput(true);
			}

			mHttpConnection.connect();
		}
		
		if (mHttpConnection != null) {

			if (postContent != null) { 
				DataOutputStream dataOutputStream = new DataOutputStream(
						mHttpConnection.getOutputStream());
				dataOutputStream.write(postContent);
				dataOutputStream.close();
			}
			
			if(mHttpConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
				stream = mHttpConnection.getInputStream();
				networkResponse.setStream(stream);
				networkResponse.setConnection(mHttpConnection);
			}else{
				
				NetworkErrorResponse derbywireError = new NetworkErrorResponse();
				
				networkResponse.setResponseCode(mHttpConnection.getResponseCode());
				networkResponse.setderbywireError(derbywireError);
				networkResponse.setContent(null);
				networkResponse.setStream(null);
				networkResponse.setConnection(mHttpConnection);
			}
			
			/*if(mHttpConnection != null){
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}*/
		}

		return networkResponse;
	}
	
	public static NetworkResponse connect(String networkUrl, String requestMethod, HashMap<String, String> postParameters, byte[] postContent) throws Exception {
		NetworkResponse networkResponse = new NetworkResponse();
		
		String content = null;
		
		if(requestMethod == null){
			requestMethod = REQUEST_METHOD_GET;
		}
		
		URL url = new URL(networkUrl);
		Log.i("NETWORK URL LIB:", ""+networkUrl);
		HttpURLConnection mHttpConnection = null;
		if (url != null) {

			mHttpConnection = (HttpURLConnection) url.openConnection();

			mHttpConnection.setConnectTimeout(CacheConstants.TIME_OUT_READ);
			mHttpConnection.setReadTimeout(CacheConstants.TIME_OUT_READ);

			mHttpConnection.setRequestMethod(requestMethod);
			
			if(postParameters != null){
				setRequestProperties(mHttpConnection, postParameters);
			}

			if(Build.VERSION.SDK_INT <= CacheConstants.VERSION_HONEYCOMB && (!HttpDelete.METHOD_NAME.equals(requestMethod))){
				mHttpConnection.setDoOutput(true);
			}

			mHttpConnection.connect();
		}
		
		InputStream stream = null;
		if (mHttpConnection != null) {

			if (postContent != null) { 
				DataOutputStream dataOutputStream = new DataOutputStream(
						mHttpConnection.getOutputStream());
				dataOutputStream.write(postContent);
				dataOutputStream.close();
			}
			
			System.out.println("response code: " + mHttpConnection.getResponseCode());
			if(mHttpConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
			{
				stream = mHttpConnection.getInputStream();
				content = CacheUtil.readStream(stream);
				
				networkResponse.setResponseCode(HttpURLConnection.HTTP_OK);
				networkResponse.setContent(content);
				networkResponse.setErrorCode(NetworkResponse.NO_ERROR);
				
				System.out.println();
				System.out.println(" Network Response:    " + content);
			}else{
				stream = mHttpConnection.getErrorStream();
				content = CacheUtil.readStream(stream);
				
				System.out.println();
				System.out.println(" Network Error Response:    " + content);
				
				NetworkErrorResponse derbywireError = parseErrorResponse(content);
				networkResponse.setResponseCode(mHttpConnection.getResponseCode());
				networkResponse.setderbywireError(derbywireError);
				networkResponse.setContent(null);
			}
			
			if(mHttpConnection != null){
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}
		}

		return networkResponse;
	}
	
	private static NetworkErrorResponse parseErrorResponse(String content) throws JSONException {
		NetworkErrorResponse derbywireErrorResponse = new NetworkErrorResponse();
		
		JSONObject root = new JSONObject(content);
		
		String status = root.optString("status");
		String code = root.optString("code");
		String msg = root.optString("msg");
		String devMsg = root.optString("devMsg");
		
		derbywireErrorResponse.setStatus(status);
		derbywireErrorResponse.setCode(code);
		derbywireErrorResponse.setMessage(msg);
		derbywireErrorResponse.setDevMessage(devMsg);
		
		return derbywireErrorResponse;
	}

	public static void connectInternal(String networkUrl, String requestMethod, String fileFullPath, HashMap<String, String> postParameters, byte[] postContent) throws Exception {
		
		if(requestMethod == null){
			requestMethod = REQUEST_METHOD_GET;
		}
		
		URL url = new URL(networkUrl);
		Log.i("NETWORK URL LIB:", ""+networkUrl);
		HttpURLConnection mHttpConnection = null;
		if (url != null) {

			mHttpConnection = (HttpURLConnection) url.openConnection();

			mHttpConnection.setConnectTimeout(CacheConstants.TIME_OUT_READ);
			mHttpConnection.setReadTimeout(CacheConstants.TIME_OUT_READ);

			mHttpConnection.setRequestMethod(requestMethod);
			
			if(postParameters != null){
				setRequestProperties(mHttpConnection, postParameters);
			}

			if(Build.VERSION.SDK_INT <= CacheConstants.VERSION_HONEYCOMB && (!HttpDelete.METHOD_NAME.equals(requestMethod))){
				mHttpConnection.setDoOutput(true);
			}

			mHttpConnection.connect();
		}
		
		InputStream stream = null;
		if (mHttpConnection != null) {

			if (postContent != null) { 
				DataOutputStream dataOutputStream = new DataOutputStream(
						mHttpConnection.getOutputStream());
				dataOutputStream.write(postContent);
				dataOutputStream.close();
			}
			
			if(mHttpConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
				long lastModified = mHttpConnection.getLastModified();
				stream = mHttpConnection.getInputStream();
				CacheUtil.writeInputStreamToFileOnInternalMemory(fileFullPath, stream, lastModified);
			}else{
				throw new Exception("Request Failed");
			}
			
			if(mHttpConnection != null){
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}
		}
	}
	
	private static void setRequestProperties(HttpURLConnection httpConnection, HashMap<String, String> properties) {
		Set<String> keys = properties.keySet();
		
		if (httpConnection != null) {
			for (String key : keys) {
				httpConnection.setRequestProperty(key, properties.get(key));
			}
		}
	}
}