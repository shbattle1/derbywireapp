package com.derbywire.app.core.network;

import java.io.InputStream;
import java.net.HttpURLConnection;

public class NetworkResponse {

	public static final String NO_ERROR = "-1";

	private int mResponseCode;
	private String mContent;

	private String mErrorCode;

	private NetworkErrorResponse mderbywireError;

	private InputStream mStream;

	private HttpURLConnection mHttpConnection;

	public void setResponseCode(int resCode) {
		mResponseCode = resCode;
	}

	public int getResponseCode() {
		return mResponseCode;
	}

	public void setContent(String content) {
		mContent = content;
	}

	public String getContent() {
		return mContent;
	}

	public void setErrorCode(String errorCode) {
		mErrorCode = errorCode;
	}

	public String getErrorCode() {
		return mErrorCode;
	}

	public void setderbywireError(NetworkErrorResponse derbywireError) {
		mderbywireError = derbywireError;
	}

	public NetworkErrorResponse getderbywireError() {
		return mderbywireError;
	}

	public void setStream(InputStream stream) {
		mStream = stream;
	}

	public InputStream getStream() {
		return mStream;
	}

	public void setConnection(HttpURLConnection httpConnection) {
		mHttpConnection = httpConnection;
	}

	public HttpURLConnection getConnection() {
		return mHttpConnection;
	}

	public void destroy() {
		try {
			if (mStream != null) {
				mStream.close();
			}
		} catch (Exception e) {
		}

		try {
			if (mHttpConnection != null) {
				mHttpConnection.disconnect();
				mHttpConnection = null;
			}
		} catch (Exception e) {

		}
	}
}