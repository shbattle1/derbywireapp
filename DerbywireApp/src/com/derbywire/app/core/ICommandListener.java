package com.derbywire.app.core;


public interface ICommandListener {

	public void commandStarted(Command command);
	public void commandInProgress(Command command);
	public void commandCompleted(Command command);
	public void commandFailed(Command command);	
	public void commandCancelled(Command command);	
	public void commandQueued(Command command);
}
