package com.derbywire.app.core;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;

import com.derbywire.app.common.CommandConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.network.NetworkStatus;

/**
 *
 * @author Snehal Shinde
 *
 */

public class CommandController {

	private static final String TAG = CommandController.class.getSimpleName();

	private static final int MAX_THREAD_NUM = 5;
	private static CommandController INSTANCE = null;
	private ThreadRunnable mCommandThreadRunnables[];
	private ThreadRunnable mSequentialRunnable;
	private ThreadRunnable mThumbnailRunnable;
	public boolean mCancelRequest;

	private Vector<Command> mParallelTasksVector = null;

	private LinkedBlockingQueue<Command> mParallelTasksCommonQueue = null;
	private LinkedBlockingQueue<Command> mSequentialTasksQ = null;
	private LinkedBlockingQueue<Command> mThumbnailTasksQ = null;

	private CommandController() {
		mCommandThreadRunnables = new ThreadRunnable[MAX_THREAD_NUM];

		mParallelTasksCommonQueue = new LinkedBlockingQueue<Command>();
		mSequentialTasksQ = new LinkedBlockingQueue<Command>();
		mThumbnailTasksQ = new LinkedBlockingQueue<Command>();

		mParallelTasksVector = new Vector<Command>();

		initQueuesAndThreads();
	}

	private void initQueuesAndThreads() {
		for (int index = 0; index < MAX_THREAD_NUM; index++) {
			mCommandThreadRunnables[index] = new ThreadRunnable(index + 1,
					"Parallel Thread");
			mCommandThreadRunnables[index]
					.setCommandsQueue(mParallelTasksCommonQueue);
			mCommandThreadRunnables[index]
					.setThreadPriority(Thread.MIN_PRIORITY);
			mCommandThreadRunnables[index].start();
		}

		mSequentialRunnable = new ThreadRunnable(MAX_THREAD_NUM + 1,
				"Sequential Thread");
		mSequentialRunnable.setCommandsQueue(mSequentialTasksQ);
		mSequentialRunnable.setThreadPriority(Thread.MAX_PRIORITY);
		mSequentialRunnable.start();

		mThumbnailRunnable = new ThreadRunnable(MAX_THREAD_NUM + 2,
				"Thumbnail Thread");
		mThumbnailRunnable.setCommandsQueue(mThumbnailTasksQ);
		mThumbnailRunnable.setThreadPriority(Thread.NORM_PRIORITY);
		mThumbnailRunnable.start();
	}

	public static CommandController getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CommandController();
		}
		return INSTANCE;
	}

	public void queueCommand(Command command) throws Exception {
		mCancelRequest = false;

		if (command.mListener != null) {
			command.mListener.commandQueued(command);
		}

		int priority = command.getPriority();

		if (priority == Command.PRIORITY_PARALLEL) {
			mParallelTasksCommonQueue.add(command);
			mParallelTasksVector.add(command);
			Log.d("CacheManager in CommCOntrolelr : q size = ",
					mParallelTasksCommonQueue.size() + "");
		} else if (priority == Command.PRIORITY_THUMBNAIL) {
			mThumbnailTasksQ.add(command);
		} else if (priority == Command.PRIORITY_MAX) {
			addToTopOfQueue(mSequentialTasksQ, command);
		} else if (priority == Command.PRIORITY_SEQUENTIAL) {
			mSequentialTasksQ.add(command);
		}
	}

	private void addToTopOfQueue(LinkedBlockingQueue<Command> mFreeQueue,
			Command newCommand) {
		Vector<Command> commands = new Vector<Command>();
		mFreeQueue.drainTo(commands);
		mFreeQueue.clear();
		commands.add(0, newCommand);
		mFreeQueue.addAll(commands);
	}

	public void cancelNetworkRequest() {
		mCancelRequest = true;
	}

	private class ThreadRunnable implements Runnable {

		private int mThreadIndex;
		private boolean mCommandExecuting = false;
		private Thread mCommandThread;
		private LinkedBlockingQueue<Command> mCommandQueue = null;
		private boolean mCommandControllerRequired = true;
		private String mThreadName;

		public ThreadRunnable(int index, String name) {
			mThreadName = name;
			mCommandControllerRequired = true;
			mThreadIndex = index;
			mCommandThread = new Thread(this);
			mCommandThread.setPriority(Thread.MIN_PRIORITY);
		}

		public LinkedBlockingQueue<Command> getCommandsQueue() {
			return mCommandQueue;
		}

		public void setCommandsQueue(LinkedBlockingQueue<Command> queue) {
			mCommandQueue = queue;
		}

		public void setThreadPriority(int priority) {
			mCommandThread.setPriority(priority);
		}

		public void start() {
			mCommandThread.start();
		}

		@Override
		public void run() {
			ICommandListener listener = null;
			Command command = null;
			while (mCommandControllerRequired) {
				try {
					Log.e(TAG, mThreadName + "/" + mThreadIndex
							+ " Waiting for New Command !!!! ");

					command = mCommandQueue.take();

					mParallelTasksVector.remove(command);

					if (command.getCommandId() == Command_Id.COMMAND_ID_NULL) {
						mCommandControllerRequired = false;
						break;
					}

					listener = command.mListener;

					mCommandExecuting = true;

					Log.i(TAG, mThreadName + "/" + mThreadIndex
							+ " Executing Command !!!! ");
					if (listener != null) {
						listener.commandStarted(command);
					}

					command.init();
					if (listener != null) {
						listener.commandInProgress(command);
					}
					command.execute();

					command.clean();

					mCommandExecuting = false;

					NetworkStatus networkStatus = new NetworkStatus();
					networkStatus.setStatusCode(NetworkStatus.STATUS_CODE_SUCCESS);

					if (listener != null) {
						listener.commandCompleted(command);
					}

				} catch (Exception e) {
					// e.printStackTrace();

					Log.e(TAG, "" + e);
					NetworkStatus networkStatus = new NetworkStatus();

					if (command != null) {
						command.errorOcurredWhileExecutingCommand();
						networkStatus.setException(e);
						command.setStatus(networkStatus);
					}

					final String errorMessage = e.getMessage();

					System.out.println("Exception in commandcontroller: "
							+ errorMessage);

					if ((e instanceof ConnectException)
							|| (e instanceof UnknownHostException)
							|| (e instanceof SocketTimeoutException)) {
						// command.setErrorCode(PXEStatusMessages.ERROR_CODE_NO_NET);
						// pxeStatus.setStatusMessage(PXEStatusMessages.MESSAGES[ERROR_CODE_NO_NET]);
						networkStatus
								.setStatusCode(NetworkStatus.STATUS_CODE_NO_NETWORK);
					} else {
						networkStatus
								.setStatusCode(NetworkStatus.STATUS_CODE_INTERNAL_ERROR);
						// command.setErrorCode(PXEStatusMessages.ERROR_CODE_INTERNAL_ERROR);
					}

					if (command != null && listener != null) {
						listener.commandFailed(command);
					}
				}

				command.destroy();

			}// While ends here

			Log.e(TAG, "Command Controller Exited ID : " + mThreadName + "/"
					+ mThreadIndex);

		}

		@SuppressWarnings("unused")
		public boolean isCommandExecuting() {
			return mCommandExecuting;
		}
	}

	public void destroy() {
		try {
			queueDestroyCommandToAll();
			mParallelTasksVector.clear();
			INSTANCE = null;
			
			/*mParallelTasksCommonQueue.clear();
			mParallelTasksCommonQueue = null;
			
			mSequentialTasksQ.clear();
			mSequentialTasksQ = null;
			
			mThumbnailTasksQ.clear();
			mThumbnailTasksQ = null;*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void queueDestroyCommandToAll() {
		mCancelRequest = false;

		// ----------------- Clean Parallel Common Queue ----------------
		// Clear All Pending Commands from Common Queue
		mParallelTasksCommonQueue.clear();

		// Add MAX_THREAD_NUM Destroy commands to common queue. One of it will
		// be consumed by each thread and will exit.
		for (int index = 0; index < MAX_THREAD_NUM; index++) {
			DestroyCommand command = new DestroyCommand(null,
					Command_Id.COMMAND_ID_NULL,
					CommandConstants.COMMAND_TAG_NULL, null, null,
					Command.PRIORITY_MAX, null);
			LinkedBlockingQueue<Command> queue = mCommandThreadRunnables[index]
					.getCommandsQueue();
			queue.add(command);
		}

		// ----------------- Clean Thumbnail Queue ----------------
		// Clear All Pending Thumbnail Commands from Thumnbails Queue
		DestroyCommand command = new DestroyCommand(null,
				Command_Id.COMMAND_ID_NULL, CommandConstants.COMMAND_TAG_NULL,
				null, null, Command.PRIORITY_MAX, null);
		mThumbnailTasksQ.clear();
		// Add Destroy command to the Thumbnail Queue to make it exit.
		mThumbnailTasksQ.add(command);

		// ----------------- Clean Sequential Queue ----------------
		command = new DestroyCommand(null, Command_Id.COMMAND_ID_NULL,
				CommandConstants.COMMAND_TAG_NULL, null, null,
				Command.PRIORITY_MAX, null);
		// Clear All Pending Sequential Commands from Sequential Queue
		mSequentialTasksQ.clear();
		// Add Destroy command to the Sequential Queue to make it exit.
		mSequentialTasksQ.add(command);
	}

	public void removeCommand(Command command) {
		mThumbnailTasksQ.remove(command);

		mSequentialTasksQ.remove(command);

		mParallelTasksCommonQueue.remove(command);

		// Remove from vector also
		mParallelTasksVector.remove(command);
	}

	public void removeAllCommands() {

		// Clear low priority queue
		for (int i = 0; i < MAX_THREAD_NUM; i++) {
			LinkedBlockingQueue<Command> queue = mCommandThreadRunnables[i]
					.getCommandsQueue();
			queue.clear();
		}

		// Clear high priority queue
		mThumbnailTasksQ.clear();

		// Clear high priority queue
		mSequentialTasksQ.clear();
	}

	/*public void clearQueue() {
		removeAllCommands();
	}*/

	public Vector<Command> getCommandsVector() {
		return mParallelTasksVector;
	}
}