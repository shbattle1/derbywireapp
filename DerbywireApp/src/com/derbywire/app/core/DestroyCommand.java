package com.derbywire.app.core;

import android.content.Context;
import android.os.Bundle;

import com.derbywire.app.common.CommandConstants.Command_Id;

public class DestroyCommand extends Command {

	public DestroyCommand(Context context, Command_Id commandId, String commandName,
			ICommandListener listener, IDependencyTaskListener dependecyTaskListener, int priority, Bundle extraParams) {
		super(commandId, commandName, listener, dependecyTaskListener, priority, extraParams);
	}

	@Override
	public void clean() throws Exception {

	}

	@Override
	public void execute() throws Exception {

	}

	@Override
	public void init() throws Exception {

	}

	@Override
	public void errorOcurredWhileExecutingCommand() {
		// TODO Auto-generated method stub
		
	}

}
