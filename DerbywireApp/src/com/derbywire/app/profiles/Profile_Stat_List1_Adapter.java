package com.derbywire.app.profiles;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.derbywire.app.R;

public class Profile_Stat_List1_Adapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<StatisticsData> data;

	public Profile_Stat_List1_Adapter(Context context,
			ArrayList<StatisticsData> data) {
		super();
		this.context = context;
		this.data = data;

	}

	class ViewHolderItem {
		TextView tx1;
		TextView tx2;
		TextView tx3;
		TextView tx4;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolderItem viewHolder;
		// LayoutInflater inflater = context.getLayoutInflater();
		if (view != null) {
			viewHolder = (ViewHolderItem) view.getTag();
		} else {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.statistics_tab1_list_item, null,
					true);
			viewHolder = new ViewHolderItem();

			viewHolder.tx1 = (TextView) view.findViewById(R.id.t1_tv1);
			viewHolder.tx2 = (TextView) view.findViewById(R.id.t1_tv2);
			viewHolder.tx3 = (TextView) view.findViewById(R.id.t1_tv3);
			viewHolder.tx4 = (TextView) view.findViewById(R.id.t1_tv4);
			view.setTag(viewHolder);
		}
		viewHolder.tx1.setText(this.data.get(position).albTitle);
		viewHolder.tx2.setText(this.data.get(position).albTitle);
		viewHolder.tx3.setText(this.data.get(position).albDownPer);
		viewHolder.tx4.setText(this.data.get(position).albMaleDown);
		return view;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
