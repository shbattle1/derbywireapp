package com.derbywire.app.profiles;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.derbywire.app.R;

public class Profile_Stat_list1 extends Fragment {

	TextView tv;

	String[][] tab1_data = { { "Jay Z", "Mercy", "67%", "390" },
			{ "Jo", "Mercy", "59%", "390" },
			{ "KellyPrice", "Mercy", "44%", "390" },
			{ "Jay Z", "Mercy", "67%", "390" },
			{ "Jo", "Mercy", "59%", "390" },
			{ "KellyPrice", "Mercy", "44%", "390" },
			{ "Jay Z", "Mercy", "67%", "390" },
			{ "Jo", "Mercy", "59%", "390" },
			{ "KellyPrice", "Mercy", "44%", "390" },

	};

	public Profile_Stat_list1() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.statistics_tab1_list, container,
				false);

		/*
		 * Profile_Stat_List1_Adapter adapter = new
		 * Profile_Stat_List1_Adapter(ProfileFrames.getAppContext(),tab1_data);
		 * 
		 * ListView list = (ListView) view.findViewById(R.id.stat_tab1_list);
		 * list.setAdapter(adapter);
		 */

		return view;
	}
}