package com.derbywire.app.profiles;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.TextView;

import com.derbywire.app.R;

public class Creators extends Fragment {

	TextView tv;
	private ImageAdapterCust mAdapter;
	private int mImageThumbSize;
	private int mImageThumbSpacing;
	private ImageFetcher mImageFetcher;
	GridView mGridView;
	private List<String> imglist = new ArrayList<String>();

	Context mContext = ProfileFrames.getAppContext();

	public static Creators newInstance(int position) {

		Creators cr = new Creators();
		Bundle b = new Bundle();
		b.putInt("index", position);
		cr.setArguments(b);

		return cr;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.photos_layout, container, false);
		mGridView = (GridView) view.findViewById(R.id.gridView);
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		downloadDataFromServer();
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void setResourcesToGridView() {

		mImageThumbSize = getResources().getDimensionPixelSize(
				R.dimen.image_thumbnail_size);
		mImageThumbSpacing = getResources().getDimensionPixelSize(
				R.dimen.image_thumbnail_spacing);

		mImageFetcher = new ImageFetcher(getActivity().getApplicationContext(),
				mImageThumbSize);
		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mAdapter = new ImageAdapterCust(imglist, mImageFetcher);
		// gridview.setAdapter(mAdapter);

		mGridView.setAdapter(mAdapter);

		mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						if (mAdapter.getNumColumns() == 0) {
							final int numColumns = (int) Math.floor(mGridView
									.getWidth()
									/ (mImageThumbSize + mImageThumbSpacing));
							if (numColumns > 0) {
								final int columnWidth = (mGridView.getWidth() / numColumns)
										- mImageThumbSpacing;
								mAdapter.setNumColumns(numColumns);
								mAdapter.setItemHeight(columnWidth);
							}
						}
					}
				});
	}

	private void parseJSON(String data) {
		try {
			JSONObject reader = new JSONObject(data);
			JSONObject details = reader.getJSONObject("creatorsDetails");
			int totalrecords = details.getInt("totalRecords");
			JSONArray jsonarray = details.getJSONArray("creatorDetails");
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				String url = jsonobject.getString("profileImageUrl");
				imglist.add(url);
			}
			setResourcesToGridView();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void downloadDataFromServer() {
		// HttpResponse httpRes = null;
		String url = "http://50.56.188.53/api/services/creatorsDetailsMob/format/json";

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 0: {
					parseJSON((String) msg.obj);
				}
					break;
				}
			}
		};
		HttpRequestGetPost httpReq = new HttpRequestGetPost(mContext, url,
				"GET", null, handler);
		// httpReq.HttpRequestTask().execute();
	}

}