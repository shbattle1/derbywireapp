package com.derbywire.app.profiles;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.derbywire.app.R;

public class CircuitBoard extends Fragment {

	TextView tv;

	private ImageAdapterCust mAdapter;
	private int mImageThumbSize;
	private int mImageThumbSpacing;
	private ImageFetcher mImageFetcher;
	private ListView list;
	GridView mGridView;
	private List<String> imglist = new ArrayList<String>();
	private EditText startDate;
	private EditText endDate;

	Context mContext = ProfileFrames.getAppContext();

	public static CircuitBoard newInstance(int index) {

		CircuitBoard cb = new CircuitBoard();
		Bundle b = new Bundle();
		b.putInt("index", index);
		cb.setArguments(b);

		return cb;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.circuitboard_layout, container,
				false);

		mGridView = (GridView) view.findViewById(R.id.gridView);
		startDate = (EditText) view.findViewById(R.id.start_date);
		endDate = (EditText) view.findViewById(R.id.end_date);
		setDateChangeListeners();
		setResourcesToGridView();
		return view;

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		// downloadDataFromServer();
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void setDateChangeListeners() {
		startDate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		endDate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.toString().equals("04/01/2015")) {
					// setResourcesToGridView();
					downloadDataFromServer();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

	}

	public void setResourcesToGridView() {

		mImageThumbSize = getResources().getDimensionPixelSize(
				R.dimen.image_thumbnail_size);
		mImageThumbSpacing = getResources().getDimensionPixelSize(
				R.dimen.image_thumbnail_spacing);

		mImageFetcher = new ImageFetcher(getActivity().getApplicationContext(),
				mImageThumbSize);
		// mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mAdapter = new ImageAdapterCust(imglist, mImageFetcher);

		mGridView.setAdapter(mAdapter);

		mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						if (mAdapter.getNumColumns() == 0) {
							final int numColumns = (int) Math.floor(mGridView
									.getWidth()
									/ (mImageThumbSize + mImageThumbSpacing));
							if (numColumns > 0) {
								final int columnWidth = (mGridView.getWidth() / numColumns)
										- mImageThumbSpacing;
								mAdapter.setNumColumns(numColumns);
								mAdapter.setItemHeight(columnWidth);
							}
						}
					}
				});
	}

	private void parseJSON(String data) {
		try {

			JSONObject reader = new JSONObject(data);
			JSONObject details = reader.getJSONObject("CircuitBoardDetails");
			int totalrecords = details.getInt("total_items");
			JSONArray jsonarray = details.getJSONArray("circuitboard_lists");
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				String url = jsonobject.getString("Image_Url");
				imglist.add(url);
			}
			setResourcesToGridView();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void downloadDataFromServer() {
		// HttpResponse httpRes = null;
		String url = "http://50.56.188.53/api/services/getCiruitBoardDet/format/json";
		ArrayList<NameValuePair> nValuePairs = new ArrayList<NameValuePair>();
		nValuePairs.add(new BasicNameValuePair("fromDate", "06/01/2015"));
		nValuePairs.add(new BasicNameValuePair("toDate", "04/01/2015"));

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				switch (msg.what) {
				case 0: {
					parseJSON((String) msg.obj);
				}
					break;
				}
			}
		};
		HttpRequestGetPost httpReq = new HttpRequestGetPost(mContext, url,
				"POST", nValuePairs, handler);
		// httpReq.HttpRequestTask().execute();
	}

}