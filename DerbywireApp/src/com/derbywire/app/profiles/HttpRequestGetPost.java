package com.derbywire.app.profiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

public class HttpRequestGetPost {

	private DefaultHttpClient httpclient;

	private Context context;

	private static String userAgent = "Apache-HttpClient/android";
	private static HttpParams httpParams = null;
	private static ThreadSafeClientConnManager connMan = null;
	private static boolean enableConnPooling = true;

	private int maxConnections = 10;

	private ArrayList<NameValuePair> nameValuePairs;
	private String url = null;
	private String httpReqType;
	private HttpResponse response;

	public HttpRequestGetPost(Context cont, String reqUrl, String reqType,
			ArrayList<NameValuePair> nValuePairs, Handler handler) {
		context = cont;
		nameValuePairs = nValuePairs;
		url = reqUrl;
		httpReqType = reqType;

		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory
				.getSocketFactory(), 443));

		if (httpParams == null) {
			httpParams = new BasicHttpParams();
			HttpConnectionParams.setSocketBufferSize(httpParams, 8192);
			HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(httpParams, "UTF-8");
			HttpProtocolParams.setUseExpectContinue(httpParams, true);
			HttpProtocolParams.setUserAgent(httpParams, userAgent);
			HttpClientParams.setRedirecting(httpParams, false);

			int totalConn = maxConnections;
			int connPerRoute = maxConnections;

			httpParams.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS,
					totalConn);
			httpParams.setParameter(
					ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
					new ConnPerRouteBean(connPerRoute));
		}

		connMan = new ThreadSafeClientConnManager(httpParams, schemeRegistry);

		if (connMan != null)
			httpclient = new DefaultHttpClient(connMan, httpParams);
		else
			httpclient = new DefaultHttpClient(httpParams);

		new HttpRequestTask().execute(handler);
	}

	public class HttpRequestTask extends AsyncTask<Handler, Void, String> {
		Handler handler;

		@Override
		protected String doInBackground(Handler... params) {
			handler = params[0];
			// String url = params[0];
			// String reqType = params[1];

			// TODO Auto-generated method stub
			HttpResponse httpRes = executeHttpRequest();
			String str = consumeData(httpRes);
			return str;
		}

		@Override
		protected void onPostExecute(String res) {
			Message msg = Message.obtain();
			msg.what = 0;
			msg.obj = res;
			handler.sendMessage(msg);
			if (ProfileFrames.mProgressProfiles != null) {
				ProfileFrames.mProgressProfiles.setVisibility(View.INVISIBLE);
			}
		}

		@Override
		protected void onPreExecute() {
			if (ProfileFrames.mProgressProfiles != null) {
				ProfileFrames.mProgressProfiles.setVisibility(View.VISIBLE);
			}
		}
	}

	private HttpResponse executeHttpRequest() {
		HttpResponse httpRes = null;
		HttpGet httpget = null;
		HttpPost httppost = null;
		URI uri = null;

		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (httpReqType.intern() == "GET") {
			httpget = new HttpGet(uri);
			HttpHost host = new HttpHost(uri.getHost(), uri.getPort(),
					uri.getScheme());
			try {
				httpRes = httpclient.execute(host, httpget);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			httppost = new HttpPost(uri.getPath());
			if (nameValuePairs != null) {
				if (nameValuePairs.size() > 0)
					try {
						httppost.setEntity(new UrlEncodedFormEntity(
								nameValuePairs, "UTF-8"));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			HttpHost host = new HttpHost(uri.getHost(), uri.getPort(),
					uri.getScheme());
			try {
				httpRes = httpclient.execute(host, httppost);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return httpRes;
	}

	private String consumeData(HttpResponse httpRes) {

		String resJsonString = null;
		if (httpRes == null)
			return null;
		int statusCode = httpRes.getStatusLine().getStatusCode();
		HttpEntity entity = httpRes.getEntity();
		InputStream is = null;
		if (entity != null) {
			try {
				is = entity.getContent();
			} catch (IllegalStateException e) {
				// e.printStackTrace();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
		if (is == null) {
			return null;
		}
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));

			String line;
			StringBuffer strBuf = new StringBuffer();

			if ((line = reader.readLine()) != null) {
				strBuf.append(line);
			}
			while ((line = reader.readLine()) != null) {
				strBuf.append(line);
			}

			resJsonString = strBuf.toString();
			is.close();
		} catch (IOException ioex) {

		}
		return resJsonString;
	}

}