package com.derbywire.app.profiles;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.derbywire.app.R;

public class Statistics extends Fragment {

	TextView tv;
	LinearLayout llStatTab1, llStatTab2;
	ImageView ivRightArrow, ivLeftArrow;

	ViewPager myViewPager;
	ListView list;

	private ArrayList<StatisticsData> statList = new ArrayList<StatisticsData>();

	public static Statistics newInstance(int index) {
		Statistics f = new Statistics();
		Bundle b = new Bundle();
		b.putInt("index", index);
		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		downloadDataFromServer();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.statistics_main, container, false);

		llStatTab1 = (LinearLayout) view
				.findViewById(R.id.ll_profile_stat_titlebar1);
		llStatTab2 = (LinearLayout) view
				.findViewById(R.id.ll_profile_stat_titlebar2);

		list = (ListView) view.findViewById(R.id.stat_tab1_list);

		llStatTab1.setVisibility(View.VISIBLE);
		llStatTab2.setVisibility(View.GONE);

		ivRightArrow = (ImageView) view
				.findViewById(R.id.iv_profile_stat_title_rightarrow);
		ivLeftArrow = (ImageView) view
				.findViewById(R.id.iv_profile_stat_title_leftarrow);

		ivRightArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				llStatTab1.setVisibility(View.GONE);
				llStatTab2.setVisibility(View.VISIBLE);
				Profile_Stat_List2_Adapter adapter = new Profile_Stat_List2_Adapter(
						ProfileFrames.getAppContext(), statList);
				list.setAdapter(adapter);
			}
		});

		ivLeftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				llStatTab1.setVisibility(View.VISIBLE);
				llStatTab2.setVisibility(View.GONE);
				Profile_Stat_List1_Adapter adapter = new Profile_Stat_List1_Adapter(
						ProfileFrames.getAppContext(), statList);
				list.setAdapter(adapter);
			}
		});

		return view;
	}

	private void parseJSON(String data) {
		try {
			JSONObject reader = new JSONObject(data);
			// JSONObject details = reader.getJSONObject("statistics");
			// int totalrecords = details.getInt("totalRecords");
			JSONArray jsonarray = reader.getJSONArray("statistics");
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				String name = jsonobject.getString("name");
				String title = jsonobject.getString("title");
				String type = jsonobject.getString("type");
				String per = jsonobject.getString("per");
				String male = jsonobject.getString("male");
				String female = jsonobject.getString("females");
				StatisticsData item = new StatisticsData(name, title, type,
						per, male, female);
				statList.add(item);
			}
			Profile_Stat_List1_Adapter adapter = new Profile_Stat_List1_Adapter(
					ProfileFrames.getAppContext(), statList);
			list.setAdapter(adapter);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void downloadDataFromServer() {
		// HttpResponse httpRes = null;
		String url = "http://50.56.188.53/derbywire/api/services/getStatisticsMob/format/json";

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				switch (msg.what) {
				case 0: {
					parseJSON((String) msg.obj);
				}
					break;
				}
			}
		};
		HttpRequestGetPost httpReq = new HttpRequestGetPost(
				ProfileFrames.getAppContext(), url, "GET", null, handler);
		// httpReq.HttpRequestTask().execute();
	}

}