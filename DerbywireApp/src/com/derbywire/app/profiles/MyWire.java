package com.derbywire.app.profiles;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.derbywire.app.R;

public class MyWire extends Fragment {

	private TextView tv;
	private ListView mApplist;
	List<ItemData> mList = new ArrayList<ItemData>();

	public MyWire() {

	}

	public static MyWire newInstance(int index) {

		MyWire f = new MyWire();

		Bundle b = new Bundle();
		b.putInt("index", index);
		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mywire_layout, container, false);
		mApplist = (ListView) view.findViewById(R.id.applist);
		for (int i = 0; i < 5; i++) {
			ItemData data = new ItemData();
			mList.add(data);
		}
		mApplist.setAdapter(new CustomArrayAdapter(getActivity()
				.getApplicationContext(), mList));
		return view;
	}

	private class CustomArrayAdapter extends ArrayAdapter<ItemData> {

		LayoutInflater inflater;
		private List<ItemData> appList;

		public CustomArrayAdapter(Context context, List<ItemData> items) {
			super(context, R.layout.mywire_list_item, items);
			this.inflater = LayoutInflater.from(context);
			appList = items;
		}

		@Override
		public int getCount() {
			return appList.size();
		}

		@Override
		public ItemData getItem(int position) {
			return appList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return appList.get(position).hashCode();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ItemData appItem = this.getItem(position);

			CheckBox checkBox;
			TextView textView;
			ImageView appicon;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.mywire_list_item, null);
				/*
				 * textView = (TextView)
				 * convertView.findViewById(R.id.textView1); checkBox =
				 * (CheckBox) convertView.findViewById(R.id.checkBoxAppItem);
				 * appicon = (ImageView)
				 * convertView.findViewById(R.id.imageView1);
				 * convertView.setTag(new AppItemViewHolder(textView,
				 * checkBox,appicon));
				 */

				/*
				 * checkBox.setOnCheckedChangeListener(new
				 * OnCheckedChangeListener() {
				 *
				 * public void onCheckedChanged(CompoundButton checkbox, boolean
				 * isChecked) { CheckBox cb = (CheckBox) checkbox; ItemData
				 * aitem = (ItemData) cb.getTag();
				 * aitem.setChecked(cb.isChecked()); SelectedApp app = new
				 * SelectedApp(aitem.mAppName); if(isChecked){
				 * if(!mSelectedAppList.contains(app)){
				 * mSelectedAppList.add(app); }
				 *
				 * } else{ if(mSelectedAppList.contains(app)){
				 * mSelectedAppList.remove(app); } } } });
				 */
			} else {
				/*
				 * AppItemViewHolder viewHolder = (AppItemViewHolder)
				 * convertView.getTag(); checkBox = viewHolder.getCheckBox();
				 * textView = viewHolder.getTextView(); appicon =
				 * viewHolder.getImageViewUid();
				 */
			}
			/*
			 * checkBox.setTag(appItem); checkBox.setChecked(appItem.isCheked);
			 * textView.setText(appItem.mAppName);
			 * appicon.setImageDrawable(appItem.mAppIcon);
			 */
			return convertView;
		}

	}

	private static class AppItemViewHolder {
		private CheckBox checkBox;
		private TextView textView;
		private ImageView appicon;

		public AppItemViewHolder(TextView textView, CheckBox checkBox,
				ImageView icon) {
			this.checkBox = checkBox;
			this.textView = textView;
			this.appicon = icon;
		}

		public CheckBox getCheckBox() {
			return checkBox;
		}

		public TextView getTextView() {
			return textView;
		}

		public ImageView getImageViewUid() {
			return appicon;
		}
	}

	private class ItemData {
		/*
		 * public ItemData(String appName, Drawable appIcon) { mAppName =
		 * appName; mAppIcon = appIcon; }
		 */

		Drawable mAppIcon;
		String mAppName;
		boolean isCheked;

		public void setChecked(boolean val) {
			isCheked = val;
		}
	}
}