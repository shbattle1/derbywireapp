package com.derbywire.app.profiles;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.derbywire.app.R;

public class Profile_Stat_List2 extends Fragment {

	TextView tv;

	String[][] tab2_data = { { "25", "5000", "100000" },
			{ "25", "5000", "100000" }, { "25", "5000", "100000" },
			{ "25", "5000", "100000" },

	};

	public Profile_Stat_List2() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.statistics_tab2_list, container,
				false);

		/*
		 * Profile_Stat_List2_Adapter adapter = new
		 * Profile_Stat_List2_Adapter(ProfileFrames.getAppContext(),statList);
		 *
		 * ListView list = (ListView) view.findViewById(R.id.stat_tab2_list);
		 * list.setAdapter(adapter);
		 */

		return view;
	}
}