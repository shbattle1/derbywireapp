package com.derbywire.app.profiles;

import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class ImageDownloaderCust extends AsyncTask<String, Integer, Bitmap> {
	private ImageView imgview;

	public ImageDownloaderCust(ImageView view) {
		imgview = view;

	}

	@Override
	protected Bitmap doInBackground(String... param) {
		// TODO Auto-generated method stub
		return downloadBitmap(param[0]);
	}

	@Override
	protected void onPreExecute() {

		if (ProfileFrames.mProgressProfiles != null) {
			ProfileFrames.mProgressProfiles.setVisibility(View.VISIBLE);
		}

	}

	@Override
	protected void onPostExecute(Bitmap result) {
		Log.i("Async-Example", "onPostExecute Called");
		imgview.setImageBitmap(result);
		if (ProfileFrames.mProgressProfiles != null) {
			ProfileFrames.mProgressProfiles.setVisibility(View.INVISIBLE);
		}
	}

	private Bitmap downloadBitmap(String url) {
		// initilize the default HTTP client object
		final DefaultHttpClient client = new DefaultHttpClient();

		// forming a HttoGet request
		final HttpGet getRequest = new HttpGet(url);
		try {

			HttpResponse response = client.execute(getRequest);

			// check 200 OK for success
			final int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode != HttpStatus.SC_OK) {
				Log.w("ImageDownloaderCust", "Error " + statusCode
						+ " while retrieving bitmap from " + url);
				return null;

			}

			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream inputStream = null;
				try {
					// getting contents from the stream
					inputStream = entity.getContent();

					// decoding stream data back into image Bitmap that android
					// understands
					final Bitmap bitmap = BitmapFactory
							.decodeStream(inputStream);

					return bitmap;
				} finally {
					if (inputStream != null) {
						inputStream.close();
					}
					entity.consumeContent();
				}
			}
		} catch (Exception e) {
			// You Could provide a more explicit error message for IOException
			getRequest.abort();
			Log.e("ImageDownloaderCust", "Something went wrong while"
					+ " retrieving bitmap from " + url + e.toString());
		}

		return null;
	}
}
