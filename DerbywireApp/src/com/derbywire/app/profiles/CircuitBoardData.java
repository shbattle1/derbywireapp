package com.derbywire.app.profiles;

public class CircuitBoardData {

	String albName;
	String albTitle;
	String albType;
	String albDownPer;
	String albMaleDown;
	String albFemaleDown;

	public CircuitBoardData(String name, String title, String type, String per,
			String maleDown, String femaleDown) {
		albName = name;
		albTitle = title;
		albType = type;
		albDownPer = per;
		albMaleDown = maleDown;
		albFemaleDown = femaleDown;
	}

}
