package com.derbywire.app.profiles;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapterCust extends BaseAdapter {
	// private final Context mContext;
	private int mItemHeight = 0;
	private int mNumColumns = 0;
	private int mActionBarHeight = 0;
	private GridView.LayoutParams mImageViewLayoutParams;
	private List<String> imglist;
	private ImageFetcher mImageFetcher;

	public ImageAdapterCust(List<String> images, ImageFetcher imgfetcher) {
		super();

		imglist = images;
		mImageFetcher = imgfetcher;
		mImageViewLayoutParams = new GridView.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	@Override
	public int getCount() {
		// Size + number of columns for top empty row
		return imglist.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		// Two types of views, the normal ImageView and the top row of empty
		// views
		return 1;
	}

	/*
	 * @Override public int getItemViewType(int position) { return (position <
	 * mNumColumns) ? 1 : 0; }
	 */
	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup container) {

		ImageView imageView;
		if (convertView == null) { // if it's not recycled, instantiate and
									// initialize
			imageView = new RecyclingImageView(ProfileFrames.getAppContext());
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setLayoutParams(mImageViewLayoutParams);
		} else { // Otherwise re-use the converted view
			imageView = (ImageView) convertView;
		}

		// Check the height matches our calculated column width
		if (imageView.getLayoutParams().height != mItemHeight) {
			imageView.setLayoutParams(mImageViewLayoutParams);
		}
		// mImageFetcher.loadImage(imglist.get(position), imageView);
		new ImageDownloaderCust(imageView).execute(imglist.get(position));
		return imageView;
	}

	/**
	 * Sets the item height. Useful for when we know the column width so the
	 * height can be set to match.
	 *
	 * @param height
	 */
	public void setItemHeight(int height) {
		if (height == mItemHeight) {
			return;
		}
		mItemHeight = height;
		mImageViewLayoutParams = new GridView.LayoutParams(
				LayoutParams.MATCH_PARENT, mItemHeight);
		mImageFetcher.setImageSize(height);
		notifyDataSetChanged();
	}

	public void setNumColumns(int numColumns) {
		mNumColumns = numColumns;
	}

	public int getNumColumns() {
		return mNumColumns;
	}

}
