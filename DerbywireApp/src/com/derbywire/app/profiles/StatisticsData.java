package com.derbywire.app.profiles;

public class StatisticsData {
	String albName;
	String albTitle;
	String albType;
	String albDownPer;
	String albMaleDown;
	String albFemaleDown;

	public StatisticsData(String name, String title, String type, String per,
			String maleDown, String femaleDown) {
		albName = name;
		albTitle = title;
		albType = type;
		albDownPer = per;
		albMaleDown = maleDown;
		albFemaleDown = femaleDown;
	}

}
