package com.derbywire.app.profiles;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.derbywire.app.R;

public class Profile_Stat_List2_Adapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<StatisticsData> data;

	public Profile_Stat_List2_Adapter(Context context,
			ArrayList<StatisticsData> data) {
		super();
		this.context = context;
		this.data = data;

	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		// LayoutInflater inflater = context.getLayoutInflater();
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.statistics_tab2_list_item,
				null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.t2_tv1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.t2_tv2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.t2_tv3);

		txt1.setText(this.data.get(position).albDownPer);
		txt2.setText(this.data.get(position).albFemaleDown);
		txt3.setText(this.data.get(position).albDownPer);

		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
