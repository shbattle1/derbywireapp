package com.derbywire.app.profiles;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.derbywire.app.R;

public class ProfileFrames extends FragmentActivity {

	int numberOfPages = 5;
	ViewPager myViewPager;
	String text = "test";
	LinearLayout llAppHeaderCommon, llAppHeaderStatistics;
	LinearLayout llProfilesTrending, llProfilesMugShot;
	LinearLayout mTrendinglayout;
	LinearLayout mTablayout;
	LinearLayout mTabBar;
	LinearLayout mAppFooter;
	LinearLayout llFragLayout;

	int iprevSelectedIndex;

	private static Context app_context = null;

	public static ProgressBar mProgressProfiles = null;

	private Fragment fragCB = null;
	private Fragment fragCreators = null;

	ImageView ivMyWire, ivCBoard, ivCreators, ivStatistics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.profile_frames_activity_main);
		app_context = this.getApplicationContext();

		ivMyWire = (ImageView) findViewById(R.id.iv_tab_mywire);
		ivCBoard = (ImageView) findViewById(R.id.iv_tab_circuitboard);
		ivCreators = (ImageView) findViewById(R.id.iv_tab_creators);
		ivStatistics = (ImageView) findViewById(R.id.iv_tab_statistics);

		llAppHeaderCommon = (LinearLayout) findViewById(R.id.ll_appheader_common);
		llAppHeaderStatistics = (LinearLayout) findViewById(R.id.ll_appheader_statistics);

		llProfilesTrending = (LinearLayout) findViewById(R.id.ll_profiles_user_trending);
		llProfilesMugShot = (LinearLayout) findViewById(R.id.ll_profiles_user_mugshot);
		mTrendinglayout = (LinearLayout) findViewById(R.id.trendinglayout);
		mTablayout = (LinearLayout) findViewById(R.id.tab_layput);
		mTabBar = (LinearLayout) findViewById(R.id.tab_bar);
		mAppFooter = (LinearLayout) findViewById(R.id.app_footer);

		llFragLayout = (LinearLayout) findViewById(R.id.fragContainer);

		mProgressProfiles = (ProgressBar) findViewById(R.id.refresh_progressbar);
		setTabClickEvents();
		Handler mHandler = new Handler();

		llAppHeaderStatistics.setVisibility(View.VISIBLE);
		llAppHeaderCommon.setVisibility(View.GONE);

		// llProfilesTrending.setVisibility(View.GONE);
		// llProfilesMugShot.setVisibility(View.GONE);

		Log.d("Rk", "Trending Weight: " + mTrendinglayout.getWeightSum());
		Log.d("Rk", "tab Weight: " + mTablayout.getWeightSum());

		loadSelectedFragment(0);
	}

	private void loadSelectedFragment(int position) {
		switch (position) {
		case 0: {
			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = fm.findFragmentByTag("trendings");
			if (fragment == null) {
				// Make new fragment to show this selection.
				fragment = Trendings.newInstance(position);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getSupportFragmentManager()
						.beginTransaction();
				ft.replace(R.id.fragContainer, fragment, "trendings");
				ft.commit();
			}
			break;
		}
		case 1: {
			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = fm.findFragmentByTag("myWire");
			if (fragment == null) {
				// Make new fragment to show this selection.
				fragment = MyWire.newInstance(position);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getSupportFragmentManager()
						.beginTransaction();
				ft.replace(R.id.fragContainer, fragment, "myWire");
				ft.commit();
			}
			break;
		}
		case 2: {
			FragmentManager fm = getSupportFragmentManager();
			/*
			 * if(if(fm.findFragmentByTag("Creators") ||
			 * fm.findFragmentByTag("trendings") ||
			 * fm.findFragmentByTag("myWire") ||
			 * fm.findFragmentByTag("CircuitBoard")){
			 * 
			 * }
			 */
			// / fragCB = fm.findFragmentByTag("CircuitBoard");
			if (fragCB == null) {
				// Make new fragment to show this selection.
				fragCB = CircuitBoard.newInstance(position);
			}
			// Execute a transaction, replacing any existing fragment
			// with this one inside the frame.
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();

			ft.replace(R.id.fragContainer, fragCB, "CircuitBoard");
			ft.commit();

			break;
		}
		case 3: {
			FragmentManager fm = getSupportFragmentManager();
			// / fragCreators = fm.findFragmentByTag("Creators");
			if (fragCreators == null) {
				// Make new fragment to show this selection.
				fragCreators = Creators.newInstance(position);
			}
			// Execute a transaction, replacing any existing fragment
			// with this one inside the frame.
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			ft.replace(R.id.fragContainer, fragCreators, "Creators");
			ft.commit();

			break;
		}
		case 4: {
			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = fm.findFragmentByTag("Statistics");
			if (fragment == null) {
				// Make new fragment to show this selection.
				fragment = Statistics.newInstance(position);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getSupportFragmentManager()
						.beginTransaction();
				ft.replace(R.id.fragContainer, fragment, "Statistics");
				ft.commit();
			}
			break;
		}
		}

	}

	private void setTabClickEvents() {

		ivMyWire.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * if(myViewPager.getCurrentItem() == 0) return;
				 */
				ivMyWire.setImageResource(R.drawable.tab1_select);
				ivCBoard.setImageResource(R.drawable.tab2_deselect);
				ivCreators.setImageResource(R.drawable.tab3_deselect);
				ivStatistics.setImageResource(R.drawable.tab4_deselect);

				llAppHeaderStatistics.setVisibility(View.GONE);
				llAppHeaderCommon.setVisibility(View.VISIBLE);

				llProfilesTrending.setVisibility(View.GONE);
				llProfilesMugShot.setVisibility(View.GONE);

				mTrendinglayout.setVisibility(View.GONE);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTablayout
						.getLayoutParams();
				params.weight = 1.0f;
				mTablayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTabBar.getLayoutParams();
				params.weight = 0.069f;
				mTabBar.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mAppFooter
						.getLayoutParams();
				params.weight = 0.075f;
				mAppFooter.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) llFragLayout
						.getLayoutParams();
				params.weight = 0.85f;
				llFragLayout.setLayoutParams(params);
				loadSelectedFragment(1);
				iprevSelectedIndex = 1;
			}
		});

		ivCBoard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (iprevSelectedIndex == 2)
					return;
				ivCBoard.setImageResource(R.drawable.tab2_select);

				ivMyWire.setImageResource(R.drawable.tab1_deselect);
				ivCreators.setImageResource(R.drawable.tab3_deselect);
				ivStatistics.setImageResource(R.drawable.tab4_deselect);

				llAppHeaderStatistics.setVisibility(View.VISIBLE);
				llAppHeaderCommon.setVisibility(View.GONE);

				llProfilesTrending.setVisibility(View.VISIBLE);
				llProfilesMugShot.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);

				mAppFooter.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTablayout
						.getLayoutParams();
				params.weight = 0.65f;
				mTablayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTrendinglayout
						.getLayoutParams();
				params.weight = 0.33f;
				mTrendinglayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTabBar.getLayoutParams();
				params.weight = 0.1f;
				mTabBar.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mAppFooter
						.getLayoutParams();
				params.weight = 0.1f;
				mAppFooter.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) llFragLayout
						.getLayoutParams();
				params.weight = 0.8f;
				llFragLayout.setLayoutParams(params);

				Log.d("Rk",
						"Trending Weight: " + mTrendinglayout.getWeightSum());
				Log.d("Rk", "tab Weight: " + mTablayout.getWeightSum());
				iprevSelectedIndex = 2;
				loadSelectedFragment(2);

			}
		});

		ivCreators.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (iprevSelectedIndex == 3)
					return;
				ivCreators.setImageResource(R.drawable.tab3_select);

				ivMyWire.setImageResource(R.drawable.tab1_deselect);
				ivCBoard.setImageResource(R.drawable.tab2_deselect);
				ivStatistics.setImageResource(R.drawable.tab4_deselect);

				llAppHeaderStatistics.setVisibility(View.VISIBLE);
				llAppHeaderCommon.setVisibility(View.GONE);

				llProfilesTrending.setVisibility(View.VISIBLE);
				llProfilesMugShot.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);
				mAppFooter.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTablayout
						.getLayoutParams();
				params.weight = 0.65f;
				mTablayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTrendinglayout
						.getLayoutParams();
				params.weight = 0.33f;
				mTrendinglayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTabBar.getLayoutParams();
				params.weight = 0.1f;
				mTabBar.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mAppFooter
						.getLayoutParams();
				params.weight = 0.1f;
				mAppFooter.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) llFragLayout
						.getLayoutParams();
				params.weight = 0.8f;
				llFragLayout.setLayoutParams(params);
				iprevSelectedIndex = 3;
				loadSelectedFragment(3);
			}
		});

		ivStatistics.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (iprevSelectedIndex == 4)
					return;
				ivStatistics.setImageResource(R.drawable.tab4_select);
				ivMyWire.setImageResource(R.drawable.tab1_deselect);
				ivCBoard.setImageResource(R.drawable.tab2_deselect);
				ivCreators.setImageResource(R.drawable.tab3_deselect);

				llAppHeaderCommon.setVisibility(View.VISIBLE);
				llAppHeaderStatistics.setVisibility(View.GONE);

				llProfilesTrending.setVisibility(View.VISIBLE);
				llProfilesMugShot.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);
				mAppFooter.setVisibility(View.VISIBLE);
				mTrendinglayout.setVisibility(View.VISIBLE);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTablayout
						.getLayoutParams();
				params.weight = 0.65f;
				mTablayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTrendinglayout
						.getLayoutParams();
				params.weight = 0.33f;
				mTrendinglayout.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mTabBar.getLayoutParams();
				params.weight = 0.1f;
				mTabBar.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) mAppFooter
						.getLayoutParams();
				params.weight = 0.1f;
				mAppFooter.setLayoutParams(params);

				params = (LinearLayout.LayoutParams) llFragLayout
						.getLayoutParams();
				params.weight = 0.8f;
				llFragLayout.setLayoutParams(params);
				iprevSelectedIndex = 4;
				loadSelectedFragment(4);
			}
		});

	}

	public static Context getAppContext() {
		return app_context;// .getApplicationContext();
	}
}
