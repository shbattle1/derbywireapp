package com.derbywire.app.menu;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<menuData> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<ActivityType, List<menuData>> _listDataChild;

	public ExpandableListAdapter(Context context,
			List<menuData> listDataHeader,
			HashMap<ActivityType, List<menuData>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		if (this._listDataChild != null) {
			List<menuData> tempList = this._listDataChild
					.get(this._listDataHeader.get(groupPosition)
							.getActivityType());
			if (tempList != null) {
				return tempList.get(childPosititon);
			} else
				return null;
		} else {
			return null;
		}
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		menuData subMenuItem = ((menuData) getChild(groupPosition,
				childPosition));
		final String childText = subMenuItem.getMenuText();
		final int childIconId = subMenuItem.getMenuIcon();

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_item, null);
		}

		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.itemText);
		txtListChild.setTypeface(null, Typeface.BOLD);
		txtListChild.setText(childText);

		ImageView childIcon = (ImageView) convertView
				.findViewById(R.id.itemIcon);
		childIcon.setBackgroundResource(childIconId);

		convertView.setTag(subMenuItem);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (this._listDataChild != null
				&& this._listDataChild.isEmpty() == false) {
			List<menuData> tempList = this._listDataChild
					.get(this._listDataHeader.get(groupPosition)
							.getActivityType());
			if (tempList != null) {
				return tempList.size();
			} else
				return 0;
		} else
			return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		menuData menuGroup = (menuData) getGroup(groupPosition);
		// String headerTitle = (String) menuGroup.getMenuText();
		// int iconId = (int) menuGroup.getMenuIcon();

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_group, null);
		}

		/*
		 * TextView lblListHeader = (TextView) convertView
		 * .findViewById(R.id.groupText); lblListHeader.setTypeface(null,
		 * Typeface.BOLD); lblListHeader.setText(headerTitle);
		 */

		ImageView groupIcon = (ImageView) convertView
				.findViewById(R.id.groupIcon);
		// groupIcon.setBackgroundResource(iconId);

		if (isExpanded) {
			// convertView.setBackgroundResource(R.drawable.round_back_fill);
			groupIcon.setBackgroundResource(menuGroup.getMenuIconEnb());

		} else {
			// convertView.setBackgroundResource(R.drawable.round_back_grey_fill);
			groupIcon.setBackgroundResource(menuGroup.getMenuIconDis());
		}

		convertView.setTag(menuGroup);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
