package com.derbywire.app.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageButton;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.common.LogFile;

public class MenuActivity extends Activity {

	final String TAG = MenuActivity.class.getSimpleName();
	Context mContext = null;
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;

	List<menuData> listDataHeader;
	HashMap<ActivityType, List<menuData>> listDataChild;

	ImageButton mMsgBtn = null;
	ImageButton mFavBtn = null;
	ImageButton mSubscriberBtn = null;
	ImageButton mAirPlayBtn = null;
	ImageButton mSettingsBtn = null;
	ImageButton mProfileBtn = null;
	ImageButton mMerchantbtn = null;

	ActivityType menuActivityType[] = { ActivityType.NONE,
			ActivityType.UPLOADS, ActivityType.MYWIRE,
			ActivityType.CIRCUIT_BOARDS, ActivityType.CREATERS,
			ActivityType.STATISTICS, ActivityType.MYWALLET };

	int menuIconDis[] = { R.drawable.menu_streaming_dis,
			R.drawable.menu_upload_dis, R.drawable.menu_mywire_dis,
			R.drawable.menu_circuitboard_dis, R.drawable.menu_creators_dis,
			R.drawable.menu_stats_dis, R.drawable.menu_mywallet_dis };

	int menuIconEnb[] = { R.drawable.menu_streaming_enb,
			R.drawable.menu_upload_enb, R.drawable.menu_mywire_enb,
			R.drawable.menu_circuitboard_enb, R.drawable.menu_creators_enb,
			R.drawable.menu_stats_enb, R.drawable.menu_mywallet_enb };

	ActivityType subMenuActivityType[] = { ActivityType.MUSIC_PLAYLIST,
			ActivityType.VIDEO_PLAYLIST, ActivityType.PHOTO_PLAYLIST,
			ActivityType.MARKETPLACE };

	int subMenuText[] = { R.string.music, R.string.video, R.string.photo,
			R.string.marketplace };

	int subMenuIcon[] = { R.drawable.menu_audio, R.drawable.menu_video,
			R.drawable.menu_camera, R.drawable.menu_marketplace };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
		mContext = this;

		intResources();
		setClickListerners();

		// preparing list data
		prepareListData();

		listAdapter = new ExpandableListAdapter(this, listDataHeader,
				listDataChild);

		// setting list adapter
		expListView.setAdapter(listAdapter);
		expListView.setGroupIndicator(null);
		expListView.expandGroup(0);
	}

	private void intResources() {
		// get the listview
		expListView = (ExpandableListView) findViewById(R.id.menuOptions);
		mMsgBtn = (ImageButton) findViewById(R.id.msgBtn);
		mFavBtn = (ImageButton) findViewById(R.id.favBtn);
		mAirPlayBtn = (ImageButton) findViewById(R.id.airplay);
		mSettingsBtn = (ImageButton) findViewById(R.id.settingsBtn);
		mSubscriberBtn = (ImageButton) findViewById(R.id.subscriberBtn);
		mProfileBtn = (ImageButton) findViewById(R.id.profileBtn);
		mMerchantbtn = (ImageButton) findViewById(R.id.merchantbtn);
	}

	private void setClickListerners() {
		expListView.setOnGroupExpandListener(groupExpandListener);
		expListView.setOnChildClickListener(childClickListener);
		// expListView.setOnGroupClickListener(groupClickListener);
		mMsgBtn.setOnClickListener(onClickListener);
		mFavBtn.setOnClickListener(onClickListener);
		mAirPlayBtn.setOnClickListener(onClickListener);
		mSettingsBtn.setOnClickListener(onClickListener);
		mSubscriberBtn.setOnClickListener(onClickListener);
		mProfileBtn.setOnClickListener(onClickListener);
		mMerchantbtn.setOnClickListener(onClickListener);
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.msgBtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.MESSAGING);
				break;
			case R.id.favBtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.FAVORITE);
				break;
			case R.id.subscriberBtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.SUBSCRIBER);
				break;
			case R.id.settingsBtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.SETTINGS);
				break;
			case R.id.airplay:
				DerbyWireUtils.startActivity(mContext, ActivityType.AIRPLAY);
				break;
			case R.id.profileBtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.PROFILE);
				break;
			case R.id.merchantbtn:
				DerbyWireUtils.startActivity(mContext, ActivityType.MERCHANT);
				break;
			default:
				break;
			}
		}
	};
	OnChildClickListener childClickListener = new OnChildClickListener() {

		@Override
		public boolean onChildClick(ExpandableListView parent, View view,
				int groupPosition, int childPosition, long id) {
			setSelectedMenu(view);
			return false;
		}
	};

	// Listview Group expanded listener
	OnGroupExpandListener groupExpandListener = new OnGroupExpandListener() {
		int previousGroup = -1;

		@Override
		public void onGroupExpand(int groupPosition) {
			if (groupPosition != previousGroup)
				expListView.collapseGroup(previousGroup);
			previousGroup = groupPosition;
			menuData selctedMenu = listDataHeader.get(groupPosition);
			if (selctedMenu.getActivityType() != ActivityType.NONE) {
				DerbyWireUtils.startActivity(mContext,
						selctedMenu.getActivityType());
			}
		}
	};

	// ListView Group clicked listener
	OnGroupClickListener groupClickListener = new OnGroupClickListener() {

		@Override
		public boolean onGroupClick(ExpandableListView parent, View view,
				int groupPosition, long id) {
			expListView.expandGroup(groupPosition);
			setSelectedMenu(view);
			return false;
		}
	};

	private void setSelectedMenu(View view) {
		if (view != null && view.getTag() != null) {
			menuData selectedMenu = (menuData) view.getTag();
			ActivityType selectedActivityType = selectedMenu.getActivityType();
			if (selectedActivityType != ActivityType.NONE) {
				LogFile.ShowLog(TAG, "Selected Menu: ActivityType: "
						+ selectedActivityType);
				DerbyWireUtils.startActivity(mContext, selectedActivityType);
			}
		}
	}

	/*
	 * Preparing the list data
	 */
	private void prepareListData() {
		listDataHeader = new ArrayList<menuData>();
		listDataChild = new HashMap<ActivityType, List<menuData>>();

		// Adding parent data
		for (int i = 0; i < menuIconEnb.length; i++) {
			menuData menuItem = new menuData();

			menuItem.setMenuIconEnb(menuIconEnb[i]);
			menuItem.setMenuIconDis(menuIconDis[i]);
			menuItem.setActivityType(menuActivityType[i]);

			listDataHeader.add(menuItem);
		}

		// Adding child data
		List<menuData> subMenuItem = new ArrayList<menuData>();
		for (int i = 0; i < subMenuText.length; i++) {
			menuData subMenuData = new menuData();

			subMenuData.setMenuText(getResources().getString(subMenuText[i]));
			subMenuData.setMenuIcon(subMenuIcon[i]);
			subMenuData.setActivityType(subMenuActivityType[i]);

			subMenuItem.add(subMenuData);
		}
		// Header, Child data
		listDataChild.put(AppConstants.ActivityType.NONE, subMenuItem);
	}
}
