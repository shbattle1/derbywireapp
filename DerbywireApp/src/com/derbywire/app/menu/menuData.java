package com.derbywire.app.menu;

import com.derbywire.app.common.AppConstants.ActivityType;

public class menuData {

	String menuText;
	int menuIcon;
	int menuIconDis;
	int menuIconEnb;
	ActivityType activityType;

	public ActivityType getActivityType() {
		return activityType;
	}
	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}
	public String getMenuText() {
		return menuText;
	}
	public void setMenuText(String menuText) {
		this.menuText = menuText;
	}
	public int getMenuIcon() {
		return menuIcon;
	}
	public void setMenuIcon(int menuIcon) {
		this.menuIcon = menuIcon;
	}
	public int getMenuIconDis() {
		return menuIconDis;
	}
	public void setMenuIconDis(int menuIconDis) {
		this.menuIconDis = menuIconDis;
	}
	public int getMenuIconEnb() {
		return menuIconEnb;
	}
	public void setMenuIconEnb(int menuIconEnb) {
		this.menuIconEnb = menuIconEnb;
	}
}
