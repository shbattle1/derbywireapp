package com.derbywire.app.signin;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.DerbyWireUtils;
import com.derbywire.app.managers.ApiManager;

public class SignInActivity extends Activity {

	Button signInBtn = null;
	Button facebookSignInBtn = null;
	ApiManager apimanager;
	public String requestBody = "";
	public JSONObject jsonObjMetaData;
	Context mContext = null;
	public static String username ,password;

	UserInfo mSignInData = null;

	EditText mEmailText = null;
	EditText mPasswordText = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		mContext = this;
		mSignInData = new UserInfo();

		intResources();
		setClickListerners();
	}

	private void intResources() {
		signInBtn = (Button) findViewById(R.id.signinBtn);
		facebookSignInBtn = (Button) findViewById(R.id.facebookBtn);
		mEmailText = (EditText) findViewById(R.id.emailText);
		mPasswordText = (EditText) findViewById(R.id.passwordText);
	}

	private void setClickListerners() {
		signInBtn.setOnClickListener(BtnListener);
		facebookSignInBtn.setOnClickListener(BtnListener);
	}

	OnClickListener BtnListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.signinBtn:

				SignIn();
				break;

			case R.id.facebookBtn:
				loginWithFacebook();
				break;

			default:
				break;
			}
		}
	};

	private void loginWithFacebook() {

	}

	private void SignIn() {
		try {

			if (mSignInData != null) {
				mSignInData.setEmail(mEmailText.getText().toString());
				mSignInData.setPassword(mPasswordText.getText().toString());
				mSignInData.setRequestFrom("mobile");
				
				username = mEmailText.getText().toString();
				password = mPasswordText.getText().toString();
				

				// signInHandler.doSignIn(mSignInData, signInListener);
			}

			apimanager = new ApiManager(getApplicationContext(),
					new ApiManager.TimeOutListener() {
						@Override
						public void onTimeout() {

							// apimanager.doLogin(mSignInData,
							// signinResponseListener);

						}
					});
			apimanager.doLogin(username,password, signinResponseListener);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Response.Listener<JSONObject> signinResponseListener = new Response.Listener<JSONObject>() {
		@Override
		public void onResponse(JSONObject response) {
			try {
				AppSession.getInstance().DEBUG(getApplicationContext(),
						"SIGN IN RESPONSE = " + response.toString());

				JSONObject jsonObject = new JSONObject();

				jsonObject = new JSONObject(response.toString());
				if (jsonObject.getString(ApiManager.RESPONSE.ERROR) != "true") {
					
					SharedPreferences sp=getSharedPreferences("Login", 0);
					SharedPreferences.Editor Ed=sp.edit();
					Ed.putString(ApiManager.FIELDS.EMAIL,mEmailText.getText().toString() );              
					Ed.putString(ApiManager.FIELDS.PASSWORD,mPasswordText.getText().toString());   
					Ed.commit();

					finish();
				DerbyWireUtils.startActivity(mContext, ActivityType.BROWSE);

				} else {
					AppSession.getInstance().DisplayToast(
							getApplicationContext(), response.toString());

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	

}
