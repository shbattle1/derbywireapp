package com.derbywire.app.signin;

import java.io.Serializable;

public class UserInfo implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -4385158449081048008L;

	int mUserId;
	String mEmail;
	String mPassword;
	String mRequestFrom = "mobile";
	String mIMEINo;
	String mFirstName;
	String mLastName;
	String mProfileImg;
	String mUserType = "Fan";
	String mConfirmpassword;
	int mMonth;
	int mDate;
	int mYear;
	String mGender;
	String mCodewire = "codewire";

	public String getEmail() {
		return mEmail;
	}

	public void setEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String mPassword) {
		this.mPassword = mPassword;
	}

	public String getRequestFrom() {
		return mRequestFrom;
	}

	public void setRequestFrom(String mRequestFrom) {
		this.mRequestFrom = mRequestFrom;
	}

	public int getUserId() {
		return mUserId;
	}

	public void setUserId(int mUserId) {
		this.mUserId = mUserId;
	}

	public String getIMEINo() {
		return mIMEINo;
	}

	public void setIMEINo(String mIMEINo) {
		this.mIMEINo = mIMEINo;
	}

	public String getFirstName() {
		return mFirstName;
	}

	public void setFirstName(String mFirstName) {
		this.mFirstName = mFirstName;
	}

	public String getLastName() {
		return mLastName;
	}

	public void setLastName(String mLastName) {
		this.mLastName = mLastName;
	}

	public String getProfileImg() {
		return mProfileImg;
	}

	public void setProfileImg(String mProfileImg) {
		this.mProfileImg = mProfileImg;
	}

	public String getUserType() {
		return mUserType;
	}

	public void setUserType(String mUserType) {
		this.mUserType = mUserType;
	}

	public String getConfirmpassword() {
		return mConfirmpassword;
	}

	public void setConfirmpassword(String mConfirmpassword) {
		this.mConfirmpassword = mConfirmpassword;
	}

	public int getMonth() {
		return mMonth;
	}

	public void setMonth(int mMonth) {
		this.mMonth = mMonth;
	}

	public int getDate() {
		return mDate;
	}

	public void setDate(int mDate) {
		this.mDate = mDate;
	}

	public int getYear() {
		return mYear;
	}

	public void setYear(int mYear) {
		this.mYear = mYear;
	}

	public String getGender() {
		return mGender;
	}

	public void setGender(String mGender) {
		this.mGender = mGender;
	}

	public String getCodewire() {
		return mCodewire;
	}

	public void setCodewire(String mCodewire) {
		this.mCodewire = mCodewire;
	}
}
