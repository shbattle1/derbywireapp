package com.derbywire.app.settings;

public class CardInfo {
	private String mCardno;
	private String mName;
	private String mType;
	private String mExpyear;
	private String mExpmonth;

	public void setCardno(String cardno) {
		mCardno = cardno;
	}

	public void setCardname(String name) {
		mName = name;
	}

	public void setCardtype(String type) {
		mType = type;
	}

	public void setCardexpyear(String expyear) {
		mExpyear = expyear;
	}

	public void setCardexpmaonth(String expmonth) {
		mExpmonth = expmonth;
	}

	public String getCardno(String cardno) {
		return mCardno;
	}

	public String getCardname(String name) {
		return mName;
	}

	public String getCardtype(String type) {
		return mType;
	}

	public String getCardexpyear(String expyear) {
		return mExpyear;
	}

	public String getCardexpmaonth(String expmonth) {
		return mExpmonth;
	}

}
