package com.derbywire.app.settings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.derbywire.app.R;
import com.derbywire.app.common.AppConstants.ActivityType;
import com.derbywire.app.common.DerbyWireUtils;

public class SettingsActivity extends Activity {
	final String TAG = SettingsActivity.class.getSimpleName();
	private Context mContext = null;
	private TextView mMerchantSale = null;
	private TextView mMerchantOption = null;
	private TextView mDashboard = null;
	private TextView mCardinfo = null;
	private TextView mActivity = null;
	private TextView mMyuploads = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.account_settings);
		mContext = this;
		intResources();
		setClickListerners();
	}

	private void intResources() {
		mMerchantSale = (TextView) findViewById(R.id.merchant_sale);
		mMerchantOption = (TextView) findViewById(R.id.merchant_option);
		mDashboard = (TextView) findViewById(R.id.dashboard);
		mCardinfo = (TextView) findViewById(R.id.card_info);
		mActivity = (TextView) findViewById(R.id.tv_act_set_activity);
		mMyuploads = (TextView) findViewById(R.id.sett_myuploads);
	}

	private void setClickListerners() {
		mMerchantSale.setOnClickListener(onClickListener);
		mMerchantOption.setOnClickListener(onClickListener);
		mDashboard.setOnClickListener(onClickListener);
		mCardinfo.setOnClickListener(onClickListener);
		mActivity.setOnClickListener(onClickListener);
		mMyuploads.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.merchant_sale:
				DerbyWireUtils.startActivity(mContext,
						ActivityType.MERCHFORSALE);
				break;
			case R.id.dashboard:
				DerbyWireUtils.startActivity(mContext, ActivityType.DASHBOARD);
				break;
			case R.id.card_info:
				DerbyWireUtils.startActivity(mContext, ActivityType.CARDINFO);
				break;
			case R.id.tv_act_set_activity:
				DerbyWireUtils.startActivity(mContext, ActivityType.ACTIVITY);
				break;
			case R.id.sett_myuploads:
				DerbyWireUtils.startActivity(mContext, ActivityType.MYUPLOADS);
				break;
			case R.id.merchant_option:
				DerbyWireUtils.startActivity(mContext, ActivityType.MERCHANT);
				break;
			}
		}
	};

}
