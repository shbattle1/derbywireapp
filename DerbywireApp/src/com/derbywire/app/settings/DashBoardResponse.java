package com.derbywire.app.settings;

import com.derbywire.app.core.network.NetworkErrorResponse;

public class DashBoardResponse extends NetworkErrorResponse {
	private DashBoardInfo mDashboardinfo = null;

	public void setDashboadinfo(DashBoardInfo info) {
		this.mDashboardinfo = info;
	}

	public DashBoardInfo getDashboadinfo() {
		return mDashboardinfo;
	}

}
