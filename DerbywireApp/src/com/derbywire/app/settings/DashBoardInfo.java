package com.derbywire.app.settings;

public class DashBoardInfo {
	private int mNumDownloads;
	private int mCardAmount;
	private int mMaleCnt;
	private int mFemaleCnt;

	public DashBoardInfo() {

	}

	public int getNumDownload() {
		return mNumDownloads;
	}

	public int getCardAmt() {
		return mCardAmount;
	}

	public int getMalecnt() {
		return mMaleCnt;
	}

	public int getFemalecnt() {
		return mFemaleCnt;
	}

	public void setNumdownload(int cnt) {
		mNumDownloads += cnt;
	}

	public void setCardAmt(int cnt) {
		mCardAmount += cnt;
	}

	public void setMalecnt(int cnt) {
		mMaleCnt += cnt;
	}

	public void setFemalecnt(int cnt) {
		mFemaleCnt += cnt;
	}
}
