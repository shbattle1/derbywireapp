package com.derbywire.app.settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.derbywire.app.common.RestAPIConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.ICommandListener;
import com.derbywire.app.core.IDependencyTaskListener;
import com.derbywire.app.core.network.ConnectionManager;
import com.derbywire.app.core.network.NetworkResponse;

public class DashBoardCommand extends Command {
	private DashBoardResponse dashboardResponse = null;

	public DashBoardCommand(Command_Id commandId, String commandName,
			ICommandListener listener,
			IDependencyTaskListener dependecyTaskListener, int priority,
			Bundle extraParams) {
		super(commandId, commandName, listener, dependecyTaskListener,
				priority, extraParams);

	}

	public DashBoardResponse getDashboardResponse() {
		return dashboardResponse;
	}

	public void setDashboardResponse(DashBoardResponse response) {
		dashboardResponse = response;
	}

	private void doNetworkOpration() {
		String networkUrl = RestAPIConstants.getInstance().DASHBOARD_INFO;
		byte[] buffer = null;
		NetworkResponse response;
		String content = "";

		try {
			response = ConnectionManager.connect(networkUrl,
					ConnectionManager.REQUEST_METHOD_GET, createHeaders(""),
					buffer);
			content = response.getContent();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!TextUtils.isEmpty(content)) {
			parseResponse(content);
		}
	}

	@Override
	public void clean() throws Exception {

	}

	@Override
	public void execute() throws Exception {
		doNetworkOpration();
	}

	@Override
	public void init() throws Exception {

	}

	@Override
	public void errorOcurredWhileExecutingCommand() {

	}

	private void parseResponse(String content) {
		dashboardResponse = new DashBoardResponse();
		DashBoardInfo info = new DashBoardInfo();

		if (content != null && info != null) {
			JSONObject root;
			try {
				root = new JSONObject(content);
				JSONArray entries = root.optJSONArray("allDownloads");

				for (int index = 0; index < entries.length(); index++) {
					JSONArray subentries = entries.optJSONArray(index);

					// JSONObject jsonObj = subentries.optJSONObject(0);
					// info.setNumdownload(jsonObj.optInt("download_media_id"));
					// info.setCardAmt(jsonObj.optInt("lattitude"));
					// info.setMalecnt(jsonObj.optInt("longitude"));
					// info.setMalecnt(jsonObj.optInt("date_of_download"));
				}
				dashboardResponse.setDashboadinfo(info);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
