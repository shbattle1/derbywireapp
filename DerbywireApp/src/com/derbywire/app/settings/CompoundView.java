/**
 * Copyright (c) 2013 Wireless Designs, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.derbywire.app.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.derbywire.app.R;
import com.derbywire.app.favorite.MeasureUtils;

public class CompoundView extends View {

	private Drawable mRightDrawable;
	private CharSequence mText;
	private StaticLayout mTextLayout, mBottomTextLayout;
	private TextPaint mTextPaint;
	private Point mTextOrigin;
	private int mSpacing;

	public CompoundView(Context context) {
		this(context, null);
	}

	public CompoundView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CompoundView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		mTextOrigin = new Point(0, 0);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.DoubleImageView, 0, defStyle);

		/*
		 * Drawable d = a
		 * .getDrawable(R.styleable.DoubleImageView_android_drawableLeft); if (d
		 * != null) { setLeftDrawable(d); }
		 */

		int color = a
				.getColor(R.styleable.DoubleImageView_android_textColor, 0);
		mTextPaint.setColor(color);

		int rawSize = a.getDimensionPixelSize(
				R.styleable.DoubleImageView_android_textSize, 0);
		mTextPaint.setTextSize(rawSize);

		CharSequence text = a.getText(R.styleable.DoubleImageView_android_text);
		setText(text);

		int spacing = a.getDimensionPixelSize(
				R.styleable.DoubleImageView_android_spacing, 0);
		setSpacing(spacing);

		Drawable d = a
				.getDrawable(R.styleable.DoubleImageView_android_drawableRight);
		if (d != null) {
			setRightDrawable(d);
		}

		a.recycle();
	}

	/*
	 * public void setLeftDrawableResource(int resId) { Drawable d =
	 * getResources().getDrawable(resId); setLeftDrawable(d); }
	 */

	public void setTextRuntime(CharSequence text) {
		mText = text;
		updateContentBounds();
		invalidate();
	}

	/*
	 * public void setLeftDrawable(Drawable left) { mLeftDrawable = left;
	 * updateContentBounds(); invalidate(); }
	 */

	public void setRightDrawable(Drawable right) {
		mRightDrawable = right;
		updateContentBounds();
		invalidate();
	}

	public void setText(int resId) {
		CharSequence text = getResources().getText(resId);
		setText(text);
	}

	public void setText(CharSequence text) {
		if (!TextUtils.equals(mText, text)) {
			mText = text;
			updateContentBounds();
			invalidate();
		}
	}

	public void setSpacing(int spacing) {
		mSpacing = spacing;
		updateContentBounds();
		invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthSize = MeasureUtils.getMeasurement(widthMeasureSpec,
				getDesiredWidth());
		int heightSize = MeasureUtils.getMeasurement(heightMeasureSpec,
				getDesiredHeight());
		setMeasuredDimension(widthSize, heightSize);
	}

	private int getDesiredWidth() {
		/*
		 * int leftWidth; if (mLeftDrawable == null) { leftWidth = 0; } else {
		 * leftWidth = mLeftDrawable.getIntrinsicWidth(); }
		 */

		int textWidth;
		if (mTextLayout == null) {
			textWidth = 0;
		} else {
			textWidth = mTextLayout.getWidth();
		}

		int rightWidth;
		if (mRightDrawable == null) {
			rightWidth = 0;
		} else {
			rightWidth = mRightDrawable.getIntrinsicWidth();
		}

		return rightWidth + textWidth + mSpacing;
	}

	private int getDesiredHeight() {
		int leftHeight;
		if (mRightDrawable == null) {
			leftHeight = 0;
		} else {
			leftHeight = mRightDrawable.getIntrinsicHeight();
		}

		int textHeight;
		if (mTextLayout == null) {
			textHeight = 0;
		} else {
			textHeight = mTextLayout.getHeight();
		}

		int bottomHeight;
		if (mBottomTextLayout == null) {
			bottomHeight = 0;
		} else {
			bottomHeight = mBottomTextLayout.getHeight();
		}

		return leftHeight + bottomHeight;
	}

	private void updateContentBounds() {
		if (mText == null) {
			mText = "";
		}
		float textWidth = mTextPaint.measureText(mText, 0, mText.length());
		mTextLayout = new StaticLayout(mText, mTextPaint, (int) textWidth,
				Layout.Alignment.ALIGN_NORMAL, 1f, 0f, true);

		int left = (getWidth() - getDesiredWidth()) / 2;
		int top = (getHeight() - getDesiredHeight()) / 2;

		if (mTextLayout != null) {
			top = (getHeight() - mTextLayout.getHeight()) / 2;
			mTextOrigin.set(left, top);
			left += (getWidth() - mTextLayout.getWidth()) / 2;
			// top += (getHeight() - mTextLayout.getHeight()) / 2;
		}

		if (mRightDrawable != null) {
			mRightDrawable.setBounds((left + mSpacing), (top),
					(left + mSpacing) + mRightDrawable.getIntrinsicWidth(), top
							+ mRightDrawable.getIntrinsicHeight());

			left += (mRightDrawable.getIntrinsicWidth());
			top += (mRightDrawable.getIntrinsicHeight());
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if (w != oldw || h != oldh) {
			updateContentBounds();
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {

		if (mTextLayout != null) {
			canvas.save();
			canvas.translate(mTextOrigin.x, mTextOrigin.y);

			mTextLayout.draw(canvas);

			canvas.restore();
		}

		if (mRightDrawable != null) {
			mRightDrawable.draw(canvas);
		}
	}
}