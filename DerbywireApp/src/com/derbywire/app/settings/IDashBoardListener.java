package com.derbywire.app.settings;

public interface IDashBoardListener {

	public interface IDashBoardInfoListener {
		public void onDashBoardSuccess(DashBoardResponse response);

		public void onDashBoardFailed(DashBoardResponse response);
	}

}
