package com.derbywire.app.settings;

import com.derbywire.app.common.CommandConstants;
import com.derbywire.app.common.CommandConstants.Command_Id;
import com.derbywire.app.core.Command;
import com.derbywire.app.core.CommandController;
import com.derbywire.app.core.ICommandListener;
import com.derbywire.app.settings.IDashBoardListener.IDashBoardInfoListener;

public class DashBoardHandler {
	private IDashBoardInfoListener mDashBoardListener = null;

	public void getDashBoardinfo(IDashBoardInfoListener playListListener) {
		this.mDashBoardListener = playListListener;
		DashBoardCommand command = new DashBoardCommand(
				Command_Id.DASHBOARD_INFO, CommandConstants.COMMAND_TAG_NULL,
				commandListener, null, Command.PRIORITY_PARALLEL, null);
		try {
			CommandController.getInstance().queueCommand(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ICommandListener commandListener = new ICommandListener() {

		@Override
		public void commandStarted(Command command) {
		}

		@Override
		public void commandQueued(Command command) {
		}

		@Override
		public void commandInProgress(Command command) {

		}

		@Override
		public void commandFailed(Command command) {
			if (command.getCommandId() == Command_Id.DASHBOARD_INFO) {
				mDashBoardListener
						.onDashBoardFailed(((DashBoardCommand) command)
								.getDashboardResponse());
			}
		}

		@Override
		public void commandCompleted(Command command) {
			if (command.getCommandId() == Command_Id.DASHBOARD_INFO) {
				mDashBoardListener
						.onDashBoardSuccess(((DashBoardCommand) command)
								.getDashboardResponse());
			}
		}

		@Override
		public void commandCancelled(Command command) {
		}
	};

}
