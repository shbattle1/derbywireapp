package com.derbywire.app.settings;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.derbywire.app.R;
import com.derbywire.app.common.AppSession;
import com.derbywire.app.common.SettingsEditor;
import com.derbywire.app.managers.ApiManager;
import com.derbywire.app.managers.OptimalPayments;
import com.derbywire.app.models.CardItem;
import com.derbywire.app.models.UserCard;
import com.derbywire.app.models.UserOpInfo;
import com.google.gson.Gson;

public class CardInfoActivity extends Activity {
	LinearLayout cardsLayout;
	int selectedCardIndex = 0;
	SettingsEditor mSettingsEditor;
	OptimalPayments optimalPayments;
	ApiManager apiManager;
	private ImageView mAdd_card;
	private ImageView mEdit_card;
	private ImageView mDel_card;
	private Dialog mDialog;
	private Dialog mProgressDialog;
	private ProgressBar mProgressbar;

	private UserOpInfo opInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cardinformation);
		cardsLayout = (LinearLayout) findViewById(R.id.cards_list);
		mEdit_card = (ImageView) findViewById(R.id.card_editinfo);
		mAdd_card = (ImageView) findViewById(R.id.add_card);
		mDel_card = (ImageView) findViewById(R.id.del_card);
		mProgressbar = new ProgressBar(getApplicationContext());
		// getCardInfo();
		loadCards();
		mAdd_card.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				CustomDialog fragment1 = new CustomDialog();
				fragment1.show(getFragmentManager(), "");
			}
		});

		mEdit_card.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				CustomDialog fragment1 = new CustomDialog();
				fragment1.show(getFragmentManager(), "");
			}
		});

		mDel_card.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DeleteDialog fragment1 = new DeleteDialog();
				fragment1.show(getFragmentManager(), "");
			}
		});

		optimalPayments = new OptimalPayments(getApplicationContext(),
				new ApiManager.TimeOutListener() {
					@Override
					public void onTimeout() {
						// if (mProgressDialog != null) {
						// mProgressDialog.dismiss();
						Toast.makeText(
								getApplicationContext(),
								"Error, Invalid fields\n Either Invalid card number or Invalid Exp Date",
								Toast.LENGTH_LONG).show();
					}
				});

		apiManager = new ApiManager(getApplicationContext(),
				new ApiManager.TimeOutListener() {
					@Override
					public void onTimeout() {
						if (mProgressDialog != null) {
							mProgressDialog.dismiss();
							Toast.makeText(getApplicationContext(),
									"Network error, try again later !",
									Toast.LENGTH_SHORT).show();
						}
					}
				});
	}

	private void showEditCardDialog(final String cardId) {
		try {
			mDialog = new Dialog(CardInfoActivity.this,
					android.R.style.Theme_Black);
			mDialog.setCancelable(true);
			View view = LayoutInflater.from(getApplicationContext()).inflate(
					R.layout.profile_card_new, null);

			final EditText txtCCNumber = (EditText) view
					.findViewById(R.id.card_num);
			final EditText txtExpDate = (EditText) view
					.findViewById(R.id.exp_date);
			final EditText txtCvv = (EditText) view.findViewById(R.id.cvv_no);

			if (cardId != null) {
				String defaultCard = mSettingsEditor
						.getStringValue(OptimalPayments.DEFAULT_CARD);

				for (int i = 0; i < opInfo.getCards().size(); i++) {
					if (opInfo.getCards().get(i).getCardId()
							.equals(defaultCard)) {
						txtCCNumber.setText("**** **** **** "
								+ opInfo.getCards().get(i).getLastDigits());
						break;
					}
				}

			}

			view.findViewById(R.id.btn_cancel).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mDialog.dismiss();
						}
					});

			view.findViewById(R.id.btn_ok).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								AppSession.getInstance().DEBUG(
										getApplicationContext(),
										"CC details  = "
												+ new Gson().toJson(AppSession
														.getInstance()
														.getCard()));

								if (txtCCNumber.getText().length() < 15) {
									txtCCNumber.setError("Invalid");
									txtCCNumber.requestFocus();
								} else if (txtExpDate.getText().length() < 7) {
									txtExpDate.setError(("Invalid"));
									txtExpDate.requestFocus();
								} else {

									String expDate = txtExpDate.getText()
											.toString();
									int year = Integer.parseInt(expDate
											.substring(
													expDate.indexOf("/") + 1,
													expDate.length()));
									if (expDate.contains("/")
											&& expDate.length() == 7) {
										String month = expDate.substring(0,
												expDate.indexOf("/"));
										if (month.length() == 2
												&& Integer.parseInt(month) <= 12) {
											txtExpDate.setError(null);

											UserCard card = new UserCard(
													txtCCNumber.getText()
															.toString(), txtCvv
															.getText()
															.toString(),
													Integer.parseInt(month),
													year);
											AppSession
													.getInstance()
													.DEBUG(getApplicationContext(),
															"Card info = "
																	+ new Gson()
																			.toJson(card));

											// mProgressDialog =
											// SelfieUtil.getFullScreenDialog(getActivity());
											// mProgressDialog.show();

											optimalPayments.createCard(
													mSettingsEditor
															.getStringValue(OptimalPayments.FIELDS.BILLING_ADDRESS_ID),
													mSettingsEditor
															.getStringValue(OptimalPayments.OP_PROFILE_ID),
													card.getCardNumber(), "",
													card.getExpMonth(), card
															.getExpYear(),
													apiResponseListener);
										} else {
											AppSession
													.getInstance()
													.DEBUG(getApplicationContext(),
															"Date invalid1 = "
																	+ new Gson()
																			.toJson(AppSession
																					.getInstance()
																					.getCard()));
											txtExpDate.setError("Invalid");
										}
									} else {
										AppSession
												.getInstance()
												.DEBUG(getApplicationContext(),
														"Date invalid2 = "
																+ new Gson()
																		.toJson(AppSession
																				.getInstance()
																				.getCard()));
										txtExpDate.setError("Invalid");
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});

			mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			mDialog.getWindow().setBackgroundDrawableResource(
					R.color.ripple_dark);
			mDialog.setContentView(view);
			mDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void deleteCard() {
		try {
			String defaultCard = mSettingsEditor
					.getStringValue(OptimalPayments.DEFAULT_CARD);

			opInfo = new Gson().fromJson(
					mSettingsEditor.getStringValue(UserOpInfo.OP_INFO_JSON),
					UserOpInfo.class);

			List<CardItem> cardItems = opInfo.getCards();

			if (cardItems.size() == 1) {
				Toast.makeText(getApplicationContext(),
						"You have only one card cannot delete!",
						Toast.LENGTH_SHORT).show();
				return;
			}

			for (int i = 0; i < cardItems.size(); i++) {
				if (!cardItems.get(i).getCardId().equals(defaultCard)) {

					optimalPayments.deleteCard(mSettingsEditor
							.getStringValue(OptimalPayments.OP_PROFILE_ID),
							cardItems.get(i).getCardId(), apiResponseListener);
					cardItems.remove(i);
					mSettingsEditor.setValue(OptimalPayments.DEFAULT_CARD,
							opInfo.getCards().get(0).getCardId());

					break;
				}
			}

			UserOpInfo opInfo = new UserOpInfo(
					mSettingsEditor
							.getStringValue(OptimalPayments.OP_PROFILE_ID),
					cardItems);
			// apiManager.insertOpInfo(true, opInfo, apiResponseListener);
			mSettingsEditor.setValue(UserOpInfo.OP_INFO_JSON,
					new Gson().toJson(opInfo));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Response.Listener<JSONObject> apiResponseListener = new Response.Listener<JSONObject>() {
		@Override
		public void onResponse(JSONObject response) {
			try {

				AppSession.getInstance().DEBUG(getApplicationContext(),
						"create card response = " + response.toString());

				if (response.toString().contains(
						OptimalPayments.FIELDS.LAST_DIGITS)) {
					List<CardItem> cards = opInfo.getCards();

					if (cards.size() <= 0 || cards == null)
						cards = new ArrayList<CardItem>();

					cards.add(new CardItem(
							response.getString(OptimalPayments.FIELDS.UNIQUE_ID),
							response.getString(OptimalPayments.FIELDS.CARD_TYPE),
							response.getString(OptimalPayments.FIELDS.LAST_DIGITS),
							false));

					UserOpInfo opInfo = new UserOpInfo(
							mSettingsEditor
									.getStringValue(OptimalPayments.OP_PROFILE_ID),
							cards);
					// apiManager.insertOpInfo(true, opInfo,
					// apiResponseListener);
					mSettingsEditor.setValue(UserOpInfo.OP_INFO_JSON,
							new Gson().toJson(opInfo));
					mProgressbar.setVisibility(View.GONE);
				} else {
					apiManager.getOpInfo(apiArrayResponseListener);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Response.Listener<JSONArray> apiArrayResponseListener = new Response.Listener<JSONArray>() {
		@Override
		public void onResponse(JSONArray response) {
			try {
				AppSession.getInstance().DEBUG(getApplicationContext(),
						"op payments response = " + response.toString());

				if (response.toString().contains(UserOpInfo.OP_INFO_JSON)) {
					mSettingsEditor.setValue(SettingsEditor.KEY_FIRST_LAUNCH,
							false);
					mSettingsEditor.setValue(SettingsEditor.KEY_STAY_LOGGED,
							true);

					UserOpInfo opInfo = new Gson().fromJson(response
							.getJSONObject(0)
							.getString(UserOpInfo.OP_INFO_JSON),
							UserOpInfo.class);

					mSettingsEditor.setValue(UserOpInfo.OP_INFO_JSON,
							new Gson().toJson(opInfo));

					loadCards();

					mProgressbar.setVisibility(View.GONE);

				} else {
					mProgressbar.setVisibility(View.GONE);
					// if (mProgressDialog != null)
					// // mProgressDialog.dismiss();

					// if(mDialog != null)
					// mDialog.dismiss();

					String msg = response.get(0).toString();
					AppSession.getInstance().DisplayToast(
							getApplicationContext(),
							msg.substring(0, msg.indexOf("-")));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private void loadCards() {
		try {
			mSettingsEditor = new SettingsEditor(getApplicationContext());
			LayoutInflater Inflater = (LayoutInflater) getApplicationContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = null;
			TextView tvCardInfo = null;
			opInfo = new Gson().fromJson(
					mSettingsEditor.getStringValue(UserOpInfo.OP_INFO_JSON),
					UserOpInfo.class);
			cardsLayout.removeAllViews();

			for (int i = 0; i < opInfo.getCards().size(); i++) {

				view = Inflater.inflate(R.layout.card_view, null);
				view.setTag("card_item" + i);
				view.setId(100 + i);
				tvCardInfo = (TextView) view.findViewById(R.id.ccinfo_text);
				final ImageView cardSelected = (ImageView) view
						.findViewById(R.id.card_selection);

				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
					tvCardInfo.setBackgroundDrawable(getCardBackground(opInfo
							.getCards().get(i).getCardType()));
				else
					tvCardInfo.setBackground(getCardBackground(opInfo
							.getCards().get(i).getCardType()));

				tvCardInfo.setText("\n\n\n\n\n\n\n\n" + "**** **** **** "
						+ opInfo.getCards().get(i).getLastDigits());

				String defaultCard = mSettingsEditor
						.getStringValue(OptimalPayments.DEFAULT_CARD);
				if (opInfo.getCards().get(i).getCardId().equals(defaultCard)) {
					view.findViewById(R.id.card_selection).setVisibility(
							View.VISIBLE);
				}

				cardsLayout.addView(view);

				view.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View cardView) {
						for (int i = 0; i < opInfo.getCards().size(); i++) {
							cardsLayout.getChildAt(i)
									.findViewById(R.id.card_selection)
									.setVisibility(View.GONE);
						}
						selectedCardIndex = Integer.parseInt(cardView.getTag()
								.toString().replace("card_item", ""));
						mSettingsEditor.setValue(OptimalPayments.DEFAULT_CARD,
								opInfo.getCards().get(selectedCardIndex)
										.getCardId());
						cardView.findViewById(R.id.card_selection)
								.setVisibility(View.VISIBLE);
					}
				});
			}
			if (mSettingsEditor.getStringValue(OptimalPayments.DEFAULT_CARD) == null
					|| mSettingsEditor.getStringValue(
							OptimalPayments.DEFAULT_CARD).length() <= 0) {
				cardsLayout.getChildAt(selectedCardIndex)
						.findViewById(R.id.card_selection)
						.setVisibility(View.VISIBLE);
				cardsLayout.scrollTo(
						(int) cardsLayout.getChildAt(selectedCardIndex).getX(),
						0);
				mSettingsEditor.setValue(OptimalPayments.DEFAULT_CARD, opInfo
						.getCards().get(0).getCardId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Drawable getCardBackground(String cardType) {
		Drawable drawable = getResources().getDrawable(R.drawable.card_bg);
		try {

			if (cardType.equals("AM"))
				drawable = getResources().getDrawable(R.drawable.cc_amex);
			else if (cardType.equals("DI"))
				drawable = getResources().getDrawable(R.drawable.cc_discover);
			else if (cardType.equals("MC"))
				drawable = getResources().getDrawable(R.drawable.cc_master);
			else if (cardType.equals("VI"))
				drawable = getResources().getDrawable(R.drawable.cc_visa);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return drawable;
	}

	private class CustomDialog extends DialogFragment {
		private EditText cardno;
		private EditText expirydate;
		private EditText cvv;
		private TextView save_btn;
		private TextView cancel_btn;

		public CustomDialog() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Dialog dialog = new Dialog(getActivity());
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
			dialog.setContentView(R.layout.profile_card_new);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(Color.TRANSPARENT));
			dialog.show();
			cardno = (EditText) dialog.findViewById(R.id.card_num);
			expirydate = (EditText) dialog.findViewById(R.id.exp_date);
			cvv = (EditText) dialog.findViewById(R.id.cvv_no);
			save_btn = (TextView) dialog.findViewById(R.id.btn_ok);
			cancel_btn = (TextView) dialog.findViewById(R.id.btn_cancel);
			save_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						AppSession.getInstance().DEBUG(
								getApplicationContext(),
								"CC details  = "
										+ new Gson().toJson(AppSession
												.getInstance().getCard()));

						if (cardno.getText().length() < 15) {
							cardno.setError("Invalid");
							cardno.requestFocus();
						} else if (expirydate.getText().length() < 7) {
							expirydate.setError(("Invalid"));
							expirydate.requestFocus();
						} else {

							String expDate = expirydate.getText().toString();
							int year = Integer.parseInt(expDate.substring(
									expDate.indexOf("/") + 1, expDate.length()));
							if (expDate.contains("/") && expDate.length() == 7) {
								String month = expDate.substring(0,
										expDate.indexOf("/"));
								if (month.length() == 2
										&& Integer.parseInt(month) <= 12) {
									expirydate.setError(null);

									UserCard card = new UserCard(cardno
											.getText().toString(), cvv
											.getText().toString(), Integer
											.parseInt(month), year);
									AppSession.getInstance().DEBUG(
											getApplicationContext(),
											"Card info = "
													+ new Gson().toJson(card));

									// mProgressDialog =
									// SelfieUtil.getFullScreenDialog(getActivity());
									// mProgressDialog.show();;
									mProgressbar.setVisibility(View.VISIBLE);

									optimalPayments.createCard(
											mSettingsEditor
													.getStringValue(OptimalPayments.FIELDS.BILLING_ADDRESS_ID),
											mSettingsEditor
													.getStringValue(OptimalPayments.OP_PROFILE_ID),
											card.getCardNumber(),
											card.getCvv(), card.getExpMonth(),
											card.getExpYear(),
											apiResponseListener);
								} else {
									AppSession
											.getInstance()
											.DEBUG(getApplicationContext(),
													"Date invalid1 = "
															+ new Gson()
																	.toJson(AppSession
																			.getInstance()
																			.getCard()));
									expirydate.setError("Invalid");
								}
							} else {
								AppSession.getInstance().DEBUG(
										getApplicationContext(),
										"Date invalid2 = "
												+ new Gson().toJson(AppSession
														.getInstance()
														.getCard()));
								expirydate.setError("Invalid");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			cancel_btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dismiss();
				}
			});
			return dialog;
		}
	}

	private class DeleteDialog extends DialogFragment {
		private TextView delete;
		private TextView cancel;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Dialog dialog = new Dialog(getActivity());
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
			dialog.setContentView(R.layout.profile_card_delete);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(Color.TRANSPARENT));
			delete = (TextView) dialog.findViewById(R.id.del_ok);
			cancel = (TextView) dialog.findViewById(R.id.del_cancel);

			delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteCard();
					dismiss();
					mProgressbar.setVisibility(View.VISIBLE);
				}
			});

			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

			dialog.show();
			return dialog;
		}

	}
}
