package com.derbywire.app.settings;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.derbywire.app.R;
import com.derbywire.app.settings.IDashBoardListener.IDashBoardInfoListener;

public class DashBoardActivity extends Activity {

	private DashBoardHandler mDashboardHandler = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		mDashboardHandler = new DashBoardHandler();
		getDashBoard();
	}

	private void getDashBoard() {
		if (mDashboardHandler != null) {
			mDashboardHandler.getDashBoardinfo(new IDashBoardInfoListener() {

				@Override
				public void onDashBoardSuccess(DashBoardResponse response) {
					if (response != null) {
						DashBoardInfo obj = response.getDashboadinfo();
						Log.d("Senthil",
								"Dashbaord value: " + obj.getNumDownload());
					}

				}

				@Override
				public void onDashBoardFailed(DashBoardResponse response) {
					if (response != null) {

					}
				}
			});
		}

	}
}
