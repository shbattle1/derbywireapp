package com.derbywire.app.common;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

public class SettingsEditor {

	private static SharedPreferences settings;
	private static SharedPreferences.Editor settingsEditor;

	public static final int INT_VALUE_NOT_SET = -1;
	public static final String KEY_FIRST_LAUNCH = "first-launch";
	public static final String KEY_STAY_LOGGED = "remember";

	private static final String TAG = Configuration.class.getSimpleName();

	public SettingsEditor(Context context) {
		// Get the app's shared preferences
		settings = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public boolean setArrayList(String key, ArrayList<String> listItems) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putInt(key + "_size", listItems.size());
			for (int i = 0; i < listItems.size(); i++) {
				settingsEditor.putString(key + "_" + i, listItems.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return settingsEditor.commit();
	}

	public ArrayList<String> getArrayList(String key) {
		ArrayList<String> arrayList = new ArrayList<String>();
		try {
			int size = settings.getInt(key + "_size", 0);
			Log.d(TAG, key + "size=" + size);

			for (int i = 0; i < size; i++) {
				arrayList.add(settings.getString(key + "_" + i, ""));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return arrayList;
	}

	public String getCardDetailsForIndex(String key, int index) {
		String cardInfo = "";
		try {
			int size = settings.getInt(key + "_size", 0);
			Log.d(TAG, key + "size=" + size);
			cardInfo = settings.getString(key + "_" + index, "");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cardInfo;
	}

	public boolean removeArrayItem(String key, String value) {
		String valTobeRemoved = value;
		int size = settings.getInt(key + "_size", 0);
		boolean isSuccess = false;
		for (int i = 0; i < size; i++) {
			String removeValue = settings.getString(key + "_" + i, "Unknown");
			Log.i(TAG, "To be removed : " + removeValue);
			if (removeValue.equals(valTobeRemoved)) {
				Log.i(TAG, "Remove : " + valTobeRemoved);
				settingsEditor = settings.edit();
				settingsEditor.remove(key + "_" + i);
				isSuccess = settingsEditor.commit();
				break;
			}
		}
		return isSuccess;
	}

	public boolean removeValue(String key) {
		Log.i(TAG, "Remove : " + key);
		settingsEditor = settings.edit();
		settingsEditor.remove(key);
		return settingsEditor.commit();
	}

	public void setValue(String key, String value) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putString(key, value);
			settingsEditor.commit();
			Log.i("Configuration", "Saving " + key + "=" + value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setValue(String key, Float value) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putFloat(key, value);
			settingsEditor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setValue(String key, boolean value) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putBoolean(key, value);
			settingsEditor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setValue(String key, int value) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putInt(key, value);
			settingsEditor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLongValue(String key, Long value) {
		try {
			settingsEditor = settings.edit();
			settingsEditor.putLong(key, value);
			settingsEditor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getIntValue(String key) {
		return settings.getInt(key, INT_VALUE_NOT_SET);
	}

	public String getStringValue(String key) {
		return settings.getString(key, "Unknown");
	}

	public String getStringValue(String key, String defaultValue) {
		return settings.getString(key, defaultValue);
	}

	public boolean getBooleanValue(String key) {
		return settings.getBoolean(key, false);
	}

	public long getLongValue(String key) {
		return settings.getLong(key, INT_VALUE_NOT_SET);
	}

	public float getFloatValue(String key) {
		return settings.getFloat(key, INT_VALUE_NOT_SET);
	}

	public boolean isFirstLaunch() {
		return settings.getBoolean(KEY_FIRST_LAUNCH, true);
	}

	public boolean isStayLogged() {
		return settings.getBoolean(KEY_STAY_LOGGED, false);
	}

}
