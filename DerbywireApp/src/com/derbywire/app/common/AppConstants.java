package com.derbywire.app.common;

import java.util.HashMap;

import android.os.Environment;

import com.derbywire.app.R;
import com.derbywire.app.airplay.AirPlayActivity;
import com.derbywire.app.browse.BrowseActivity;
import com.derbywire.app.favorite.FavoriteActivity;
import com.derbywire.app.intro.IntroMainActivity;
import com.derbywire.app.menu.MenuActivity;
import com.derbywire.app.merchant.MerchForSale;
import com.derbywire.app.merchant.MerchantActivity;
import com.derbywire.app.messaging.MessagingActivity;
import com.derbywire.app.profiles.ProfileFrames;
import com.derbywire.app.settings.CardInfoActivity;
import com.derbywire.app.settings.DashBoardActivity;
import com.derbywire.app.settings.MyUploadsActivity;
import com.derbywire.app.settings.SettingsActivity;
import com.derbywire.app.settings.SettingsActivityPage;
import com.derbywire.app.signin.SignInActivity;
import com.derbywire.app.signup.SignUpActivity;
import com.derbywire.app.streaming.MarketPlaceActivity;
import com.derbywire.app.streaming.MusicListActivity;
import com.derbywire.app.streaming.PhotosListActivity;
import com.derbywire.app.streaming.VideoListActivity;
import com.derbywire.app.subscriber.SubscriberActivity;
import com.derbywire.app.uploads.CaptureActivity;
import com.derbywire.app.uploads.UploadActivity;

public class AppConstants {

	private static final String TEMP_DIR_NAME = "systemcache";
	public static final String CACHE_DIR = Environment
			.getExternalStorageDirectory() + "/" + TEMP_DIR_NAME + "/";
	public static final String KEY_ENTITY_OBJECT = "entity";

	public static final String PREF_TAG = "DerbywirePreferences";
	public static final String DataPref = "Data";
	public static final String IdPref = "ID";

	public static enum ViewType {
		ALBUM, ARTIST, TRACK
	}

	public static enum detailsPageNo {
		PAGE_ONE, PAGE_TWO
	}

	public static enum ActivityType {
		NONE, SING_IN, SING_UP, MENU, BROWSE, MUSIC_PLAYLIST, PHOTO_PLAYLIST, VIDEO_PLAYLIST, MARKETPLACE, UPLOADS, MYWIRE, CIRCUIT_BOARDS, CREATERS, STATISTICS, MYWALLET, SETTINGS, AIRPLAY, MESSAGING, FAVORITE, PROFILE, SUBSCRIBER, MERCHANT, DASHBOARD, CARDINFO, ACTIVITY, CAPTUREACTIVITY, MERCHFORSALE, MYUPLOADS, INTRO
	}

	public final static HashMap<ActivityType, Class<?>> ActivityMap = new HashMap<ActivityType, Class<?>>();
	static {
		ActivityMap.put(ActivityType.SING_IN, SignInActivity.class);
		ActivityMap.put(ActivityType.SING_UP, SignUpActivity.class);
		ActivityMap.put(ActivityType.MENU, MenuActivity.class);
		ActivityMap.put(ActivityType.BROWSE, BrowseActivity.class);
		ActivityMap.put(ActivityType.MUSIC_PLAYLIST, MusicListActivity.class);
		ActivityMap.put(ActivityType.PHOTO_PLAYLIST, PhotosListActivity.class);
		ActivityMap.put(ActivityType.VIDEO_PLAYLIST, VideoListActivity.class);
		ActivityMap.put(ActivityType.MARKETPLACE, MarketPlaceActivity.class);
		ActivityMap.put(ActivityType.UPLOADS, UploadActivity.class);
		ActivityMap.put(ActivityType.MYWIRE, ProfileFrames.class);
		ActivityMap.put(ActivityType.CIRCUIT_BOARDS, ProfileFrames.class); // Need
																			// to
																			// change
																			// later
		ActivityMap.put(ActivityType.CREATERS, ProfileFrames.class); // Need to
																		// change
																		// later
		ActivityMap.put(ActivityType.STATISTICS, ProfileFrames.class); // Need
																		// to
																		// change
																		// later
		ActivityMap.put(ActivityType.MYWALLET, CardInfoActivity.class);
		ActivityMap.put(ActivityType.SETTINGS, SettingsActivity.class);
		ActivityMap.put(ActivityType.AIRPLAY, AirPlayActivity.class);
		ActivityMap.put(ActivityType.MESSAGING, MessagingActivity.class);
		ActivityMap.put(ActivityType.FAVORITE, FavoriteActivity.class);
		ActivityMap.put(ActivityType.PROFILE, ProfileFrames.class);
		ActivityMap.put(ActivityType.SUBSCRIBER, SubscriberActivity.class);
		ActivityMap.put(ActivityType.MERCHANT, MerchantActivity.class);
		ActivityMap.put(ActivityType.DASHBOARD, DashBoardActivity.class);
		ActivityMap.put(ActivityType.CARDINFO, CardInfoActivity.class);
		ActivityMap.put(ActivityType.ACTIVITY, SettingsActivityPage.class);
		ActivityMap.put(ActivityType.MYUPLOADS, MyUploadsActivity.class);
		ActivityMap.put(ActivityType.MERCHFORSALE, MerchForSale.class);
		ActivityMap.put(ActivityType.CAPTUREACTIVITY, CaptureActivity.class);
		ActivityMap.put(ActivityType.INTRO, IntroMainActivity.class);
	}
	/*
	 * ActivityMap.put(ActivityType.CIRCUIT_BOARDS,
	 * CircuitBoardsActivity.class); ActivityMap.put(ActivityType.CREATERS,
	 * CreatorsActivity.class); ActivityMap.put(ActivityType.STATISTICS,
	 * StatisticsActivity.class); ActivityMap.put(ActivityType.MYWALLET,
	 * CardInfoActivity.class);
	 */
	public static final int galleryImageWidth = 400;

	public static final Integer[] tracksImageId = { R.drawable.gallery_photo_1,
			R.drawable.gallery_photo_2, R.drawable.gallery_photo_3,
			R.drawable.gallery_photo_4, R.drawable.gallery_photo_5 };
}