package com.derbywire.app.common;

/**
 * Created by cogniflex on 10/2/15.
 */

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;

public class KairosObjectRequest extends Request<Object> {

	private final Response.Listener<Object> mListener;
	private String mBody;
	private String mContentType;
	private HashMap mCustomHeaders;
	private static final int RequestTimeOut = 120000;// 120 seconds - change to
														// what you want

	public KairosObjectRequest(int method, String url, HashMap customHeaders,
			String body, Response.Listener<Object> reponseListener,
			Response.ErrorListener errorListener) {

		super(method, url, errorListener);
		mCustomHeaders = customHeaders;
		mBody = body;
		mListener = reponseListener;
		mContentType = "application/json";

		RetryPolicy policy = new DefaultRetryPolicy(RequestTimeOut,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}

	public KairosObjectRequest(String url, HashMap customHeaders,
			Response.Listener<Object> reponseListener,
			Response.ErrorListener errorListener) {

		super(Method.GET, url, errorListener);
		mCustomHeaders = customHeaders;
		mListener = reponseListener;
		mContentType = "application/json";

		RetryPolicy policy = new DefaultRetryPolicy(RequestTimeOut,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}

	@Override
	protected Response parseNetworkResponse(NetworkResponse response) {
		// return Response.success(response.statusCode,
		// HttpHeaderParser.parseCacheHeaders(response));
		try {
			Log.d("Parse nw response = ", response.toString());

			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			Log.d("Json string kairos = ", jsonString);
			return Response.success(jsonString,
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (Exception je) {
			return Response.error(new ParseError(je));
		}
	}

	@Override
	protected void deliverResponse(Object response) {
		mListener.onResponse(response);
	}

	@Override
	public Map getHeaders() throws AuthFailureError {
		if (mCustomHeaders != null) {
			return mCustomHeaders;
		}
		return super.getHeaders();
	}

	@Override
	public byte[] getBody() throws AuthFailureError {
		return mBody.getBytes();
	}

	@Override
	public String getBodyContentType() {
		return mContentType;
	}

	public String getContentType() {
		return mContentType;
	}

	public void setContentType(String mContentType) {
		this.mContentType = mContentType;
	}

}