package com.derbywire.app.common;

public class CommandConstants {

	// Command IDs

	public enum Command_Id {
		COMMAND_ID_NULL, COMMAND_ID_THUMBNAIL, COMMAND_ID_SINGIN, COMMAND_ID_SINGUP, COMMAND_ID_UPLOAD, COMMAND_ID_PHOTO_LIST, COMMAND_ID_PHOTO_DETAILS, COMMAND_ID_AUDIO_LIST, COMMAND_ID_AUDIO_DETAILS, CARDINFO_DELETE, CARDINFO_FETCH, CARDINFO_UPLOAD, DASHBOARD_INFO, SINGIN, SINGUP
	}

	// Command Names

	public static final String COMMAND_TAG_NULL = "CmdNull";
	public static final String CMD_THUMBNAIL = "CmdThumbNail";
	public static final String CMD_SINGIN = "CmdSignIn";
	public static final String CMD_SINGUP = "CmdSignUp";
	public static final String CMD_PROFILE = "CmdProfile";
	public static final String CMD_UPLOAD = "CmdUpload";

}
