package com.derbywire.app.common;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.derbywire.app.models.UserCard;
import com.derbywire.app.models.UserProfile;

/**
 * Created by cogniflex on 16/4/15.
 */
public class AppSession extends Application {

	private static final String TAG = AppSession.class.getSimpleName();

	private static final boolean DEBUG = true;

	private UserProfile mUserProfile;
	private UserProfile mToProfile;

	private static AppSession mInstance;
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	private Bitmap mPhoto;

	private LocationListener mLocationListener;
	private LocationManager mLocationManager;


	public static final String LAST_KNOWN_LOCATION = "last_location";
	public static final boolean USE_DUMMY_PRODUCTS = false;

	UserCard mCard;
	int mCartCount = 0;

	public static final int ORDER_MODE_CART = 0;
	public static final int ORDER_MODE_PICKUP = 1;

	public int PAY;
	public int TRANSFER;

	public String MERCHENTID = null;

	int mCurrentOrderMode = ORDER_MODE_CART;

	private View mCartCountView;

	public static synchronized AppSession getInstance() {

		if (mInstance == null)
			mInstance = new AppSession();

		return mInstance;
	}

	private AppSession() {
	}

	public UserProfile getToUserProfile() {
		return mToProfile;
	}

	public void setToUserProfile(UserProfile profile) {
		mToProfile = profile;
	}

	public void updateUserProfile(UserProfile profile) {
		mUserProfile = profile;
	}

	public UserProfile getUserProfile() {
		return mUserProfile;
	}

	public void DebugToast(Context context, String message) {
		if (DEBUG)
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public void DisplayToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public void DEBUG(Context context, String message) {
		if (DEBUG)
			Log.d(context.getClass().getSimpleName(), message);
	}

	public void ERROR(Context context, String message) {
		Log.e(context.getClass().getSimpleName(), message);
	}

	public View getCountView() {
		return mCartCountView;
	}

	public void setCountView(View cartCountView) {
		mCartCountView = cartCountView;
	}

	public int getCurrentOrderMode() {
		return mCurrentOrderMode;
	}

	public RequestQueue getRequestQueue(Context context) {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(context);
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader(Context context) {
		getRequestQueue(context);
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue,
					new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Context context, Request<T> req,
			String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue(context).add(req);
		DEBUG(context, req + " Added to que");
	}

	public <T> void addToRequestQueue(Context context, Request<T> req) {
		req.setTag(TAG);
		getRequestQueue(context).add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public void setCapturedImage(Bitmap photo) {
		this.mPhoto = photo;
	}

	public Bitmap getCapturedImage() {
		return mPhoto;
	}

	public void setCartCount(int value) {
		mCartCount = value;
	}

	public int getCartCount() {
		return mCartCount;
	}

	public void setCard(UserCard value) {
		mCard = value;
	}

	public UserCard getCard() {
		return mCard;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		try {
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
