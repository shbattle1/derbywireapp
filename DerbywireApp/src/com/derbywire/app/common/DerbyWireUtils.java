package com.derbywire.app.common;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

import org.json.JSONArray;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.Window;

import com.derbywire.app.common.AppConstants.ActivityType;

public class DerbyWireUtils {

	// private static final String TAG = DerbywireUtil.class.getSimpleName();

	public static Bitmap decodeBitmapFromFile(Context context, int imageId,
			boolean scale, int scaleFactor) {
		Options opts = new Options();
		if (scale == true) {
			if (scaleFactor == 0) {
				opts.inSampleSize = 1;
			} else {
				opts.inSampleSize = scaleFactor;
			}
		}
		opts.inPreferredConfig = Config.RGB_565;
		Bitmap bmp = BitmapFactory.decodeResource(context.getResources(),
				imageId, opts);

		context = null;

		return bmp;
	}

	public static Bitmap decodeBitmapFromFile(String filePath, boolean scale,
			int scaleFactor) {
		Options opts = new Options();
		if (scale == true) {
			if (scaleFactor == 0) {
				opts.inSampleSize = 1;
			} else {
				opts.inSampleSize = scaleFactor;
			}
		}
		opts.inPreferredConfig = Config.RGB_565;
		opts.inPurgeable = true;
		FileInputStream fis;
		Bitmap bmp = null;
		try {
			fis = new FileInputStream(filePath);
			bmp = BitmapFactory.decodeStream(fis, null, opts);
			fis.close();
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return bmp;
	}

	public static boolean getBooleanPrefValue(Context context, String key,
			boolean defValue) {
		SharedPreferences preferences = context.getSharedPreferences(
				AppConstants.PREF_TAG, Context.MODE_PRIVATE);

		boolean value = preferences.getBoolean(key, defValue);

		context = null;

		return value;
	}

	public static boolean getBooleanPrefValue(Context context, String key) {
		SharedPreferences preferences = context.getSharedPreferences(
				AppConstants.PREF_TAG, Context.MODE_PRIVATE);

		boolean value = preferences.getBoolean(key, false);

		context = null;

		return value;
	}

	public static void setBooleanPrefValue(Context context, String key,
			boolean value) {
		SharedPreferences preferences = context.getSharedPreferences(
				AppConstants.PREF_TAG, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
		context = null;
	}

	public static int getIntPref(Context context, String prefName,
			int defaultValue) {
		if (null != context) {
			SharedPreferences lPreferences = context.getSharedPreferences(
					AppConstants.PREF_TAG, Context.MODE_PRIVATE);
			int lPrefResult = lPreferences.getInt(prefName, defaultValue);
			return lPrefResult;
		} else
			return defaultValue;
	}

	public static void setIntPref(Context context, String prefName,
			int prefValue) {
		if (null != context) {
			SharedPreferences lPreferences = context.getSharedPreferences(
					AppConstants.PREF_TAG, Context.MODE_PRIVATE);
			SharedPreferences.Editor prefsEditor = lPreferences.edit();
			prefsEditor.putInt(prefName, prefValue);
			prefsEditor.commit();
		}
	}

	public static String getStringPref(Context context, String prefName,
			String defaultValue) {
		if (null != context) {
			SharedPreferences lPreferences = context.getSharedPreferences(
					AppConstants.PREF_TAG, Context.MODE_PRIVATE);
			String lPrefResult = lPreferences.getString(prefName, defaultValue);
			return lPrefResult;
		} else
			return defaultValue;
	}

	public static void setStringPref(Context context, String prefName,
			String prefValue) {
		if (null != context) {
			SharedPreferences lPreferences = context.getSharedPreferences(
					AppConstants.PREF_TAG, Context.MODE_PRIVATE);
			SharedPreferences.Editor prefsEditor = lPreferences.edit();
			prefsEditor.putString(prefName, prefValue);
			prefsEditor.commit();
		}
	}

	public static String getUrlBeforeLastSegment(String pageUri) {
		String base = null;

		if (!TextUtils.isEmpty(pageUri)) {

			int indexOfLastHash = pageUri.lastIndexOf("/");
			base = pageUri.substring(0, indexOfLastHash + 1);
		}

		return base;
	}

	public static String[] getStringArrayFromJSONArray(JSONArray labelsArray) {
		String[] stringArray = null;

		if (labelsArray != null) {
			int count = labelsArray.length();
			stringArray = new String[count];

			for (int index = 0; index < count; index++) {

			}

		}

		return stringArray;
	}

	public static String getContentUrlWithoutHash(String url) {
		String noHashUrl = url;

		int indexOfHash = url.indexOf("#");

		if (indexOfHash != -1) {
			noHashUrl = url.substring(0, indexOfHash);
		}

		return noHashUrl;
	}

	public static void startActivity(Context mContext, ActivityType activityType) {
		Intent intent = new Intent(mContext,
				AppConstants.ActivityMap.get(activityType));
		if (intent != null) {
			mContext.startActivity(intent);
		}
	}

	@TargetApi(11)
	public static void enableStrictMode() {
		if (DerbyWireUtils.hasGingerbread()) {
			StrictMode.ThreadPolicy.Builder threadPolicyBuilder = new StrictMode.ThreadPolicy.Builder()
					.detectAll().penaltyLog();
			StrictMode.VmPolicy.Builder vmPolicyBuilder = new StrictMode.VmPolicy.Builder()
					.detectAll().penaltyLog();

			/*
			 * if (Utils.hasHoneycomb()) {
			 * threadPolicyBuilder.penaltyFlashScreen(); vmPolicyBuilder
			 * .setClassInstanceLimit(ImageGridActivity.class, 1)
			 * .setClassInstanceLimit(ImageDetailActivity.class, 1); }
			 */
			StrictMode.setThreadPolicy(threadPolicyBuilder.build());
			StrictMode.setVmPolicy(vmPolicyBuilder.build());
		}
	}

	public static String getEncodedImage(Bitmap bitmap) {
		byte[] encodedImage = null;
		;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Bitmap scaledBitmap = getScaledBitmap(bitmap, 256, true);
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm
																			// is
																			// the
																			// bitmap
																			// object

			// Log.d("IMAGE_SIZE", "AFTER. Height: " + scaledBitmap.getHeight()
			// + " Width: " + scaledBitmap.getWidth());
			byte[] byteArrayImage = baos.toByteArray();
			encodedImage = Base64.encode(byteArrayImage, Base64.NO_WRAP | Base64.NO_PADDING | Base64.NO_CLOSE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String(encodedImage);
	}

	public static Bitmap getScaledBitmap(Bitmap realImage, float maxImageSize, boolean filter) {
		Bitmap newBitmap = realImage;
		try {

			/*
			 * int width = 256; int height = 256;
			 * 
			 * newBitmap= Bitmap.createBitmap(width, height,
			 * Bitmap.Config.ARGB_8888); float originalWidth =
			 * realImage.getWidth(), originalHeight = realImage.getHeight();
			 * Canvas canvas = new Canvas(newBitmap); float scale =
			 * width/originalWidth; float xTranslation = 0.0f, yTranslation =
			 * (height - originalHeight * scale)/2.0f; Matrix transformation =
			 * new Matrix(); transformation.postTranslate(xTranslation,
			 * yTranslation); transformation.preScale(scale, scale); Paint paint
			 * = new Paint(); paint.setFilterBitmap(true);
			 * canvas.drawBitmap(realImage, transformation, paint);
			 */

			float ratio = Math.min(maxImageSize / realImage.getWidth(), maxImageSize / realImage.getHeight());
			int width = Math.round(ratio * realImage.getWidth());
			int height = Math.round(ratio * realImage.getHeight());

			newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return newBitmap;
	}

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	public static Dialog getFullScreenDialog(final Activity activity) {
		Dialog dialogTransparent = null;
		try {
			dialogTransparent = new Dialog(activity, android.R.style.Theme_Black);
			dialogTransparent.setCancelable(false);
			// View view =
			// LayoutInflater.from(activity).inflate(R.layout.progress_dialog,
			// null);
			View view = null;
			dialogTransparent.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogTransparent.setContentView(view);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dialogTransparent;
	}

	public static String convertSecondsToHMmSs(long millis) {

		int h = (int) (MILLISECONDS.toHours(millis) % 24);
		int m = (int) (MILLISECONDS.toMinutes(millis) % 60);
		int s = (int) (MILLISECONDS.toSeconds(millis) % 60);

		return String.format("%d:%02d:%02d", h, m, s);
	}

}