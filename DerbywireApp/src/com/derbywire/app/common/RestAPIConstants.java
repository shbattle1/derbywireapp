package com.derbywire.app.common;

public class RestAPIConstants {

	private static RestAPIConstants mInstance = null;
	private final String contextPath = "/api";
	private int mEnvIndex;

	public static final String REST_BASE_URLS[] = { 
		"http://50.56.188.53",
		"http://staging.derbygram.com",
		"http://dev.derbywire.com",// Dev Env
		"http://qa.derbywire.com"// QA Env
	};

	private String BASE_SHORT_URL = "";
	private String BASE_URL = BASE_SHORT_URL + contextPath;

	// Declaration of URL
	public String SINGIN_URL = "http://50.56.188.53/api/services/Login/format/json";
	public String SINGUP_URL = "http://50.56.188.53/api/services/Signup/format/json";
	public String PROFILES_URL = "";
	public String UPLOAD_URL = "";
	public String PRODUCT_LIST_URL = "";
	public String PHOTO_LIST_URL = "";
	public String PHOTO_DETAILS_URL = "";
	public String AUDIO_LIST_URL = "";
	public String AUDIO_DETAILS_URL = "";
	public String DASHBOARD_INFO = "";
	public String CARDINFO_FETCH = "";
	public String CARDINFO_SAVE = "";

	private RestAPIConstants() {
	}

	public static RestAPIConstants getInstance() {
		if (mInstance == null) {
			mInstance = new RestAPIConstants();
		}
		return mInstance;
	}

	public void setDerbywireBaseUrl(int envIndex) {
		mEnvIndex = envIndex;
		BASE_URL = RestAPIConstants.REST_BASE_URLS[envIndex] + contextPath;
		BASE_SHORT_URL = RestAPIConstants.REST_BASE_URLS[envIndex];

		reinitAllUrls();
	}

	public int getEnvIndex() {
		return mEnvIndex;
	}

	private void reinitAllUrls() {
		// Assigning Values To URL
		SINGIN_URL   		= BASE_URL + "/services/Login/format/json";
		SINGUP_URL   		= BASE_URL + "/services/Signup/format/json";
		PROFILES_URL 		= BASE_URL + "/derbywire/users/%s/profile";
		UPLOAD_URL   		= BASE_URL + "/derbywire/upload";
		PRODUCT_LIST_URL 	= BASE_URL + "/listProduct.php";
		PHOTO_DETAILS_URL 	= BASE_URL + "/services/getPhotoDetailsMob/id/%s/format/json";
		PHOTO_LIST_URL 		= BASE_URL + "/services/getPhotoPersonalPlayList/id/%s/format/json";
		AUDIO_DETAILS_URL 	= BASE_URL + "/services/getAudioDetailsMob/id/%s/format/json";
		AUDIO_LIST_URL	 	= BASE_URL + "/services/getAudioPersonalPlayList/id/%s/format/json";
		DASHBOARD_INFO      = BASE_URL + "/services/getDownloads/format/json";
		CARDINFO_FETCH      = BASE_URL + "/services/getCardDetails/id/5/format/json";
		CARDINFO_SAVE       = BASE_URL + "/services/saveCardDetails/format/json";

	}

}
