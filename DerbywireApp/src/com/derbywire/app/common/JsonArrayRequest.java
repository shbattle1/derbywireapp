package com.derbywire.app.common;

/**
 * Created by cogniflex on 10/2/15.
 */

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;

public class JsonArrayRequest extends Request<JSONArray> {

	private Listener<JSONArray> listener;
	private Map<String, String> mParams;
	private Map<String, String> mHeaders;

	private static final int RequestTimeOut = 120000;// 120 seconds - change to
														// what you want

	public JsonArrayRequest(String url, Map<String, String> params,
			Listener<JSONArray> reponseListener, ErrorListener errorListener) {
		super(Method.GET, url, errorListener);
		this.listener = reponseListener;
		this.mParams = params;
		RetryPolicy policy = new DefaultRetryPolicy(RequestTimeOut,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}

	public JsonArrayRequest(int method, String url, Map<String, String> params,
			Listener<JSONArray> responseListener, ErrorListener errorListener) {
		super(method, url, errorListener);
		this.listener = responseListener;
		this.mParams = params;
		RetryPolicy policy = new DefaultRetryPolicy(RequestTimeOut,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}

	public JsonArrayRequest(int method, String url, Map<String, String> params,
			Map<String, String> headers, Listener<JSONArray> reponseListener,
			ErrorListener errorListener) {
		super(method, url, errorListener);
		this.listener = reponseListener;
		this.mParams = params;
		this.mHeaders = headers;
		RetryPolicy policy = new DefaultRetryPolicy(RequestTimeOut,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		return mParams != null ? mParams : super.getHeaders();
	};

	@Override
	protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
		try {
			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			return Response.success(new JSONArray(jsonString),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JSONException je) {
			return Response.error(new ParseError(je));
		}
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		return mHeaders != null ? mHeaders : super.getHeaders();
	}

	@Override
	protected void deliverResponse(JSONArray response) {
		// TODO Auto-generated method stub
		listener.onResponse(response);
	}

	/*
	 * @Override protected VolleyError parseNetworkError(VolleyError
	 * volleyError) { if(volleyError.networkResponse != null &&
	 * volleyError.networkResponse.data != null){ VolleyError error = new
	 * VolleyError(new String(volleyError.networkResponse.data)); volleyError =
	 * error; } return volleyError;
	 * 
	 * }
	 */
}