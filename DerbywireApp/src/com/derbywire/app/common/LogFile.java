package com.derbywire.app.common;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author SShinde
 *
 */
public class LogFile {
	public static final boolean DEBUG = true;

	/**
     * This API shows logs depends upon debug value, if debug flag is true then logs will be printed. 
     * for production build debug will be set to false. By default it prints the debug type(i.e Log.d) logs.
     * @param TAG : tag for log
     * @param message: message in log
     */
	public static void ShowLog(String TAG, String message){
		if (DEBUG) {
			ShowLog(Log.DEBUG, TAG, message);
		}
	}
	
	/**
	 * This API shows logs depends log type and debug flag
	 * @param TAG : tag for log
	 * @param logType: type of log, e.g Log.DEBUG/Log.ERROR/Log.WARN/Log.VERBOSE/Log.INFO
	 * @param message: message in log
	 */
	public static void ShowLog(int logType, String TAG,  String message){
		if (DEBUG) {
			PrintLog(TAG, logType, message);
		}
	}
	
	public static void PrintLog(String TAG, int logType, String message){
		switch(logType){
		case Log.DEBUG:
			 Log.d(TAG, message);
			break;
		case Log.ERROR:
			 Log.e(TAG, message);
			break;
		case Log.INFO:
			 Log.i(TAG, message);
			break;
		case Log.WARN:
			 Log.w(TAG, message);
			break;
		case Log.VERBOSE:
			 Log.v(TAG, message);
			break;		
		default:
			 Log.d(TAG, message);
			break;
		}
	}
	
	/**
     * This API shows logs always in all builds.
     * @param TAG : tag for log
     * @param logType: type of log, e.g Log.DEBUG/Log.ERROR/Log.WARN/Log.VERBOSE/Log.INFO
     * @param message: message in log
     */
	public static void ShowLogAlways(String TAG, int logType, String message){
		PrintLog(TAG, logType, message);
	}
	
	/**
     * This API shows toast depends upon debug value, if debug flag is true then only toast will be shown. 
     * for production build debug will be set to false. 
     * @param Context : context of activity.
     * @param message: message to show in toast.
     */
	public static void ShowToast(Context mContext, String Message){
		if (DEBUG){
			Toast.makeText(mContext, Message,
					Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
     * This API shows toast always in all builds.
     * @param Context : context of activity.
     * @param message: message to show in toast.
     */
	public static void ShowToastAlways(Context mContext, String Message){
		Toast.makeText(mContext, Message,
					Toast.LENGTH_LONG).show();
	}
}
